window.addEvent('domready',function(){
  if ($$('.SaveModal')){
    // save modal event
    $$('.SaveModal').addEvent('click',window.fstsystem.modal_save);

    // calendar
    date_picker();
  }
  //console.log(window.modal);
  // modal tabs
  $$('.tabs').each(function(tabs){
    var li_list = tabs.getElement('.modal_tabs').getElements('li');
    var tab_list = tabs.getElement('.modal_contents').getElements('.modal_tab');

    // default active first tab
    if(!$("backActive")) {
      li_list[0].addClass('active');
      tab_list.each(function(tab,k){
        if (k>0){
          tab.addClass('none');
        }
      });
    }
    else{
      $$(".modal_tab").addClass("none");
      $("backActive").addClass('active');
      $($("backActive").get("data-open")).removeClass("none");
    }

    // event tabs li
    li_list.addEvent('click',function(e){
      e.stop();
      li_list.removeClass('active');
      e.target.addClass('active');

      var index = li_list.indexOf(e.target);
      tab_list.addClass('none');
      tab_list[index].removeClass('none');
      //console.log($('modal_in').getSize().y);

      if(e.target.hasClass("ajax")){
        Vars.req = new Request.HTML({
          url:e.target.get("data-href"),
          onComplete: (Vars.reqComplete = function(r){
            tab_list[index].adopt(r);
          }).bind(this)
        });

        Vars.req.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
        Vars.req.post();
      }

      window.modal.resize_modal($('modal_in').getSize().y);
    });
  });

  $$('.password').addEvent('click',function(e){
    e.target.value = '';
    $$('.password2').each(function(item){
      item.value = '';
    });
  });


  // find ares request
  $$('.find_ares').addEvent('change',function(e){
    if (e.target.value != '' && e.target.value.length == 8){
      var form = e.target.getParent('form');
      var label = e.target.getPrevious('label');
      label.addClass('preloader');
      VarsModal.ares_req = new Request.JSON({
        url:'/pages/ares/'+e.target.value,
        onComplete: (VarsModal.ares_req_complete = function(json){
          label.removeClass('preloader');
          if (json.result == true){
            form.getElement('.ares_firma').value = json.firma;
            form.getElement('.ares_dic').value = json.dic;
            form.getElement('.ares_ulice').value = json.ulice;
            form.getElement('.ares_mesto').value = json.mesto;
            form.getElement('.ares_psc').value = json.psc;
            form.getElement('.ares_stat').value = 1;
            form.getElement('.ares_dph').value = json.dph;
           
            //FstAlert(json.message);
          } else {

            FstError(json.message);
          }
          json = null;
        }).bind(this)
      });
      VarsModal.ares_req.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
      VarsModal.ares_req.send();
    }
  });
});

