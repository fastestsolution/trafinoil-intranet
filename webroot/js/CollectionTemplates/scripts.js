window.addEvent('domready', function() {

  var myURI = new URI(window.location);
  var base = "http://"+myURI.parsed.host;
  var modal = window.modal = new frameModal("modalWindow", {base : base});

  $$('.draggable').each(function(drag){
    make_dragable(drag, '/collection-templates/dndCallback/');
  });

});

function make_dragable(dragElement, url){
    var dragAction = new Drag.Move(dragElement, {
            droppables: $$('.droppable'),
            onDrop: Vars['drop_fnc'] = function(element, droppable, event){

                if (droppable){
                   element.inject(droppable);
                   Vars['req'] = new Request({
                       url: url,
                       onSuccess: Vars['req_callback'] = function (json){
                           //TODO process return value true/false actions
                           fstvars.clean();
                       }
                   });
                   Vars['req'].setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
                   Vars['req'].post({
                     'day' : droppable.getParent('tr').get('data-day'),
                     'carrier_id' : droppable.get('data-carrier'),
                     'id' : element.get('data-id')
                   });
                   droppable.removeClass('hovered');
                }else{
                   event.stop();
                }
                element.setStyles({'left':'','top':'','position':'relative'});
            },

            onEnter: Vars['enter_fnc'] = function(element, droppable){
                droppable.addClass('hovered');
            },

            onLeave: Vars['leave_fnc'] = function(element, droppable){
                droppable.removeClass('hovered');
            }
	});
}


function close_modal(){
  $("close_modal").click();
}
