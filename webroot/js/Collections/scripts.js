window.addEvent('domready', function() {

  date_divide();

  var myURI = new URI(window.location);
  var base = "http://"+myURI.parsed.host;
  var modal = window.modal = new frameModal("modalWindow", {base : base});

  //$$(".table-formated .td a.add_item").addEvent("click", function(e){
  //  var url = e.target.get("href");
  //  modal.show(e.target);
  //  modal.setContent(url);
  //  e.stop();
  //});


  $$('.draggable').each(function(drag){
    make_dragable(drag, '/collections/dndCallback/');
  });
});

function make_dragable(dragElement, url){

    var dragAction = new Drag.Move(dragElement, {
            droppables: $$('.droppable'),

            onStart : function(){
              dragElement.getElement("a").removeEvents("click").addEvent("click", function(e){
                e.stop();
              })
            },

            onComplete : function(){
              (function(){
                dragElement.getElement("a").addEvent("click", function(e){
                  e.stop();
                  var url = this.get("href");
                  window.modal.show(e.target);
                  window.modal.setContent(url);

                });
              }).delay(500);
            },

            onDrop: Vars['drop_fnc'] = function(element, droppable, event){
                if (droppable){
                   element.inject(droppable);
                   Vars['req'] = new Request({
                       url: url,
                       onSuccess: Vars['req_callback'] = function (json){
                           fstvars.clean();
                       }
                   });
                   Vars['req'].setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
                   Vars['req'].post(
                     {
                       'day' : droppable.getParent('.tr').get('data-day'),
                       'carrier_id' : droppable.get('data-carrier'),
                       'id' : element.get('data-id')
                     }
                   );
                   droppable.removeClass('hovered');
                }else{
                   event.stop();
                }
                element.setStyles({'left':'','top':'','position':'relative'});
            },

            onEnter: Vars['enter_fnc'] = function(element, droppable){
                droppable.addClass('hovered');
            },

            onLeave: Vars['leave_fnc'] = function(element, droppable){
                droppable.removeClass('hovered');
            }
	});
}

function date_divide(){
  $$(".date").addEvent("change", function(){
    var date = new Date(window.picker.date);

    if(this.hasClass("from")){
      date.setDate(date.getDate() + 28);
      $$(".date.to").set("value", date.format("%d.%m.%Y"));
    }
    else{
      date.setDate(date.getDate() - 28)
      $$(".date.from").set("value", date.format("%d.%m.%Y"));
    }
  })
}

function close_modal(){
  $("close_modal").click();
}
