/**
 * Created by Marin Hrabal on 20.6.2016.
 */

var ShopsOrder = new Class({
  initialize : function(){
    this.drag = $$("#in-shops tbody");

    new Sortables(this.drag, {
      clone : false,
      revert : true,
      opacity : 0.8,
      handle : "td:first-of-type",

      onComplete: function(){
        var iter = 1;
        var order = [];

        var elements = this.drag.getElements("tr");
        elements.each(function(i, k){
          order = i.get("data-id");
        });

        $("shops-in-sort").value = order.toString();
        send_sorting();

      }.bind(this)
    });
  }
});

window.addEvent('domready',function(){
  var so = new ShopsOrder();
});

function send_sorting(){
  var req = new Request({
    url: "/collections/reorder",
    data:
    {
      "order" : $("shops-in-sort").value,
      "collection_id" : $("collection-id").value
    },
    oncomplete : function(j){
      console.log(j);
    }
  });
  req.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
  req.send();
}


