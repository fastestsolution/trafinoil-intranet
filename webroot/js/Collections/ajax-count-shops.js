window.addEvent('domready', function() {
  $("route-id").addEvent("change", function(){
      var route_id = $(this).value;

      Vars['req'] = new Request({
        url: "/routes/count-shops/"+route_id,
        onSuccess: Vars['req_callback'] = function (html){
          $("count-shops").removeClass("load").innerHTML = html;
          fstvars.clean();
          window.modal.resize_modal($('modal_in').getSize().y);
        },
        onRequest: Vars["req_progress"] = function(){
          $("count-shops").addClass("load");
        }
      });
      Vars['req'].setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
      Vars['req'].post();
  });
});