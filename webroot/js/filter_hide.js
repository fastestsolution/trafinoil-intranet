$$(".filter_hide").addEvent("click", function(){
  var color = $(this).get("value");
  var checked = $(this).checked;
  var trs = $$(".fst-hide .table-formated tr");
  trs.each(function(item){
    if(item.hasClass(color)){
      if(checked === true){
        item.show();
      }
      else{
        item.hide();
      }
    }
  });
})