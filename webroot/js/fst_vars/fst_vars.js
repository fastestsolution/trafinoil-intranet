/*
Fst Vars 
created Jakub Tyson Fastest Solution 
copyright 2015
*/
var Vars = {};
var VarsModal = {};
var FstVars = this.FstVars = new Class({
	Implements:[Options,Events],
	debug : false,
	
	
	// init fce
	initialize:function(options){
		this.setOptions(options);
		//console.log('run');
		
		
		
	},
	clean: function(){
		
		//console.log(Vars[items]);
		//console.log(Vars);
		
		
		if (this.debug == true){
		
			console.log('before Vars: ');
			console.log(Vars);
			console.log('before VarsModal: ');
			console.log(VarsModal);
		}
		Object.each(Vars,function(item,k){
			//console.log(typeof(item));
			//console.log(k);
			//console.log(item);
			if (k == 'modal'){
				//console.log(item);
			}
			else if (item == null){
				Vars[k] = null;
				delete Vars[k];
				//return true;
			}
			else if (typeof(item) == 'object'){
				//console.log('clean');
				//console.log(item);
				if (Object.getLength(item)>0){
					
					Object.each(item,function(it){
						//console.log('it');
						//console.log(it);
						it = null; 
					});
					
				}
				if (item.cancel){
					//console.log(item);
					item.cancel();
				}
				if (item.abort)
					item.abort();
				
				item = null;
				Vars[k] = null;
				delete Vars[k];
				//delete item;
			}
			
			else if (typeof(item) == 'function'){
				item = null;
				delete item;
				Vars[k] = null;
				delete Vars[k];
			}
			else if (typeof(item) == 'string'){
				item = null;
				delete item;
				Vars[k] = null;
				delete Vars[k];
			}
			else if (typeof(item) == 'number'){
				item = null;
				delete item;
				Vars[k] = null;
				delete Vars[k];
			}
		});
		if (this.debug == true){
			console.log('after Vars: ');
			console.log(Vars);
			console.log('after VarsModal: ');
			console.log(VarsModal);
			
		}
		Vars = {};
		
	},
	clean_modal: function(){
		
		//console.log(Vars[items]);
		//console.log(Vars);
		
		
		if (this.debug == true){
		
			console.log('before Vars: ');
			console.log(Vars);
			console.log('before VarsModal: ');
			console.log(VarsModal);
		}
		Object.each(VarsModal,function(item,k){
			//console.log(typeof(item));
			//console.log(k);
			//console.log(item);
			if (k == 'modal'){
				//console.log(item);
			}
			else if (item == null){
				VarsModal[k] = null;
				delete VarsModal[k];
				//return true;
			}
			else if (typeof(item) == 'object'){
				//console.log(VarsModal);
				//console.log('clean');
				//console.log(item);
				if (Object.getLength(item)>0){
					
					Object.each(item,function(it){
						//console.log('it');
						//console.log(it);
						it = null; 
					});
					
				}
				if (item.cancel){
					//console.log(item);
					item.cancel();
				}
				if (item.abort)
					item.abort();
				//delete item;
				//console.log(VarsModal[k]);
				//console.log(k);
				item = null;
				VarsModal[k] = null;
				delete VarsModal[k];
				//delete item;
			}
			
			else if (typeof(item) == 'function'){
				item = null;
				delete item;
				VarsModal[k] = null;
				delete VarsModal[k];
			}
			else if (typeof(item) == 'string'){
				item = null;
				delete item;
				VarsModal[k] = null;
				delete VarsModal[k];
			}
			else if (typeof(item) == 'number'){
				item = null;
				delete item;
				VarsModal[k] = null;
				delete VarsModal[k];
			}
		});
		if (this.debug == true){
			console.log('after Vars: ');
			console.log(Vars);
			console.log('after VarsModal: ');
			console.log(VarsModal);
			
		}
		
		VarsModal = {};
		
	},
	
	
	modal_clean: function(){
		this.clean_modal();
		
	},
	
	destroyEl:function(el) {
	  if (el)
	  el.getElements('*').each((function(elem){
		  
		  //console.log(elem);
		  elem.removeEvents('click');
		  elem.removeEvents('change');
		  //elem.parentNode.removeChild(elem);
		  //console.log(elem);
		  //elem.destroy();
		  this.removeChildrenFromNode(elem);
		  elem.destroy();
		  elem = null;
		  //delete elem;
	  }).bind(this));
	  
	  /*
	  if (google.maps){
		  google.maps.event.clearInstanceListeners(window);
		  google.maps.event.clearInstanceListeners(document);
	  
	  }
	  */
	  
	  el.destroy();
	  el = null;
	  
	},
	clearEl: function(el) {
	  if (el)
	  el.getElements('*').each((function(elem){
		  
		  //console.log(elem);
		  elem.removeEvents('click');
		  elem.removeEvents('change');
		  //elem.parentNode.removeChild(elem);
		  //console.log(elem);
		  //elem.destroy();
		  this.removeChildrenFromNode(elem);
		  //console.log(elem);
		  elem.destroy();
		  //delete elem;
		  elem = null;
		  //console.log(elem);
		  //delete elem;
	  }).bind(this));
	  el = null;
	  
	 },
	 
	 removeChildrenFromNode: function(node){
		if (!node) return;
		while (node.hasChildNodes()) {
			this.removeChildrenFromNode(node.firstChild);
			node.removeChild(node.firstChild);
		}
	  }
	
	
	
});

//})();	
//window.addEvent('domready', function () {
	var fstvars = new FstVars();
//});