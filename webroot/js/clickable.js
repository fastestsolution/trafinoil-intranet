/**
 * Created by Marin Hrabal on 20.1.2016.
 */

var Clickable = new Class({

  initialize : function() {
    this.selected = {};
  },

  select : function(el){

    //console.log(el);

    if(!$(el).hasClass("selected")) {
      $(el).addClass("selected");
      this.selected[el.get("click-id")] = {data : el.get("click-data")};
    }
    else{
      $(el).removeClass("selected");
      if(this.selected[el.get("click-id")]){
        delete(this.selected[el.get("click-id")]);
      }
    }
  },

  getId : function(el){
    return el.get("click-id");
  }

});