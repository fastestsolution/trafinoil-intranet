var FstPages = this.FstPages = new Class({
	Implements:[Options,Events],
	
	// init fce
	initialize:function(options){
		this.top_menu();
		localStorage.clear();
	},
	
	top_menu: function(){
		
		$('top_menu').getElements('a').removeClass('active');
		if ($('top_menu').getElement('.menu_'+$('body').get('data-controller')))
		$('top_menu').getElement('.menu_'+$('body').get('data-controller')).addClass('active');
		
		$('top_menu2').getElement('.menu_logout').addEvents({
			'mouseenter':function(){
				$('logged_info').removeClass('none');
			},
			'mouseleave':function(){
				$('logged_info').addClass('none');
			},
		});
	
	},
	
	
	
});
window.addEvent('domready',function(){
	var fst_pages = new FstPages();

	
});

