    function autoSave(element){
        if(!element.get('data-id')){
            var result = false;
            var form = $('base_route_form');
            if($('name').value == ''){
                alert('Vyplnte nejprve prosim nazev trasy');
                return false;
            }
            var req = new Request.JSON({
                async: false,
                url: form.get('action'),
                onSuccess: function save_cllbck(json){
                    if(json && json.r == true){
                        element.set('href', element.get('href') + '/' + json.data);
                        result = true;
                    }else{
                        alert(json.m);
                    }
                }
            });
            req.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
            req.send(form);
            return result;
        }else{
            return true;
        }
    }