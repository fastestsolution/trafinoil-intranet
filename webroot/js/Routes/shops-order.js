/**
 * Created by Marin Hrabal on 20.6.2016.
 */

var ShopsOrder = new Class({
  initialize : function(){
    this.drag = $$("#drag tbody");

    new Sortables(this.drag, {
      clone : false,
      revert : true,
      opacity : 0.8,
      onComplete: function(){
        var iter = 1;
        var order = [];

        var elements = this.drag.getElements("tr");
        elements.each(function(i, k){
          order = i.get("data-id");
          i.each(function(r){
            r.getElement(".ord").set("html", (iter++));
          });
        });

        $("shops-sort").value = order.toString();

      }.bind(this)
    });


  }




});

window.addEvent('domready',function(){
  var so = new ShopsOrder();
});


