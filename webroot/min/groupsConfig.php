<?php
//if (! function_exists('src1_fetch')) {function src1_fetch() {return file_get_contents('http://scripts.fastesthost.cz/js/mootools1.4/c_core.js');}}
if (! function_exists('src1_fetch')) {function src1_fetch() {return file_get_contents('http://scripts.fastesthost.cz/js/mootools1.4/core1.6.js');}}
//if (! function_exists('src2_fetch')) {function src2_fetch() {return file_get_contents('http://scripts.fastesthost.cz/js/mootools1.4/c_more.js');}}
if (! function_exists('src2_fetch')) {function src2_fetch() {return file_get_contents('http://scripts.fastesthost.cz/js/mootools1.4/c_more1.6.js');}}

if (! function_exists('css1_fetch')) {function css1_fetch() {return file_get_contents('https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css');}}


$src1 = new Minify_Source(array('id' => 'source1','getContentFunc' => 'src1_fetch','contentType' => Minify::TYPE_JS,'lastModified' => ($_SERVER['REQUEST_TIME'] - $_SERVER['REQUEST_TIME'] % 86400),));
$src2 = new Minify_Source(array('id' => 'source2','getContentFunc' => 'src2_fetch','contentType' => Minify::TYPE_JS,'lastModified' => ($_SERVER['REQUEST_TIME'] - $_SERVER['REQUEST_TIME'] % 86400),));

$css1 = new Minify_Source(array('id' => 'source1_css','getContentFunc' => 'css1_fetch','contentType' => Minify::TYPE_CSS,'lastModified' => ($_SERVER['REQUEST_TIME'] - $_SERVER['REQUEST_TIME'] % 86400),));





return array(
    'js' => array(
        $src1,
        $src2,
        '//js/clickable.js',
        '//js/frameModal.js',
//        '//js/uploader/DropZone.js',
//        '//js/uploader/DropZone.Flash.js',
//        '//js/uploader/DropZone.HTML4.js',
//        '//js/uploader/DropZone.HTML5.js',
        '//js/uploader/Request.Blob.js',
				'//js/mbox/min/mBox.All.min.js',
				'//js/autocompleter/autocomplete.js',
	      '//js/fst_history/fst_history.js',
				'//js/fst_system/fst_menu.js',   //right click menu
				'//js/fst_system/fst_system.js',   //global function system

				'//js/calendar/Picker.js',
				'//js/calendar/Picker.Attach.js',
				'//js/calendar/Picker.Date.js',
				'//js/calendar/Picker.Date.Range.js',
		
        '//js/lang/cz.js',   //language soubor pro JS
        '//js/default.js',   //obdoba c_page_load.js
        '//js/multiselect.js',
        '//js/fst_vars/fst_vars.js',
	      '//js/fst_system/context-menu.js',
	      '//js/lightFace/LightFace.js',
	      '//js/lightFace/LightFace.IFrame.js'
    ),
	
    'css' => array(
          //$css1,				
        '//css/fonts/fonts.css',
        '//css/ajax.css',
				'//js/mbox/assets/mBoxNotice.css',
        '//css/base.css',
        '//css/mixins.css',
        '//css/default.css',
        '//css/pospiech.css',
				'//css/response.css',
	      '//css/lightFace/LightFace.css',
        
		'//js/calendar/datepicker_vista/datepicker_vista.css',
    ),
);
