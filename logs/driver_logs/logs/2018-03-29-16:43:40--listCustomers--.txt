Array
(
    [__className] => RestResponse
    [status] => 200
    [data] => Array
        (
            [0] => Array
                (
                    [__className] => Customer
                    [ico] => 01119656
                    [dic] => 
                    [street] => Řeznická 227
                    [town] => Benešov
                    [zip] => 
                    [pays_vat] => 
                    [is_invoiced] => 0
                    [name] => Alena Kudrnová
                    [id] => 8760
                )

            [1] => Array
                (
                    [__className] => Customer
                    [ico] => 03435300
                    [dic] => 
                    [street] =>  Pražská 955/34
                    [town] => Brandýs nad Labem-Stará Bolesl
                    [zip] => 
                    [pays_vat] => 
                    [is_invoiced] => 0
                    [name] => Jitka Tylová
                    [id] => 14653
                )

            [2] => Array
                (
                    [__className] => Customer
                    [ico] => 86565168
                    [dic] => CZ5955101108
                    [street] => Pražkého povstnání 18/82
                    [town] => Benešov u Prahy
                    [zip] => 
                    [pays_vat] => 
                    [is_invoiced] => 0
                    [name] => Eva Boušková
                    [id] => 4
                )

            [3] => Array
                (
                    [__className] => Customer
                    [ico] => 67427685
                    [dic] => 
                    [street] => Hráského 1936
                    [town] => Benešov
                    [zip] => 
                    [pays_vat] => 
                    [is_invoiced] => 0
                    [name] => Dung Le Thi
                    [id] => 4784
                )

            [4] => Array
                (
                    [__className] => Customer
                    [ico] => 47086271
                    [dic] => CZ6909010031
                    [street] => Vlašimská 1927
                    [town] => Benešov u Prahy
                    [zip] => 
                    [pays_vat] => 1
                    [is_invoiced] => 0
                    [name] => Hospůdka U Soudku
                    [id] => 11
                )

            [5] => Array
                (
                    [__className] => Customer
                    [ico] => 49389441
                    [dic] => CZ6504162709
                    [street] => Rezlerova 290 Petrovice
                    [town] => Praha 10
                    [zip] => 
                    [pays_vat] => 1
                    [is_invoiced] => 0
                    [name] => Jaroslav Švehla
                    [id] => 17
                )

            [6] => Array
                (
                    [__className] => Customer
                    [ico] => 45065438
                    [dic] => CZ516219329
                    [street] => Okružní 275
                    [town] => Týnec nad Sázavou
                    [zip] => 
                    [pays_vat] => 
                    [is_invoiced] => 0
                    [name] => JÍDELNA METAZ
                    [id] => 20
                )

            [7] => Array
                (
                    [__className] => Customer
                    [ico] => 69305340
                    [dic] => CZ6811211836
                    [street] => Tyršova 189
                    [town] => Benešov u Prahy
                    [zip] => 
                    [pays_vat] => 
                    [is_invoiced] => 0
                    [name] => Restaurace Halong
                    [id] => 28
                )

            [8] => Array
                (
                    [__className] => Customer
                    [ico] => 41743229
                    [dic] => CZ6609290479
                    [street] => Hvězdonice 104
                    [town] => Chocerady
                    [zip] => 
                    [pays_vat] => 
                    [is_invoiced] => 0
                    [name] => Jiří Tvrzník
                    [id] => 29
                )

            [9] => Array
                (
                    [__className] => Customer
                    [ico] => 10067311
                    [dic] => 021-6308180758
                    [street] => Jemniště 15
                    [town] => Postupice
                    [zip] => 
                    [pays_vat] => 
                    [is_invoiced] => 0
                    [name] => Miroslav Požárek
                    [id] => 30
                )

            [10] => Array
                (
                    [__className] => Customer
                    [ico] => 86615734
                    [dic] => CZ8001160387
                    [street] => Palmovka 888/3
                    [town] => Praha 8
                    [zip] => 
                    [pays_vat] => 
                    [is_invoiced] => 0
                    [name] => Tomáš Bejček
                    [id] => 31
                )

            [11] => Array
                (
                    [__className] => Customer
                    [ico] => 71298606
                    [dic] => CZ6310231653
                    [street] => Lipová 200
                    [town] => Pyšely
                    [zip] => 
                    [pays_vat] => 1
                    [is_invoiced] => 0
                    [name] => Stará hospoda Nespeky
                    [id] => 44
                )

            [12] => Array
                (
                    [__className] => Customer
                    [ico] => 45241350
                    [dic] => CZ45241350
                    [street] => Jahodnická 795/20
                    [town] => Praha 9, Hloubětín
                    [zip] => 
                    [pays_vat] => 1
                    [is_invoiced] => 0
                    [name] => IGC - INTERNATIONAL GURMAN CLUB, s.r.o.
                    [id] => 12149
                )

            [13] => Array
                (
                    [__className] => Customer
                    [ico] => 25084313
                    [dic] => CZ25084313
                    [street] => nám. Svobody 527
                    [town] => Třinec, Lyžbice, 
                    [zip] => 
                    [pays_vat] => 1
                    [is_invoiced] => 0
                    [name] => KARTA GROUP a.s.
                    [id] => 805
                )

            [14] => Array
                (
                    [__className] => Customer
                    [ico] => 42872146
                    [dic] => CZ6805220598
                    [street] => Na kopci 1292
                    [town] => Třinec
                    [zip] => 
                    [pays_vat] => 1
                    [is_invoiced] => 0
                    [name] => Pavel Pelech
                    [id] => 819
                )

            [15] => Array
                (
                    [__className] => Customer
                    [ico] => 13443372
                    [dic] => CZ515919254
                    [street] => č.p. 191(pošta Třinec)
                    [town] => Oldřichovice
                    [zip] => 
                    [pays_vat] => 1
                    [is_invoiced] => 0
                    [name] => Eva Burianová
                    [id] => 824
                )

            [16] => Array
                (
                    [__className] => Customer
                    [ico] => 00642215
                    [dic] => CZ00642215
                    [street] => Jankovcova 1603/47a
                    [town] => Praha 7, Holešovice
                    [zip] => 
                    [pays_vat] => 1
                    [is_invoiced] => 0
                    [name] => EUREST, spol. s r.o.
                    [id] => 2647
                )

            [17] => Array
                (
                    [__className] => Customer
                    [ico] => 00642215
                    [dic] => CZ00642215
                    [street] => Jankovcova 1603/47a
                    [town] => Praha 7, Holešovice
                    [zip] => 
                    [pays_vat] => 1
                    [is_invoiced] => 0
                    [name] => EUREST, spol. s r.o.
                    [id] => 2647
                )

            [18] => Array
                (
                    [__className] => Customer
                    [ico] => 00642215
                    [dic] => CZ00642215
                    [street] => Jankovcova 1603/47a
                    [town] => Praha 7, Holešovice
                    [zip] => 
                    [pays_vat] => 1
                    [is_invoiced] => 0
                    [name] => EUREST, spol. s r.o.
                    [id] => 2647
                )

            [19] => Array
                (
                    [__className] => Customer
                    [ico] => 70134324
                    [dic] => CZ6753141923
                    [street] => Radotínská 40
                    [town] => Černošice
                    [zip] => 
                    [pays_vat] => 1
                    [is_invoiced] => 0
                    [name] => Jana Koubková
                    [id] => 3966
                )

            [20] => Array
                (
                    [__className] => Customer
                    [ico] => 87983516
                    [dic] => 
                    [street] => Bezručova 220/9
                    [town] => Český Těšín, 
                    [zip] => 
                    [pays_vat] => 
                    [is_invoiced] => 0
                    [name] => 	David Janeczko

                    [id] => 7801
                )

            [21] => Array
                (
                    [__className] => Customer
                    [ico] => 26827310
                    [dic] => CZ26827310
                    [street] => Staroměstská 278
                    [town] => Třinec, Staré Město
                    [zip] => 
                    [pays_vat] => 1
                    [is_invoiced] => 0
                    [name] => GAJA servis s. r. o.
                    [id] => 4003
                )

            [22] => Array
                (
                    [__className] => Customer
                    [ico] => 62473221
                    [dic] => 
                    [street] => Jiřího Horáka 1697
                    [town] => Benešov
                    [zip] => 
                    [pays_vat] => 1
                    [is_invoiced] => 0
                    [name] => Martin Činátl
                    [id] => 4069
                )

            [23] => Array
                (
                    [__className] => Customer
                    [ico] => 04496680
                    [dic] => CZ6753111046
                    [street] => Habrová 401
                    [town] => Třinec, Dolní Líštná, 
                    [zip] => 
                    [pays_vat] => 1
                    [is_invoiced] => 0
                    [name] => Dagmar Křižánková
                    [id] => 14509
                )

            [24] => Array
                (
                    [__className] => Customer
                    [ico] => 49828894
                    [dic] => CZ49828894
                    [street] => Jiráskova 888
                    [town] => Benešov
                    [zip] => 
                    [pays_vat] => 1
                    [is_invoiced] => 0
                    [name] => Školní jídelna Benešov
                    [id] => 4608
                )

            [25] => Array
                (
                    [__className] => Customer
                    [ico] => 61387703
                    [dic] => 
                    [street] => Komenského 365
                    [town] => Jílové u Prahy
                    [zip] => 
                    [pays_vat] => 
                    [is_invoiced] => 0
                    [name] => Základní škola 
                    [id] => 4757
                )

            [26] => Array
                (
                    [__className] => Customer
                    [ico] => 15516652
                    [dic] => 
                    [street] => Pivovarská 89/1
                    [town] => Přerov1-Město
                    [zip] => 
                    [pays_vat] => 
                    [is_invoiced] => 0
                    [name] => Pavel Mračna-PEM-EXPRES-langoše, zmrzlina
                    [id] => 4404
                )

            [27] => Array
                (
                    [__className] => Customer
                    [ico] => 64489949
                    [dic] => CZ7505294456
                    [street] => Jaroslava Kučery 1533/54
                    [town] => Prostějov, 
                    [zip] => 12345
                    [pays_vat] => 1
                    [is_invoiced] => 0
                    [name] => Jiří Sklenář
                    [id] => 15691
                )

            [28] => Array
                (
                    [__className] => Customer
                    [ico] => 25542320
                    [dic] => CZ25542320
                    [street] => Hlavní 210
                    [town] => ŠTÍTINA    
                    [zip] => 
                    [pays_vat] => 1
                    [is_invoiced] => 0
                    [name] => JP group s.r.o.
                    [id] => 10814
                )

            [29] => Array
                (
                    [__className] => Customer
                    [ico] => 00934917
                    [dic] => CZ8405284679
                    [street] => Cvrčelka 483
                    [town] => Plumlov 
                    [zip] => 
                    [pays_vat] => 1
                    [is_invoiced] => 0
                    [name] => Lukáš Valtr
                    [id] => 13321
                )

            [30] => Array
                (
                    [__className] => Customer
                    [ico] => 
                    [dic] => 
                    [street] => 
                    [town] => 
                    [zip] => 
                    [pays_vat] => 
                    [is_invoiced] => 0
                    [name] => Firma bez IČA
                    [id] => 5435
                )

            [31] => Array
                (
                    [__className] => Customer
                    [ico] => 00000000
                    [dic] => 
                    [street] => 
                    [town] => 
                    [zip] => 
                    [pays_vat] => 
                    [is_invoiced] => 0
                    [name] => Černohlávek
                    [id] => 5911
                )

        )

    [total] => 32
)
