<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $title ?>
    </title>
    <?= $this->Html->meta('icon') ?>
	<link rel="icon" href="/css/layout/favicon.png" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet/less" type="text/css" href="<?php echo $cssUri;?>"  media="screen" />
    <?= $this->Html->script("less"); ?>
    <?= $this->Html->script($jsUri); ?>
	<?php /* ?>
	<script src="http://scripts.fastesthost.cz/js/mootools1.4/core1.6.js"></script>
	<script src="http://scripts.fastesthost.cz/js/mootools1.4/c_more1.6.js"></script>
    */?>


    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
	<?php
		if (isset($scripts)){if (is_array($scripts)){foreach ($scripts as $link){echo $this->Html->script($link);}} else {echo $this->Html->script($scripts);}}
		if (isset($styles)){if (is_array($styles)){foreach ($styles as $style){echo $this->Html->css($style);} } else {echo $this->Html->css($styles);}} 
	?>
</head>
<body id="body" class="<?php echo strtolower($this->request->controller).' '.$this->request->action;?>" data-controller="<?php echo strtolower($this->request->controller)?>">
    <?= $this->Flash->render() ?>
	<?php echo $this->element('top-menu');?>
    <section id="content">
        <div class="container2 clearfix">
            <?= $this->fetch('content') ?>
        </div>
    </section>
	<div id="addon"></div>
	<div id="page_url" class="none"><?php echo 'http://'+$_SERVER['HTTP_HOST'];?></div>
</body>
</html>
