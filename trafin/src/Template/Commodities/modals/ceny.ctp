<?php
foreach($countries as $k => $name){
	if($k == 0){
		continue;
	}

	echo '<h3>'.$name.'</h3>';
		foreach($entity["country_commodities"] as $i => $cc){
			if($cc["country_id"] == $k){
				echo '<div class="row">';
				echo '<div class="one col">'.$this->Form->input("Commodities.country_commodities.".$i.".id", ['value' => $cc["id"]]).'</div>';
				echo '<div class="two col">'.$this->Form->input("Commodities.country_commodities.".$i.".name", ['value' => $cc["name"], "label" => __("Název")]).'</div>';
				echo '<div class="one col">'.$this->Form->input("Commodities.country_commodities.".$i.".shortcut", ['value' => $cc["shortcut"], "label" => __("Zkratka")]).'</div>';
				echo '<div class="one col">'.$this->Form->input("Commodities.country_commodities.".$i.".price", ['value' => $cc["price"], "label" => __("Cena")]).'</div>';
				echo '<div class="one col">'.$this->Form->input("Commodities.country_commodities.".$i.".dph", ['value' => $cc["dph"], "label" => __("DPH")]).'</div>';
				echo '<div class="one col">'.$this->Form->input("Commodities.country_commodities.".$i.".znz_correction", ['value' => $cc["znz_correction"], "label" => __("Korekce ZNZ")]).'</div>';
				echo '<div class="one col">'.$this->Form->input("Commodities.country_commodities.".$i.".stock_price_tolerance", ['value' => $cc["stock_price_tolerance"], "label" => __("Tolerance")]).'</div>';
				echo '</div>';
				break;
			}
		}


}