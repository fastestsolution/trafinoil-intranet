<nav class="large-3 medium-4 col" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New User Auth'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="userAuths index large-9 medium-8 col content">
    <h3><?= __('User Auths') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('user_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($userAuths as $userAuth): ?>
            <tr>
                <td><?= $this->Number->format($userAuth->id) ?></td>
                <td><?= $this->Number->format($userAuth->user_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $userAuth->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $userAuth->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $userAuth->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userAuth->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
