<div class="row route-info">
	<div class="<?= (isset($driverCollection)? "three" : "four"); ?> col">
		<?php
			if(isset($route)){
				$regions = $regions->toList();
				echo '<table class="table-formated horizontal secondary">';
				if(isset($driverCollection)) {
					echo '<tr>';
					echo '<th>' . __("Čas otevření") . '</th>';
					echo '<td>'. $driverCollection["date_start"] .'</td>';
					echo '</tr>';
					echo '<tr>';
					echo '<th>' . __("Čas uzavření") . '</th>';
					echo '<td>' . ((!empty($driverCollection["date_complete"])) ? $driverCollection["date_complete"] : "") . '</td>';
					echo '</tr>';
					echo '<tr>';
					echo '<th>' . __("Zpracováno") . '</th>';
					echo '<td></td>';
					echo '</tr>';
					echo '<tr>';
					echo '<th>' . __("Černé zastávky") . '</th>';
					echo '<td></td>';
					echo '</tr>';
					echo '<tr>';
						echo '<th>' . __("Dopravce") . '</th>';
						echo '<td>'.$entity["carrier"]["name"].'</td>';
					echo '</tr>';
				}
				echo '<tr>';
				echo '<th width="30%">'.__("Region").'</th>';
				echo '<td>'.$regions[$route->region_id].'</td>';
				echo '</tr>';
				echo '<tr>';
				echo '<th>'.__("Trasa").'</th>';
				echo '<td>'.$route->name.'</td>';
				echo '</tr>';
				echo '<tr>';
				echo '<th>'.__("Stav").'</th>';
				echo '<td>'.$collection_status["name"][$entity->status].'</td>';
				echo '</tr>';
				echo '<tr>';
				echo '<th>'.__("Ujeto").'</th>';
				echo '<td>'.((!empty($entity->distance))? $entity->distance : 0).' km</td>';
				echo '</tr>';

				echo '</table>';
			}
		?>
	</div>
	<?php
		if(isset($route) && !isset($driverCollection)) {
			echo $this->element("../Collections/elements/route_info/second");
		}
		if(isset($route)  && !isset($driverCollection)) {
			echo $this->element("../Collections/elements/route_info/third");
		}
		if(isset($route)  && isset($driverCollection)){
			echo $this->element("../Collections/elements/collection_info/sum", ["trades" => $trades]);
		}
	?>
</div>