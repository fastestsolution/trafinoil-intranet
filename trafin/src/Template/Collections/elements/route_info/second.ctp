<div class="four col">
	<?php
		$carriers->toList();
		echo '<table class="table-formated horizontal secondary">';
		echo '<tr>';
		echo '<th width="30%">'.__("Datum").'</th>';
		echo '<td>'.$this->Form->input("date_plan", ["type" => "text", "label" => false, "value" => $date, "class" => "date", "required" => true, "placeholder" => __("Vyberte datum")]).'</td>';
		echo '</tr>';
		echo '<tr>';
		echo '<th>'.__("Dopravce").'</th>';
		echo '<td>'.$this->Form->select("carrier_id", $carriers, ["id" => "carrier-id", "value" => $entity->carrier->id]).'</td>';
		echo '</tr>';
		echo '</table>';
		echo $this->Html->link(__("Mapa"), ["action" => "map", $entity["id"]], ["ajax" => true, "class" => "btn"]);
    echo $this->Form->button(__("Svoz je připraven"), ["id" => "saveCollection", "class" => "btn", "style" => "margin-left:5px;"]);
    echo $this->Form->hidden("status", ["value" => 2]);
    echo $this->Form->hidden("collection_id", ["value" => $entity["id"], "id" => "collection-id"]); ?>
</div>