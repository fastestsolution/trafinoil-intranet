<div class="four col">
	<?php
		$carriers->toList();
		echo '<table class="table-formated horizontal secondary">';
		echo '<tr>';
		echo '<th width="30%">' . __("Start") . '</th>';
		echo '<td id="stock-in-wrap">'
			. $this->Form->input("start_type", ["options" => ["Bydliště", "Sklad"], "label" => false, "default" => 0])
			. $this->Form->input("start", ["value" => ((!empty($entity["start"])) ? $entity["start"] : $entity["carrier"]["street"] . " " . $entity["carrier"]["town"]), "label" => false])
			. '</td>';
		echo '</tr>';
		echo '<tr>';
		echo '<th>' . __("Cíl") . '</th>';
		echo '<td id="stock-out-wrap">'
			. $this->Form->input("end_type", ["options" => ["Bydliště", "Sklad"], "label" => false, "default" => 0])
			. $this->Form->input("end", ["value" => ((!empty($entity["start"])) ? $entity["start"] : $entity["carrier"]["street"] . " " . $entity["carrier"]["town"]), "label" => false])
			. '</td>';
		echo '<tr>';
		echo '<th>' . __("Sklad výdej") . '</th>';
		echo '<td>' . $this->Form->input("stock_out", ["options" => $stocks, "label" => false]) . '</td>';
		echo '</tr>';
		echo '<tr>';
		echo '<th>' . __("Sklad příjem") . '</th>';
		echo '<td>' . $this->Form->input("stock_in", ["options" => $stocks, "label" => false]) . '</td>';
		echo '</tr>';
		echo '</table>';

	?>
</div>