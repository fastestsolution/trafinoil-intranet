<div class="layout">
	<?php

		$filter_from = new \Cake\Chronos\Date($date_from);
		echo '<div id="filtration">';
		echo $this->Form->create("filtr", ["style" => "margin: 0"]);
		echo $this->Form->input("user_id", ["options" => $users, "label" => false, "empty" => __("Všichni dispečeři")]);
		echo '<label>'.$this->Html->link("<span class='fa fa-angle-left'>", [$filter_from->modify("-28 days")], ["escape" => false]).'</label>';
		echo $this->Form->input("date_from", ["class" => "date from", "style" => "width: 80px; text-align:center", "label" => false, "value" => $date_from]);
		echo '<label>-</label>';
		echo $this->Form->input("date_to", ["class" => "date to", "style" => "width: 80px; text-align:center", "label" => false, "value" => $date_to]).'<label>'.$this->Html->link("<span class='fa fa-angle-right'></span>", [$filter_from->modify("+28 days")], ["escape" => false]).'</label>';

		echo $this->Form->button(__("Vybrat"), ["class" => "btn"]);
		echo $this->Form->end();
		echo '</div>';
	?>
	<div id="fst_history" class="collections">
    <?= $this->element("../Collections/elements/table"); ?>
	</div>
</div>

<?= $this->Html->script("Collections/scripts.js"); ?>
