<?php
	echo $this->element("Collection/count_shops", ["data" => $entity["shops_colors"]]);
?>
<div class="row">
	<div class="col three">
	<?php
		echo $this->Form->create("Collections");
			echo $this->Form->hidden("id", ["value" => $entity["id"]]);
			echo $this->Form->hidden("type", ["value" => "changeStatus"]);
			echo $this->Form->hidden("status", ["value" => 1]);
			echo $this->Form->button(__("Vytvořit svoz"), ["class" => "btn", "id" => "SaveModal"]);
		echo $this->Form->end();
	?>
	</div>
</div>
