<?php
	echo $this->Form->create();
		echo '<div class="row"><div class="col twelve">';
		echo $this->Form->input("route_id", ["options" => $routes, "label" => __("Vyberte trasu"), "default" => ((isset($route_id))? $route_id : null)]);
		echo '</div></div>';
		echo $this->element("Collection/count_shops", ["data" => ((isset($count_shops))? $count_shops : null)]);
		echo '<div class="tcenter">';
				echo $this->Form->button(__("Vytvořit svoz"), ["class" => "btn SaveModal"]);
			echo '</div>';
	echo $this->Form->end();

	echo $this->Html->script("fst_system/modal_window");
	echo $this->Html->script("Collections/ajax-count-shops");

?>

