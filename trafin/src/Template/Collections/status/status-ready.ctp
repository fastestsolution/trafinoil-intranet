<div class="layout row" id="collections-layout">
	<?php

		$trades = [1 => [], 2 => []];

		$t = '<table class="table-formated">';
		$t .= '<thead><tr>';
			$t .= '<th width="30px">'.__("Pořadí").'</th>';
			$t .= '<th width="80px">'.__("Čas").'</th>';
			$t .= '<th>'.__("Provoz").'</th>';
			$t .= '<th>'.__("Stav").'</th>';
			$t .= '<th>'.__("Poznámka Dopravce").'</th>';
			$t .= '<th>'.__("Poznámka disp.").'</th>';
			$t .= '<th>'.__("Obchody").'</th>';
			$t .= '<th>'.__("Plán pořadí").'</th>';
			$t .= '<th width="50px">'.__("Plán datum").'</th>';
		$t .= '</tr></thead><tbody>';
		if(isset($driverCollection)){
//			pr($driverCollection);
//			die();
			if(isset($driverCollection["driver_collection_items"]) && count($driverCollection["driver_collection_items"])){
				foreach($driverCollection["driver_collection_items"] as $dci){
					$t .= '<tr>';
						$t .= '<td>'.$dci->poradi.'</td>';
						$t .= '<td>'.$dci->time->format("j.n h:i").'</td>';
						$t .= '<td>'.$this->Html->link($dci["shop"]["name"], ["controller" => "shops", "action" => "edit", $dci["shop"]["id"]], ["ajax" => true]).'</td>';
						$t .= '<td><span class="handle-status '.\Cake\Core\Configure::read("select_config.driver_collection_item.handled.decode.".$dci->status).'"></span></td>';
						$t .= '<td>'.$dci->note.'</td>';
						$t .= '<td>'.$dci["CollectionShops"]["call_note"].'</td>';

						$b = 0;
						$s = 0;

						if(isset($dci["driver_collection_item_trades"]) && count($dci["driver_collection_item_trades"])){
							foreach($dci["driver_collection_item_trades"] as $trade){
								// výkup
								if($trade["type"] == 1) {
									$b++;
									if (!isset($trades[$trade["type"]][$trade["commodity_id"]])) {
										$trades[$trade["type"]][$trade["commodity_id"]] =
											[
												"price" => 0,
												"price_vat" => 0,
												"amount" => 0,
												"name" => $trade["commodity"]["name"],
												'unit' => $trade["commodity"]["unit"],
												'debt' => [0, 1],
												'cash' => [0, 1]
											];
									}
									if($dci["vykup_faktura"] == 1){
										$trades[$trade["type"]][$trade["commodity_id"]]["debt"][$dci["dph_payer"]] += $trade["price_no_vat"];
									}
									else{
										$trades[$trade["type"]][$trade["commodity_id"]]["cash"][$dci["dph_payer"]] += $trade["price_no_vat"];
									}

									$trades[$trade["type"]][$trade["commodity_id"]]["price_vat"] += $trade["price"];
									$trades[$trade["type"]][$trade["commodity_id"]]["price"] += $trade["price_no_vat"];
									$trades[$trade["type"]][$trade["commodity_id"]]["amount"] += $trade["amount"];
								}
								// prodej
								else{
									$s++;
									if (!isset($trades[$trade["type"]][$trade["commodity_id"]])) {
										$trades[$trade["type"]][$trade["commodity_id"]] =
											[
												"price_hotove" => 0,
												"price_faktura" => 0,
												"amount" => 0,
												"pack" => $trade["commodity"]["in_pack"],
												"name" => $trade["commodity"]["name"],
												'unit' => $trade["commodity"]["unit"],
												'debt' => [0, 1],
												'cash' => [0, 1]
											];
									}
									if($dci["vykup_faktura"] == 1) {
										$trades[$trade["type"]][$trade["commodity_id"]]["price_faktura"] += $trade["price"];
									}
									else{
										$trades[$trade["type"]][$trade["commodity_id"]]["price_hotove"] += $trade["price"];
									}
									$trades[$trade["type"]][$trade["commodity_id"]]["amount"] += $trade["amount"];
								}
							}
						}
						//pr($dci);
						$t .= '<td>'.$this->Html->link('buy:'.$b.' sell:'.$s, ["action" => "view-trades", $dci->id], ["ajax" => true]).'</td>';
						$t .= '<td>'.$dci["CollectionShops"]["ordering"].'</td>';
						$t .= '<td>'.$entity->date_plan->format("j.n").'</td>';
					$t .= '</tr>';
				}
			}
			else{
				$t .= "Zatím nejsou žádná data";
			}
		}
		$t .= '</tbody></table>';
		echo $this->element("../Collections/elements/route_info", ["trades" => $trades]);

	?>
	<div class="row">
		<div class="tvelwe col">
		<?php echo $t; ?>
		</div>
	</div>
</div>