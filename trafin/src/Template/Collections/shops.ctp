
	<div class="col six" id="fst_hide" <?= ((count($shops))? null : 'style="display:none"') ?>>

		<div id="filtration">
			<?php
				echo $this->element("color_filter", ["filter_hide" => 1]);
			?>
			<?= $this->Html->link(__("Přidat provoz do svozu"), ["controller" => "collections", "action" => "addNotInRouteShop", $entity["id"]], ["ajax" => true, "class" => "btn xs fright", "style" => "margin-top: -3px; margin-right: 0"]) ?>
		</div>
		<table class="table-formated" id="all-shops">
			<thead>
				<tr>
					<th></th>
					<th><?= __("Provozovna") ?></th>
					<th class="tcenter"><?= __("Obj.") ?></th>
					<th><?= __("poznámka svozu / provozovny") ?></th>
					<th><?= __("Marketingová poznámka") ?></th>
					<th class="call"></th>
				</tr>
			</thead>
			<tbody>
			<?php
			if(count($shops)){
				foreach($shops as $k => $rs){
					echo $this->Element->collectionShopTr($rs, $entity["id"], $route["id"]);
				}
			}
			?>
			</tbody>
		</table>
	</div>
	<div class="col six">
		<?php echo $this->Form->hidden("shops.sort", ["value" => "", "id" => "shops-in-sort"]); ?>
		<table class="table-formated" id="in-shops">
			<thead>
				<tr>
					<th colspan="6"><div style="padding: 6px 0;"><?= __("Zařazené")." (<span id='shop-don-in-count'>".count($shopsDonIn)."</span>)" ?></div></th>
				</tr>
				<tr>
					<th></th>
					<th><?= __("Provozovna") ?></th>
					<th class="tcenter"><?= __("Obj.") ?></th>
					<th><?= __("poznámka svozu / provozovny") ?></th>
					<th><?= __("Marketingová poznámka") ?></th>
					<th class="call"></th>
				</tr>
			</thead>
			<tbody>
			<?php
				if(isset($shopsDonIn) && count($shopsDonIn)){
					foreach($shopsDonIn as $k => $rs){
						echo $this->Element->collectionShopTr($rs, $entity["id"], $entity["route"]["id"], true);
					}
				}
			?>
			</tbody>
		</table>
	</div>

	<div class="col six" style="float:right">
		<table class="table-formated" id="out-shops">
			<thead>
			<tr>
				<th colspan="6"><div style="padding: 6px 0;"><?= __("Nezařazené")." (<span id='shop-don-out-count'>".count($shopsDonOut)."</span>)" ?></div></th>
			</tr>
			<tr>
				<th></th>
				<th><?= __("Provozovna") ?></th>
				<th class="tcenter"><?= __("Obj.") ?></th>
				<th><?= __("poznámka svozu / provozovny") ?></th>
				<th><?= __("Marketingová poznámka") ?></th>
				<th class="call"></th>
			</tr>
			</thead>
			<tbody>
			<?php

				if(isset($shopsDonOut) && count($shopsDonOut)){
					foreach ($shopsDonOut as $k => $rs){
						echo $this->Element->collectionShopTr($rs, $entity["id"], $entity["route"]["id"], true);
					}
				}
			?>
			</tbody>
		</table>
	</div>

	<?php echo $this->Html->script("Collections/edit-shops"); ?>
	<?php echo $this->Html->script("Collections/shops-order"); ?>
