<div class="layout" id="collections-layout">
	<div class="top-actions"></div>
	<?= $this->Form->create($entity); ?>
	<?php
		echo $this->element("../Collections/elements/route_info");
	?>

	<div class="row collection-shops">
			<?php echo $this->element("../Collections/shops"); ?>
	</div>
	<?= $this->Form->end(); ?>
</div>

<script type="text/javascript">
	date_picker();
</script>
