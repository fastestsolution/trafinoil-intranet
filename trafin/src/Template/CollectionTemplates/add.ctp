<?php
echo $this->Form->create($entity, ["class" => "tcenter"]);
	echo $this->Form->hidden("carrier_id", ["value" => $carrier_id]);
	echo $this->Form->hidden("day", ["value" => $day]);
	echo $this->Form->hidden("user_id", ["value" => $user_id]);
	echo '<h2 class="tcenter">'.__("Přidat svoz do šablony?").'</h2>';
		echo '<div style="width: 300px; margin: auto;">';
		echo $this->Form->input("route_id", ["options" => @$routes, "label" => __("Vyberte dopravci přiřazenou trasu")]);
		echo '</div>';
		echo $this->Form->button(__("Uložit"), ["class" => "btn SaveModal", "style" => "margin-right: 15px"]);
echo $this->Form->end();
?>
