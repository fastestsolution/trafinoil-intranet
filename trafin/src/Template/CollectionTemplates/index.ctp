<div class="layout">
<?php
	echo '<div id="filtration">';
		echo $this->Form->create("filtr", ["style" => "margin: 0"]);
		echo $this->Form->input("user_id", ["options" => $users, "label" => false]);
		echo $this->Form->button(__("Vybrat"), ["class" => "btn"]);
		echo $this->Form->end();
	echo '</div>';
	echo '<div id="fst_history" class="collections">';
		echo $this->element("../CollectionTemplates/elements/table");
	echo '</div>';

	echo $this->Html->script("CollectionTemplates/scripts.js");
?>
</div>


