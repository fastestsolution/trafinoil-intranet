<?php
	echo $this->Form->create("replicate");
	echo '<div class="row">';
	echo '<h2>Zvolte datum</h2>';
	echo '<div class="col twelve" style="height: 100px;">';
	echo $this->Form->input("date", ["class" => "date", "value" => date("d.m.Y"), "label" => __("Zvolte datum začátku replikace svozů")]);
	echo '</div>';
	echo '</div>';
	echo '<div class="tcenter">';
	echo $this->Form->button(__("Replikovat"), ["class" => "btn", "id" => "SaveModal"]);
	echo '</div>';
	echo $this->Form->end();
?>


<script type="text/javascript">
	date_picker();
</script>
