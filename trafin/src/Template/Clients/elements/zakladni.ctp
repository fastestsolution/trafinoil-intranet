<?php  
if (isset($zakazka->client_id)){
	$real_id = $zakazka->client_id;
}  
if (isset($clients->id)){
	$real_id = $clients->id;
}
?>
<?php echo $this->Form->input((isset($zakazka)?'client.':'')."id",['class'=>'add_adr client_id client_adopt','data-type'=>'client_id','value'=>(isset($real_id)?$real_id:'tmp_'.time())]); ?>
<fieldset>
<?php //pr(); ?>
	<legend><?php echo __("Základní údaje")?></legend>
	<div class="sll"> 
		<div class="relative">
			<?php echo $this->Form->input((isset($zakazka)?'client.':'').'name', ['label' => __("Společnost").':']); ?>
			<div id="auto_loading" class="auto_loading none"></div>
		</div>
	</div>
	<div class="slr"> 
		<?php echo $this->Form->input((isset($zakazka)?'client.':'').'ic', ['label' => __("IČ").':','class'=>'client_adopt']); ?>
		<?php echo $this->Form->input((isset($zakazka)?'client.':'').'dic', ['label' => __("DIČ").':','class'=>'client_adopt']); ?>

	</div>
</fieldset>
<fieldset>
	<legend><?php echo __("Fakturační adresa")?></legend>
	<div class="sll"> 
		<?php echo $this->Form->input((isset($zakazka)?'client.':'').'ulice', ['label' => __("Ulice").':','tabindex'=>20,'class'=>'client_adopt']); ?>
		<?php echo $this->Form->input((isset($zakazka)?'client.':'').'psc', ['label' => __("PSČ").':','tabindex'=>22,'class'=>'client_adopt']); ?>
		
	</div>
	<div class="slr"> 
		<?php echo $this->Form->input((isset($zakazka)?'client.':'').'mesto', ['label' => __("Město").':','tabindex'=>21,'class'=>'client_adopt']); ?>
		<?php echo $this->Form->input((isset($zakazka)?'client.':'').'stat', ['label' => __("Stát").':','tabindex'=>24,'class'=>'client_adopt']); ?>

	</div>
</fieldset>
<?php //pr($client); ?>