<div class="users form large-9 medium-8 col content">
    <?= $this->Form->create("UserAuths") ?>
    <fieldset>
        <legend><?= __('Editovat oprávnění uživatele')." ".h($user["name"]) ?></legend>
        <?php
            foreach($auths as $con => $act){
                if(is_array($act)) {
                    echo '<h2>'.$auth_names["controller"][$con].'</h2>';
                    foreach ($act as $a) {
                        echo $this->Form->input($con .".". $a, ['type' => 'checkbox', 'label' => $auth_names["action"][$a], 'checked' => ((isset($user_auth[$con][$a]) && $user_auth[$con][$a] == 1 )? true : false)]);
                    }
                }
                else{
                    echo '<h2>'.$auth_names["controller"][$con].'</h2>';
                    echo $this->Form->input($con . ".all", ['type' => 'checkbox', 'label' => $auth_names["controller"][$act], 'checked' => ((isset($user_auth[$con][$act]) && $user_auth[$con][$act] == 1 )? true : false)]);
                }
            }
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
