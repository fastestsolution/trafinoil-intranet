<div class="row">
	<div class="col three">
		<?php echo $this->Form->input('name', ['label' => __('Jméno a příjmení')]); ?>
	</div>
	<div class="col three">
	<?php echo $this->Form->input('username', ['label' => __('Email')]); ?>
	</div>
	<div class="clear"></div>
	<div class="col two">
		<?php echo $this->Form->input('phone', ['label' => __('Telefon')]); ?>
	</div>
	<div class="col two">
	<?php echo $this->Form->input(__("password"), ['type' => 'password', 'label' => __('Heslo'),'class'=>'password', 'value' => '']);?>
	</div>
	<div class="col two">
		<?php echo $this->Form->input(__("password2"), ['type' => 'password', 'label' => __('Heslo znovu'),'class'=>'password2', 'value' => '']);	?>
	</div>
	<div class="clear"></div>
	<div class="col one">
		<?php echo $this->Form->input(__("active"), ['options' => $bool, 'default' => 1, 'label' => __('Aktivní'),'class'=>'password2']);	?>
	</div>
</div>
