<style>
	#gmap {width:80%;display:inline-block;height:600px;overflow: visible;background:transparent url('/css/validation/preloader_load.gif') no-repeat center center;}
	#side_panel {width:19%;display:inline-block;height:600px;}
	#side_panel.preloader {background:url('/css/validation/preloader_load.gif') no-repeat center center;}
	#delka_trasa {position:absolute;top:0;right:0;width:20%;}
</style>
<div class="relative">
	<div id="gmap"></div>
	<div id="delka_trasa"></div>
	<div id="side_panel"></div>
</div>
<?php
	echo $this->Html->script("maps/map.js");
?>
<script type="text/javascript">
	function mapInit(){
		$$("li[data-open=modal-tab-map]").addEvent("click", function(){
			var opt = {
				'id':$('MapId').value,
				'url' : '/routes/getShopsInRoute/'
			}
			var Map = new window.Map($('gmap'),opt);

		});
	}
</script>
