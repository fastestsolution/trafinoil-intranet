<table class="table-formated">
	<tr>
		<th colspan="2"><?= __("Výdej") ?></th>
	</tr>
	<tr>
		<td width="80px"><strong><?= __("Jméno:") ?></strong></td>
		<td><?= $this->Form->input("Shops.contact_name", ['label' => false]); ?></td>
	</tr>
	<tr>
		<td><strong><?= __("Telefon:") ?></strong></td>
		<td><?= $this->Form->input("Shops.contact_mobile", ['label' => false]); ?></td>
	</tr>
</table>