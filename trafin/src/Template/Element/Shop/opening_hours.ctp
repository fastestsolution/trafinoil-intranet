<?php

	// parametr $data obsahuje serialize array 1-7(po-ne) f(from),t(to),c(close)
	// parametr $disabled zruší možnost editace

	if(!empty($data)){
		$data = unserialize($data);
	}
?>

<table class="table-formated" id="opening_hours">
	<tr>
		<th colspan="4"><?= __("Provozní doba") ?></th>
	</tr>
	<tr>
		<td width="25%"><strong><?= __("Pondělí:") ?></strong></td>
		<td width="25%">
			<?= $this->Form->input("opening_hours.1.f", ["value" => (isset($data[1])? $data[1]["f"] : null), 'label' => false, "disabled" => (isset($disabled)? true : false)]); ?>
		</td>
		<td width="25%">
			<?= $this->Form->input("opening_hours.1.t", ["value" => (isset($data[1])? $data[1]["t"] : null), 'label' => false, "disabled" => (isset($disabled)? true : false)]); ?>
		</td>
		<td width="25%">
			<?= $this->Form->input("opening_hours.1.c", ["checked" => ((isset($data[1]) && $data[1]["c"])? true : false), 'label' => false, "disabled" => (isset($disabled)? true : false)]); ?>
		</td>
	</tr>
	<tr>
		<td><strong><?= __("Úterý:") ?></strong></td>
		<td>
			<?= $this->Form->input("opening_hours.2.f", ["value" => (isset($data[2])? $data[2]["f"] : null), 'label' => false, "disabled" => (isset($disabled)? true : false)]); ?>
		</td>
		<td>
			<?= $this->Form->input("opening_hours.2.t", ["value" => (isset($data[2])? $data[2]["t"] : null), 'label' => false, "disabled" => (isset($disabled)? true : false)]); ?>
		</td>
		<td>
			<?= $this->Form->input("opening_hours.2.c", ["checked" => ((isset($data[2]) && $data[2]["c"])? true : false), 'label' => false, "disabled" => (isset($disabled)? true : false)]); ?>
		</td>
	</tr>
	<tr>
		<td><strong><?= __("Středa:") ?></strong></td>
		<td>
			<?= $this->Form->input("opening_hours.3.f", ["value" => (isset($data[3])? $data[3]["f"] : null), 'label' => false, "disabled" => (isset($disabled)? true : false)]); ?>
		</td>
		<td>
			<?= $this->Form->input("opening_hours.3.t", ["value" => (isset($data[3])? $data[3]["t"] : null), 'label' => false, "disabled" => (isset($disabled)? true : false)]); ?>
		</td>
		<td>
			<?= $this->Form->input("opening_hours.3.c", ["checked" => ((isset($data[3]) && $data[3]["c"])? true : false), 'label' => false, "disabled" => (isset($disabled)? true : false)]); ?>
		</td>
	</tr>
	<tr>
		<td><strong><?= __("Čtvrtek:") ?></strong></td>
		<td>
			<?= $this->Form->input("opening_hours.4.f", ["value" => (isset($data[4])? $data[4]["f"] : null), 'label' => false, "disabled" => (isset($disabled)? true : false)]); ?>
		</td>
		<td>
			<?= $this->Form->input("opening_hours.4.t", ["value" => (isset($data[4])? $data[4]["t"] : null), 'label' => false, "disabled" => (isset($disabled)? true : false)]); ?>
		</td>
		<td>
			<?= $this->Form->input("opening_hours.4.c", ["checked" => ((isset($data[4]) && $data[4]["c"])? true : false), 'label' => false, "disabled" => (isset($disabled)? true : false)]); ?>
		</td>
	</tr>
	<tr>
		<td><strong><?= __("Pátek:") ?></strong></td>
		<td>
			<?= $this->Form->input("opening_hours.5.f", ["value" => (isset($data[5])? $data[5]["f"] : null), 'label' => false, "disabled" => (isset($disabled)? true : false)]); ?>
		</td>
		<td>
			<?= $this->Form->input("opening_hours.5.t", ["value" => (isset($data[5])? $data[5]["t"] : null), 'label' => false, "disabled" => (isset($disabled)? true : false)]); ?>
		</td>
		<td>
			<?= $this->Form->input("opening_hours.5.c", ["checked" => ((isset($data[5]) && $data[5]["c"])? true : false), 'label' => false, "disabled" => (isset($disabled)? true : false)]); ?>
		</td>
	</tr>
	<tr>
		<td><strong><?= __("Sobota:") ?></strong></td>
		<td>
			<?= $this->Form->input("opening_hours.6.f", ["value" => (isset($data[6])? $data[6]["f"] : null), 'label' => false, "disabled" => (isset($disabled)? true : false)]); ?>
		</td>
		<td>
			<?= $this->Form->input("opening_hours.6.t", ["value" => (isset($data[6])? $data[6]["t"] : null), 'label' => false, "disabled" => (isset($disabled)? true : false)]); ?>
		</td>
		<td>
			<?= $this->Form->input("opening_hours.6.c", ["checked" => ((isset($data[6]) && $data[6]["c"])? true : false), 'label' => false, "disabled" => (isset($disabled)? true : false)]); ?>
		</td>
	</tr>
	<tr>
		<td><strong><?= __("Neděle:") ?></strong></td>
		<td>
			<?= $this->Form->input("opening_hours.7.f", ["value" => (isset($data[7])? $data[7]["f"] : null), 'label' => false, "disabled" => (isset($disabled)? true : false)]); ?>
		</td>
		<td>
			<?= $this->Form->input("opening_hours.7.t", ["value" => (isset($data[7])? $data[7]["t"] : null), 'label' => false, "disabled" => (isset($disabled)? true : false)]); ?>
		</td>
		<td>
			<?= $this->Form->input("opening_hours.7.c", ["checked" => ((isset($data[7]) && $data[7]["c"])? true : false), 'label' => false, "disabled" => (isset($disabled)? true : false)]); ?>
		</td>
	</tr>
</table>

<script type="text/javascript">
	window.addEvent('domready', function(){
		$$("#opening_hours input[type=checkbox]").addEvent("click", function(){
			if(this.checked){
				this.getParent("tr").getElements("input").set("value", "");
			}
		})
	});

</script>