<?php
echo '<ul class="pagination">';
echo $this->Paginator->prev('<span class="fa fa-angle-left"></span> ' . __('Předchozí'), ['escape' => false, 'class' => 'btn blue']);
echo '<span class="counter">'.$this->Paginator->counter('<span class="current">{{page}}</span>/{{pages}}').'</span>';
echo $this->Paginator->next(__('Další').' <span class="fa fa-angle-right"></span>', ['escape' => false]);
echo '</ul>';
