<?php
	/**
	 * @definition
	 * očekává data funkce RoutesTable->countShops($route_id);
	 **/
?>
<div class="row layout" id="count-shops">
	<?php
		echo '<h2>'. __("V trase jsou tyto provozovny").'</h2>';
		if(isset($data)){
			echo '<div class="col three">';
			echo '<table class="table-formated">';
			foreach ($data as $k => $c) {
				echo '<tr>';
				echo '<th style="width: 30%;">' . $shop_colors_plural[$k] . '</th>';
				echo '<td>' . $c . '</td>';
				echo '</tr>';
			}
			echo '</table>';
			echo '</div>';
		}
		else{
			echo '<div class="col twelve"><p>'.__("Trasa neobsahuje žádné provozy").'</p></div>';
		}
	?>
</div>