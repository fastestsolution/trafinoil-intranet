<?php
echo '<div class="tabs">';
echo '<ul class="modal_tabs">';
if (isset($load)) {
	foreach ($load AS $k => $p) {
		echo '<li ' . ((isset($tab) && $tab == $k) ? "id='backActive'" : null) . ' data-open="modal-tab-' . $k . '">';
		echo $p;
		echo '</li>';
	}
}
if (isset($ajax)) {
	foreach ($ajax AS $k => $p) {
		echo '<li ' . ((isset($tab) && $tab == $k) ? "id='backActive'" : null) . '
				class="ajax"
				data-href="' . ((isset($p["params"])) ? $this->Url->build(array_merge(["action" => $k], $p["params"])) : ["action" => $k]) . '"
				data-open="modal-tab-' . $k . '">';
		echo $p["name"];
		echo '</li>';
	}
}
echo '</ul>';

echo '<div class="clear"></div>';
echo '<div class="modal_contents">';
if (isset($load)) {
	foreach ($load AS $k => $p) {
		echo '<div class="modal_tab" id="modal-tab-' . $k . '">';
		echo $this->element('../' . $this->request->params['controller'] . '/modals/' . $k);
		echo '<div class="clear"></div>';
		echo '</div>';
	}
}
if (isset($ajax)) {
	foreach ($ajax AS $k => $p) {
		echo '<div class="modal_tab" id="modal-tab-' . $k . '">';
		//echo $this->element('../' . $this->request->params['controller'] . '/modals/' . $k);
		echo '<div class="clear"></div>';
		echo '</div>';
	}
}
echo '</div>';
echo '<div class="clear"></div>';
echo '</div>';