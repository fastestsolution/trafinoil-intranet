<div class="row">
  <h2><?= __("Dopravce") ?></h2>

	<div class="two col">
		<?= $this->Form->input("dealer_id", ['label' => __('Obchodník')]); ?>
  </div>
	<div class="clear"></div>
	<div class="two col">
    <?= $this->Form->input("ic", ['label' => __('IČO'), 'class' => 'find_ares']); ?>
  </div>
  <div class="two col">
    <?= $this->Form->input("c_name", ['label' => __('Zastoupen'), 'class' => 'ares_firma']); ?>
  </div>
  <div class="three col">
    <?= $this->Form->input("name", ['label' => __('Název dopravce'), 'class' => '']); ?>
  </div>
  <div class="two col">
    <?= $this->Form->input("shortcut", ['label' => __('Zkratka'), 'class' => '']); ?>
  </div>
  <div class="two col">
    <?= $this->Form->input("dic", ['label' => __('DIČ'), 'class' => 'ares_dic']); ?>
  </div>

	<div class="clear"></div>

  <h2><?= __("Adresa") ?></h2>
  <div class="three col">
    <?= $this->Form->input("street", ['label' => __('Ulice'), 'class' => 'ares_ulice']); ?>
  </div>
  <div class="three col">
    <?= $this->Form->input("town", ['label' => __('Město'), 'class' => 'ares_mesto']); ?>
  </div>
  <div class="three col">
    <?= $this->Form->input("psc", ['label' => __('PSČ'), 'class' => 'ares_psc']); ?>
  </div>
  <div class="three col">
    <?= $this->Form->input("country_id", ['label' => __('Stát'), 'class' => 'ares_stat', 'options' => Cake\Core\Configure::read("select_config.countries")]); ?>
  </div>

	<div class="clear"></div>

  <h2><?= __("Kontaktní informace") ?></h2>
  <div class="two col">
    <?= $this->Form->input("phone", ['label' => __('Telefon')]); ?>
  </div>
  <div class="two col">
    <?= $this->Form->input("mobile", ['label' => __('Mobil')]); ?>
  </div>
  <div class="two col">
    <?= $this->Form->input("email", ['label' => __('Email')]); ?>
  </div>
  <div class="two col">
    <?= $this->Form->input("www", ['label' => __('WWW')]); ?>
  </div>
  <div class="two col">
    <?= $this->Form->input("bank_account", ['label' => __('Číslo bank. účtu')]); ?>
  </div>
  <div class="two col">
    <?= $this->Form->input("spz", ['label' => __('SPZ')]); ?>
  </div>
  <div class="twelve col">
  <?= $this->Form->input("note", ['type' => 'textarea', 'label' => __('Poznámka')]); ?>
  </div>

	<div class="clear"></div>

  <h2><?= __("Region a sklad") ?></h2>
  <div class="two col">
    <?= $this->Form->input("region_id", ['label' => __('Region'), 'options' => []]); ?>
  </div>
  <div class="two col">
    <?= $this->Form->input("stock_id", ['label' => __('Sklad'), 'options' => []]); ?>
  </div>

  <div class="clear"></div>

  <h2><?= __("Náklad") ?></h2>
  <div class="two col">
    <?= $this->Form->input("cargo1", ['label' => __('Náklad 1')]); ?>
  </div>
  <div class="two col">
    <?= $this->Form->input("cargo2", ['label' => __('Náklad 2')]); ?>
  </div>
  <div class="two col">
    <?= $this->Form->input("cargo3", ['label' => __('Náklad 3')]); ?>
  </div>


</div>