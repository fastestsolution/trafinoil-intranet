<?php  
echo '<div class="layout">';
echo '<div class="top-actions">';
if (isset($params->topActions)){
	foreach($params->topActions AS $key=>$item){
		echo $this->Html->link($item, ['action' => $key], ['class' => 'btn', 'ajax' => true, "escape" => false]);
	}
}
if(isset($back)){
	echo $this->Html->link(__("Zpět"), $back, ["class" => "btn"]);
}
echo '</div>';
echo $this->element('../AutoTable/elements/filtr');

echo '<div id="fst_history">';
//pr($params);


if(isset($items) && $items->count() > 0){
	echo $this->element('../AutoTable/items');
} else {
	echo '<div class="noresult">'.__('Nenalezeny žádné záznamy').'</div>';
}

echo '</div>';
?>