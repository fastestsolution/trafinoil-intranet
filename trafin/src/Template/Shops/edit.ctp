<?php
if(isset($back)){
	echo $this->Html->link(__("Zpět"), $back, ['class' => 'btn']);
}

echo $this->element('modal',
	[
		'load'=>[
			//'zakladni'=>__('Základní'),
			'kontaktni'=>__('Kontaktní'),
			'provozni' => __("Provozní"),
			'systemove' => __("Systémové"),
			'obchodni' => __("Obchodní")
		]
	]
);
