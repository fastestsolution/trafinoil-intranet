<?php


    echo '<div class="row">';
    echo '<h2>'.__("Adresa").'</h2>';
      echo '<div class="twelve col">';
        echo $this->Form->input("Shops.name", ['label' => __("Provozovna")]);
      echo '</div>';
    echo '</div>';

    echo '<div class="row">';
      echo '<div class="six col">';
        echo $this->Form->input("Shops.street", ['label' => __("Ulice")]);
        echo $this->Form->input("Shops.town_part", ['label' => __("Část obce")]);
        echo $this->Form->input("Shops.region", ['label' => __("Okres")]);
      echo '</div>';
      echo '<div class="six col">';
        echo $this->Form->input("Shops.town", ['label' => __("Obec")]);
        echo $this->Form->input("Shops.psc", ['label' => __("PSČ")]);
      echo '</div>';
    echo '</div>';

    echo '<div class="row">';
      echo '<h2>'.__("Obchod (majitel/provozní)").'</h2>';
      echo '<div class="six col">';
        echo $this->Form->input("Shops.bcontact_name", ['label' => __("Jméno")]);
        echo $this->Form->input("Shops.bcontact_phone", ['label' => __("Telefon")]);
      echo '</div>';
      echo '<div class="six col">';
        echo $this->Form->input("Shops.bcontact_mobile", ['label' => __("Mobil")]);
        echo $this->Form->input("Shops.bcontact_email", ['label' => __("Email")]);
      echo '</div>';
    echo '</div>';

    echo '<div class="row">';
      echo '<h2>'.__("Výdej").'</h2>';
      echo '<div class="six col">';
        echo $this->Form->input("Shops.contact_name", ['label' => __("Jméno")]);
      echo '</div>';
      echo '<div class="six col">';
        echo $this->Form->input("Shops.contact_mobile", ['label' => __("Telefon")]);
      echo '</div>';
    echo '</div>';

    echo '<div class="row">';
      echo '<h2>'.__("Další informace").'</h2>';
      echo '<div class="six col">';
        echo $this->Form->input("Shops.www", ['label' => __("WWW")]);
        echo $this->Form->input("Shops.pact_date", ['label' => __("Datum smlouvy"), 'type' => 'text']);
        echo $this->Form->input("Shops.payment_type", ['label' => __("Způsob platby"), 'options' => \Cake\Core\Configure::read("select_config.payment_type")]);


        echo $this->Form->input("Shops.iscc", ['label' => __("ISCC")]);
      echo '</div>';
      echo '<div class="six col">';
        echo $this->Form->input("Shops.source", ['label' => __("Původ"), 'options' => \Cake\Core\Configure::read("select_config.shop_source")]);
        echo $this->Form->input("Shops.user_id", ['label' => __("Obchodník"), 'options' => $users]);
      echo '</div>';
    echo '</div>';

    echo '<div class="row">';
      echo '<h2>'.__("GPS souřadnice").$this->Html->link('<span class="fa fa-pencil clickable" id="gps-button"></span>', "#", ['escape' => false]).'</h2>';
      echo '<div class="twelve col map-container">';
        echo '<div id="map" style="width: 100%; height: 400px;"></div>';
      echo '</div>';
      echo '<div class="six col">';

      echo '</div>';
      echo '<div class="six col">';

      echo '</div>';
    echo '</div>';

    echo '<div class="row">';
      echo '<h2>'.__("Počet - Obaly").'</h2>';
      echo '<div class="six col">';
        echo $this->Form->hidden("Shops.shop_packs.0.commodity_id", ['value' => 33]);
        echo $this->Form->input("Shops.shop_packs.0.quan", ['label' => __("Kohout"), 'options' => \Cake\Core\Configure::read("select_config.numbers")]);

        echo $this->Form->hidden("Shops.shop_packs.1.commodity_id", ['value' => 43]);
        echo $this->Form->input("Shops.shop_packs.1.quan", ['label' => __("Barel 30l"), 'options' => \Cake\Core\Configure::read("select_config.numbers")]);
      echo '</div>';
      echo '<div class="six col">';
        echo $this->Form->hidden("Shops.shop_packs.2.commodity_id", ['value' => 26]);
        echo $this->Form->input("Shops.shop_packs.2.quan", ['label' => __("Barel 50l"), 'options' => \Cake\Core\Configure::read("select_config.numbers")]);

        echo $this->Form->hidden("Shops.shop_packs.3.commodity_id", ['value' => 44]);
        echo $this->Form->input("Shops.shop_packs.3.quan", ['label' => __("IBC 1000l"), 'options' => \Cake\Core\Configure::read("select_config.numbers")]);
      echo '</div>';
    echo '</div>';
?>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBwwXsW7TpwLuyhF46VwYUHigo8a1COdI4" async defer></script>
<script type="text/javascript">

  var map;
  function initMap() {
    $$(".map-container").toggle();

    if($$(".map-container").isDisplayed()) {
      map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 49.6280138, lng: 16.7071967},
        zoom: 8,
        draggableCursor: 'crosshair'
      });

      $("gps-button").addClass("active");
      initGetGps();
    }
    else{
      $("gps-button").removeClass("active");
      map = null;
    }
  }

  window.addEvent('domready', function(){

    $("gps-button").addEvent("click", function(){
      initMap();
    });

    var myAutocompleter = new GooCompleter('shops-find-company', {
      action: '/companies/find/',	// JSON source
      param: 'search',			// Param string to send
      minlen: 0,
      listbox_offset: { y: 2 },	// Listbox offset for Y
      delay: 200,					// Request delay to 0.5 seconds
      fire_event : function(opt){
        $("shop-company-id").value = opt.key;
      }
    });
  });
</script>

