<?php
	echo $this->Form->create($entity);
	if(isset($back)){
		echo $this->Form->hidden("back", ['value' => $back]);
	}
	echo $this->Form->hidden("id");
?>

	<div class="row">
		<h2><?= __("Firma") ?></h2>
		<div class="ten col">
			<?= $this->Form->input("Shops.find_company",
				['placeholder' => __("Zadejte název nebo IČO..."), 'label' => __("Firma"), "value" => ((isset($shop["company"]["name"]))? $shop["company"]["name"] : null )])
			?>
			<?= $this->Form->hidden("Shops.company_id", ['label' => __("Firma"), "id" => "shop-company-id"]) ?>
		</div>
		<div class="two col poss">
			<?= $this->Html->link('<span class="fa fa-plus"></span>',
				["controller" => "companies", "action" => "edit", ((isset($shop->id))? $shop->id : null), 'back' => "/".$this->request->url],
				['escape' => false, 'id' => 'find-company', 'title' => __("Přidat firmu")])
			?>
		</div>
	</div>

	<div class="row">
		<div class="six col">
			<?= $this->Form->input("Shops.name", ['label' => __("Provozovna")]) ?>
			<?= $this->Form->input("Shops.color", ['label' => __("Barva"), 'options' => Cake\Core\Configure::read("select_config.shop_colors")]) ?>
			<?= $this->Form->input("Shops.currency", ['label' => __("Měna")]) ?>
		</div>
		<div class="six col">
			<?php
				echo $this->Form->input("Shops.supplier_id", ['label' => __("Dodavatel"), 'options' => $suppliers, 'empty' => __('Nevybráno')]);
				echo $this->Form->input("Shops.franchise_id", ['label' => __("Franšíza")]);
				echo $this->Form->input("Shops.carrier_id", ['label' => __("Dopravce")]);
			?>
		</div>
	</div>


<?php
	echo $this->Form->button(__("Uložit"), ["class" => "btn", "id" => "SaveModal"]);
	echo $this->Form->end();
?>