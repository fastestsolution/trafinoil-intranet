<?php
	echo $this->Form->create($entity);
	if(isset($back)){
		echo $this->Form->hidden("back", ['value' => $back]);
	}
	echo $this->Form->hidden("id");
?>
	<div class="twelve col">
		<?= $this->Form->input("Shops.name", ['label' => __("Provozovna")]) ?>
	</div>

	<div class="row">
		<div class="eight col inner">
			<div class="row">
				<div class="six col">
					<?= $this->element("Shop/kontakt_obchod"); ?>
				</div>

				<div class="six col">
					<?= $this->element("Shop/kontakt_vydej"); ?>
				</div>
			</div>
			<div class="row">
				<div class="six col">
					<?php
						echo $this->Form->input("Shops.street", ['label' => __("Ulice")]);
						//echo $this->Form->input("Shops.town_part", ['label' => __("Část obce")]);
						echo $this->Form->input("Shops.region", ['label' => __("Okres")]);
					?>
				</div>
				<div class="six col">
					<?php
						echo $this->Form->input("Shops.town", ['label' => __("Obec")]);
						echo $this->Form->input("Shops.psc", ['label' => __("PSČ")]);
					?>
				</div>
			</div>
		</div>
		<div class="four col">
			<div class="twelve col">
				<?= $this->element("Shop/opening_hours", ["data" => $entity["opening_hours"]]); ?>
			</div>
		</div>
	</div>


<div class="row">
	<div class="col twelve tcenter mtop20">
	<?php
		echo $this->Form->button(__("Uložit"), ["class" => "btn", "id" => "SaveModal"]);
		echo $this->Form->end();
	?>
	</div>
</div>
