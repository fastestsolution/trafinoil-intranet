<?php

	echo $this->Form->create($entity);
	if(isset($back)){
		echo $this->Form->hidden("back", ['value' => $back]);
	}
	echo $this->Form->hidden("id");

	?>

	<div class="twelve col">
		<?= $this->Form->input("Shops.name", ['label' => __("Provozovna")]) ?>
	</div>

<div class="row">
	<div class="eight col inner">
		<div class="row">
			<div class="six col">
				<?= $this->element("Shop/kontakt_obchod"); ?>
			</div>

			<div class="six col">
				<?= $this->element("Shop/kontakt_vydej"); ?>
			</div>
		</div>
		<div class="row">
			<div class="six col">
				<?php
					echo $this->Form->input("Shops.street", ['label' => __("Ulice")]);
					echo $this->Form->input("companies.ic", ['label' => __("IČ firmy")]);
					echo $this->Form->input("companies.dic", ['label' => __("DIČ firmy")]);
					echo $this->Form->input("Shops.region", ['label' => __("Okres")]);
				?>
			</div>
			<div class="six col">
				<?php
					echo $this->Form->input("Shops.town", ['label' => __("Obec")]);
					echo $this->Form->input("Shops.psc", ['label' => __("PSČ")]);
					echo $this->Form->input("Shops.lat", ['label' => __("Severní šířka")]);
					echo $this->Form->input("Shops.lng", ['label' => __("Východní délka")]);
				?>
			</div>
			<div class="twelve col">
				<?php
					echo $this->Form->input("Shops.note", ['label' => __("Poznámka")]);
				?>
			</div>
		</div>
	</div>
</div>



<?php

	echo $this->Form->button(__("Uložit"), ["class" => "btn", "id" => "SaveModal"]);
	echo $this->Form->end();