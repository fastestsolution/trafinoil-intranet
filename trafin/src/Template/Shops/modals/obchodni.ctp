<div class="row">
  <h2>Špinavý olej</h2>
  <?= $this->Form->hidden("Shops.shop_potentials.0.commodity_id", ['value' => __("1")]); ?>
  <div class="four col">
    <?= $this->Form->input("Shops.shop_potentials.0.quan", ['label' => __('Množství')]); ?>
  </div>
  <div class="four col">
  <?= $this->Form->input("Shops.shop_potentials.0.price", ['label' => __('Cena')]); ?>
  </div>
  <div class="four col">
    <?= $this->Form->input("Shops.shop_potentials.0.freq", ['label' => __('Frekvence'), 'options' => \Cake\Core\Configure::read("select_config.freq")]); ?>
  </div>
  <div class="row">
    <div class="twelve col">
    <?= $this->Form->input("Shops.shop_potentials.0.note", ['label' => __('Poznámka')]); ?>
    </div>
  </div>
</div>



<div class="row">
  <h2>Čistý olej</h2>
  <div class="row">
    <div class="one col">
      <span class="label">Řepka:</span>
      <?= $this->Form->hidden("Shops.shop_potentials.1.commodity_id", ['value' => __("1")]); ?>
      <?= $this->Form->hidden("Shops.shop_potentials.1.commodity_type_id", ['value' => __("1")]); ?>
    </div>
    <div class="two col">
      <?= $this->Form->input("Shops.shop_potentials.1.quan", ['label' => __('Množství')]); ?>
    </div>
    <div class="two col">
      <?= $this->Form->input("Shops.shop_potentials.1.price", ['label' => __('Cena')]); ?>
    </div>
    <div class="two col">
      <?= $this->Form->input("Shops.shop_potentials.1.freq", ['label' => __('Frekvence'), 'options' => \Cake\Core\Configure::read("select_config.freq")]); ?>
    </div>
    <div class="five col">
      <?= $this->Form->input("Shops.shop_potentials.1.note", ['label' => __('Poznámka')]); ?>
    </div>
  </div>

  <div class="row">
    <div class="one col">
      <span class="label">Sluneč:</span>
      <?= $this->Form->hidden("Shops.shop_potentials.2.commodity_id", ['value' => __("2")]); ?>
      <?= $this->Form->hidden("Shops.shop_potentials.2.commodity_type_id", ['value' => __("1")]); ?>
    </div>
    <div class="two col">
      <?= $this->Form->input("Shops.shop_potentials.2.quan", ['label' => __('Množství')]); ?>
    </div>
    <div class="two col">
      <?= $this->Form->input("Shops.shop_potentials.2.price", ['label' => __('Cena')]); ?>
    </div>
    <div class="two col">
      <?= $this->Form->input("Shops.shop_potentials.2.freq", ['label' => __('Frekvence'), 'options' => \Cake\Core\Configure::read("select_config.freq")]); ?>
    </div>
    <div class="five col">
      <?= $this->Form->input("Shops.shop_potentials.2.note", ['label' => __('Poznámka')]); ?>
    </div>
  </div>

  <div class="row">
    <div class="one col">
      <span class="label">palma:</span>
      <?= $this->Form->hidden("Shops.shop_potentials.3.commodity_id", ['value' => __("3")]); ?>
      <?= $this->Form->hidden("Shops.shop_potentials.3.commodity_type_id", ['value' => __("1")]); ?>
    </div>
    <div class="two col">
      <?= $this->Form->input("Shops.shop_potentials.3.quan", ['label' => __('Množství')]); ?>
    </div>
    <div class="two col">
      <?= $this->Form->input("Shops.shop_potentials.3.price", ['label' => __('Cena')]); ?>
    </div>
    <div class="two col">
      <?= $this->Form->input("Shops.shop_potentials.3.freq", ['label' => __('Frekvence'), 'options' => \Cake\Core\Configure::read("select_config.freq")]); ?>
    </div>
    <div class="five col">
      <?= $this->Form->input("Shops.shop_potentials.3.note", ['label' => __('Poznámka')]); ?>
    </div>
  </div>

  <div class="row">
    <div class="one col">
      <span class="label">Mix:</span>
      <?= $this->Form->hidden("Shops.shop_potentials.4.commodity_id", ['value' => __("4")]); ?>
      <?= $this->Form->hidden("Shops.shop_potentials.4.commodity_type_id", ['value' => __("1")]); ?>
    </div>
    <div class="two col">
      <?= $this->Form->input("Shops.shop_potentials.4.quan", ['label' => __('Množství')]); ?>
    </div>
    <div class="two col">
      <?= $this->Form->input("Shops.shop_potentials.4.price", ['label' => __('Cena')]); ?>
    </div>
    <div class="two col">
      <?= $this->Form->input("Shops.shop_potentials.4.freq", ['label' => __('Frekvence'), 'options' => \Cake\Core\Configure::read("select_config.freq")]); ?>
    </div>
    <div class="five col">
      <?= $this->Form->input("Shops.shop_potentials.4.note", ['label' => __('Poznámka')]); ?>
    </div>
  </div>
</div>

<div class="row">
	<h2>Konkurence</h2>
	<div class="col three">
    <?= $this->Form->input("Shops.rival", ['label' => __("Konkurence")]); ?>
	</div>
</div>