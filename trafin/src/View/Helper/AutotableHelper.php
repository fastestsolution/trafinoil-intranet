<?php

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;
use Cake\ORM\TableRegistry;

class AutotableHelper extends Helper
{

  public $helpers = ['Paginator', 'Form'];
  public $tableth = "";
  private $cname = [];

  public function th($ckey, $cvalue = null){
    if(!is_array($cvalue)){
      $this->tableth .= '<th class="autotable-'.$ckey.'">'.$this->Paginator->sort($ckey, self::translateCol($ckey)).'</th>';
    }
    else{
      foreach($cvalue as $k => $v) {
        self::th($ckey.".".$k, $v);
      }
    }
  }

  public function td($cvalue,$params = null){
    //pr($params['list']);
	
		if(!is_array($cvalue)){
      echo '<td>'.$cvalue.'</td>';
    }
    else{
      foreach($cvalue as $k=>$v) {
				if (isset($params->listValues[$k])){
					if (isset($params->listValues[$k][$v])){
						$v = $params->listValues[$k][$v];

					} else {
						$v = __('Nevybráno');
					}
				}
				self::td($v);
      }
    }

	  // pokud budeme dělat nějaké post_query
	  if(isset($params->postCols)){
		  foreach($params->postCols as $i){
			  echo '<td>';
			    echo $this->postQuery($i, $cvalue);
			  echo '</td>';
		  }
	  }

  }


  // překládá název sloupce pokud je překlad vyplněn v config/autotable_translate.php kde je to děláno skrze get text.
  // může tam být jak překlad sloupce všeobecně, tak překlad sloupce konkrétně pro nějaký Model
  // Pokud ani to nepomůže, můžeš si ty překlady přepsat ve volání helperu, ale musíš si to trochu poupravit.
  // tzn. nastavíš si třeba proměnnou v controlleru a přidáš si ji jako parametr do volání fce th();
  private function translateCol($ckey)
  {
    if ($this->config("colTranslate." . @key($this->request->paging).".".$ckey)) {
      return $this->config("colTranslate." . @key($this->request->paging).".".$ckey);
    }
    else if($this->config("colTranslate.".$ckey)){
      return $this->config("colTranslate." . $ckey);
    }
    else{
      return $ckey;
    }
  }

	// některá data nelze dostat do tabulky normálním dotazem. Použij post_cols v nastavení view_index v controlleru.
	// například 'post_cols' => [0 => ['name' => 'CarrierRoutes', 'method' => 'Routes.CarriersWithIds']]

	// @method = název metody ve formátu Table.customFinder
	// @row = aktuální řádek který se vypisuje. Můžeš to ve finderu použít pro podmínky atd..
	private function postQuery($method, $row = null){
		$method = explode(".", $method);
		$table = TableRegistry::get($method[0]);

		echo $table->find($method[1], $row);
	}

	public function filter($params){
		foreach($params as $k => $p){
			echo $this->Form->input($p["model"].".".$k,
				[
					"label" => $p["name"],
				  "placeholder" => $p["name"],
					"class" => "text fst_h autotable-filter-$k",
					"data-fparams" => $p["model"]."__".$k,
					"empty" => ((isset($p["empty"]))? $p["empty"] : __("---")),
					"options" => ((isset($p["options"]))? $p["options"] : null)
				]
			);
		}
	}


}