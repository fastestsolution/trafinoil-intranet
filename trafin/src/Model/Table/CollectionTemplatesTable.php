<?php
namespace App\Model\Table;

use App\Model\Entity\User;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

use Cake\Network\Session;
use Cake\I18n\Date;

class CollectionTemplatesTable extends Table
{
	public function initialize(array $config)
	{
		parent::initialize($config);
		$this->belongsTo("Routes");
		$this->belongsTo("Carriers");
	}

	public function replicate($templates, $startDate){
		if(count($templates)){
			$ct = TableRegistry::get("Collections");
			$daysMove = 1;

			$date = new Date($startDate);
			for($i = 1; $i <= 28; $i++){
				if(!$this->is_badDay($date->format("N"))){
					foreach($templates as $template){
						if($template->day == $daysMove){
							$data["route_id"] = $template["route_id"];
							$data["carrier_id"] = $template["carrier_id"];
							$data["user_id"] = $template["user_id"];
							$data["created"] = new Time();
							$data["date_plan"] = $date;
							$data["status"] = 0;
							$entity = $ct->newEntity($data);
							$ct->save($entity);
						}
					}
					$daysMove++;
				}

				$date->addDay();
			}
		}

		return true;
	}

	public function is_badDay($dayInWeek){
		if($dayInWeek < 6){
			return false;
		}
		else {
			return true;
		}
	}
}
