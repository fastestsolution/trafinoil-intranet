<?php
namespace App\Model\Table;

use App\Model\Entity\User;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

use Cake\Network\Session;

class RouteShopsTable extends Table
{

  public $exportTable = "prov_trasy";
  public $importTable = "route_shops";
	public $exportPrimaryKey = null;

  public $exportSetting = [
    'trasa_id' => 'route_id',
    'pro_id' => 'shop_id',
    'poradi' => 'ord',
    'vlozeno' => 'created',
    'zruseno' => 'kos',
  ];

	public $exportTransValues = ['zruseno'];

	public function zruseno($value){
		if($value == "0000-00-00 00:00:00"){
			return null;
		}
		else{
			return $value;
		}
	}





  public function initialize(array $config)
  {
	  $this->belongsTo("Shops");
    //$this->addBehavior('Synchronize');
  }

}
