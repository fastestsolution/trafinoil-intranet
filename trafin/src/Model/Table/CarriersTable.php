<?php
namespace App\Model\Table;

use App\Model\Entity\User;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

use Cake\Network\Session;

class CarriersTable extends Table
{

  public $exportTable = "sez_dopr";
  public $importTable = "carriers";
	public $exportPrimaryKey = "dop_id";
//
  public $exportSetting = [
    'dop_id' => 'id',
//  'dop_idx' => 0,
//    'dop_ipvzn' => 192.168.1.106,
//    'dop_dcvzn' => 17.08.2010 10:00,
//    'dop_jmvzn' => Tlustikova,
//    'dop_ipzme' => 192.168.1.1,
//    'dop_dczme' => 14.02.2011 14:41,
//    'dop_jmzme' => Tlustikova,
    'dop_typ' => 'type',
    'dop_stav' => 'status',
    'dop_nazev' => 'c_name',
    'dop_zkratka' => 'shortcut',
    'dop_jmeno' => 'name',
    'dop_ulice' => 'street',
    'dop_mesto' => 'town',
    'dop_psc' => 'psc',
    'dop_ico' => 'ic',
    'dop_dic' => 'dic',
    'dop_tel' => 'phone',
//    'dop_fax' => ,
    'dop_mobil' => 'mobile',
    'dop_email' => 'email',
    'dop_web' => 'www',
    'dop_pozn' => 'note',
//    'dop_okresy' => '',
//    'dop_sklad_aid' => '',
//    'dop_bydliste_aid' =>,
    'dop_driver_id' => 'driver_id',
    'dop_region_id' => 'region_id',
    'dop_driver_interval' => 'driver_interval',
    'dop_driver_synctime' => 'driver_synctime',
    'dop_driver_resetdb' => 'driver_resetdb',
    'dop_driver_fullsync' => 'driver_fullsync',
    'dop_sklad_id' => 'stock_id',
//    'dop_dispecer' => ,
//    'dop_test' => 0,
    'dop_scanner_force' => 'scanner_force',
    'dop_naklad1' => 'cargo1',
    'dop_naklad2' => 'cargo2',
    'dop_naklad3' => 'cargo3',
    'dop_driver_resetpremises' => 'driver_resetpremises',
    'dop_redirect' => 'redirect',
//    'dop_obchodnik_id' => '',
  ];



  public function initialize(array $config)
  {
		$this->belongsTo("Stocks");
	  $this->belongsTo("Regions");
  }

	public function findCarriers($query){
		return $query;
	}

  public function validationDefault(Validator $validator)
  {
    $validator
      ->requirePresence('name', 'create',   __("Název společnosi je povinný"))
      ->notEmpty('name')
	    ->notEmpty('shortcut');


    return $validator;
  }

  // hash certifikátu
  public function getCarrierByHash($hash){
	  $CTable = TableRegistry::get("Carriers");
	  $c = $CTable->find()->Contain("Regions")->where(["driver_id" => $hash])->first();

	  if(!$c){
	    Throw new \Exception("Dopravce nenalezen");
    }

    return $c;

	}

}
