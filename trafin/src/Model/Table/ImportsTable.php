<?php
namespace App\Model\Table;

use Cake\Core\Configure;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

use Cake\Network\Session;


// DŮLEŽITÉ!! - KOMUNIKACI MEZI JEDNOTLIVÍMI AKCEMI ŘÍDÍ CONTROLLER, MRKNI DO IMPORTSCONTROLLERU, NASTAVUJE I JAKÉ CONNECITON SE MAJÍ POUŽÍT ATD...
// Martin Hrabal 7.4.2016
class ImportsTable extends Table
{
  // Upraví názvy sloupců dle toho, jestli se provádí import nebo export, do pole a uloží sloupce, které se budou ignorovat
  // data: array fetchnutá data
  // tableEntity: Instance loadModel()
  public function trans($data, $tableEntity, $reverse = false){
    $return = array();
	  $error = [];

    if($reverse == true){
      $tableEntity->exportSetting = array_flip($tableEntity->exportSetting);
    }

    foreach($data as $kl => $keys){
      foreach($keys as $k => $v){
        if(isset($tableEntity->exportSetting[$k])){
	        // pokud je pole definováno, že potřebuje převézt hodnoty
	        if(isset($tableEntity->exportTransValues) && in_array($k, $tableEntity->exportTransValues)){
		        $return[$kl][$tableEntity->exportSetting[$k]] = $tableEntity->$k($v);
	        }
	        else{
		        $return[$kl][$tableEntity->exportSetting[$k]] = $v;
	        }
        }
        else{
          $error[$k] = $v;
        }
      }
    }
    return ["d" => $return, "e" => $error];
  }

  // vybere data. jednotlivé options se píšou v sql.
  // tableEntity: Instance loadModel()
  // connection instance ConnectionManager default nebo old (old = původní dispečer)
  // options array podmínek a omezení pro query
  public function getData($tableEntity, $connection, $options = null){

    if(!isset($options["limit"])){
      $options["limit"] = 20;
    }

	  $query = $connection->newQuery()->select('*')->from($tableEntity->exportTable);

	  if(isset($options["where"])){
		  $query->where($options["where"]);
	  }
	  if(isset($options["limit"])){
		  $query->limit($options["limit"]);
	  }
	  if(isset($options["offset"])){
		  $query->offset($options["offset"]);
	  }

	  $data = $connection->execute($query)->fetchAll("assoc");
    if(count($data) > 0) {
      return $this->trans($data, $tableEntity);
    }
    else{
      return false;
    }
  }


  // table: název tabulky
  // connection: instance ConnectionManager default nebo old (old = původní dispečer)
  // data: data která prošla funkcí trans a mají tedy přepsané názvy sloupců dle toho kam se mají uložit
  public function setData($table, $connection, $data){
	  $ins = TableRegistry::get($table);
		$keys = array_keys($data[0]);
	  $query = $ins->query()->insert($keys);
	  $onDuplicate = "";

	  foreach($keys as $k => $v){
		  $onDuplicate .= $v."=values($v)".(($k+1) < (count($keys))? "," : null);
	  }

	  foreach($data as $dat){
		  $query->values($dat);
	  }



	  $query->epilog('ON DUPLICATE KEY UPDATE '.$onDuplicate)->execute();
	  unset($data);
	  return true;

	  //pr($d);
	  //$connection->save($table, $data);

  }
//    foreach($data as $k => $dat){
//      $exist = 0;
//	    $tr = TableRegistry::get($table);
//
//	    if($tr->exportPrimaryKey){
//		    $primKey = array_keys($dat)[0];
//		    $primVal = array_values($dat)[0];
//		    // pokud je id větší jak nula tak se zeptáme, zda už existuje
//		    if ($primVal > 0) {
//			    $query = "select " . $primKey . " from " . $table . " where " . $primKey . " = ?";
//			    $exist = $connection->execute($query, [$primVal])->count();
//		    }
//	    }
//
//      if($exist == 0){
//	      $connection->insert($table, $dat);
//      }
//      else{
//				$connection->update($table, $dat, [$primKey => $primVal]);
//      }
//    }
//    return true;
//  }
}
