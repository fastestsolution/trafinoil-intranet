<?php
namespace App\Model\Table;

use App\Model\Entity\User;
use Aura\Intl\Exception;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

use Cake\Network\Session;

class DriverImportsTable extends Table
{

  public $collection_id;
  public $carrier_id = 17;

  /*
   * @TODO potřebujeme carrier_id získat skrze cerfitikát
   * */
  public function importDeliverySubmit($data, $name = null){

    $DCT = TableRegistry::get("DriverCollections");
    $DCIT = TableRegistry::get("DriverCollectionItems");
    $CoT = TableRegistry::get("Commodities");
    $CaT = TableRegistry::get("Carriers");
    $DCITT = TableRegistry::get("DriverCollectionItemTrades");

    foreach($data as $key => $item){

      if(is_array($item)){
        $item = (object) $item;
      }

      $this->collection_id = $item->cartage_id;

      if(!isset($item->cartage_id)){
        $item->cartage_id = 0;
      }
      if(!isset($driverCollection) || (isset($driverCollection) && $item->cartage_id != $driverCollection["collection_id"])) {
        $driverCollection = $DCT->find()->where(["collection_id" => $item->cartage_id])->select(["id", "date_start", "collection_id", "carrier_id"])->first();
      }


      if(!$driverCollection){
        // vlastní svoz řidiče
        if($item->cartage_id == 0){
          $driverCollection = $DCT->find()->where(["remote_id" => $item->cartage_local_id])->select(["id", "date_start", "collection_id", "carrier_id"])->first();
          if(!$driverCollection) {
            $CT = TableRegistry::get("Collections");
            $collection = $CT->newEntity(array("carrier_id" => $this->carrier_id, "date_realization" => $item->time));
            $CT->save($collection);
            $item->cartage_id = $collection->id;
          }
        }

        if(!$driverCollection) {
          if (isset($name)) {
            $carrier_id = @ltrim(substr(substr($name, 22), 0, 3), '0');
          }
          $driverCollection = $DCT->newEntity();
          $driverCollection->collection_id = $item->cartage_id;
          $driverCollection->carrier_id = $this->carrier_id;
          $driverCollection->date_start = $item->time;
          $driverCollection->status = 1;
          $driverCollection->remote_id = $item->cartage_local_id;
          $DCT->save($driverCollection);
        }
      }

      $country = $CaT->find()->where(["Carriers.id" => $driverCollection->carrier_id])->select(["id" => "Regions.country_id"])->hydrate(false)->contain("Regions")->first();
      $cd_item = $DCIT->newEntity();


      // přepis svozu
      $rewrite = false;
      if (isset($item->pickup_record_local_id) && $item->pickup_record_local_id > 0)
      {
        $test = $DCIT->find()->where(["collection_id" => $driverCollection->collection_id, "remote_pickup_record_id" => $item->pickup_record_local_id])->first();
        if(count($test)){
          $DCIT->patchEntity($cd_item, $test->toArray());
          $rewrite = true;
        }
      }
      if ($rewrite == false &&  isset($item->premise_local_id) && $item->premise_local_id > 0)
      {
        $test = $DCIT->find()->where(["collection_id" => $driverCollection->collection_id, "remote_premise_id" => $item->premise_local_id])->first();
        if (count($test))
        {
          $DCIT->patchEntity($cd_item, $test->toArray());
          $rewrite = true;
        }
      }

      if (!$rewrite)
      {
        $prev_zast = $DCIT->find()->where(["collection_id" => $driverCollection->collection_id])->count();
        $cd_item->poradi = $prev_zast + 1;
      }

      /****** konec přepis svozu **/

      $cd_item->driver_collection_id = $driverCollection->id;
      $cd_item->collection_id = $driverCollection->collection_id;
      $cd_item->carrier_id = $driverCollection->carrier_id;
      $cd_item->time = new Time($item->time);
      $cd_item->shop_id = isset($item->premise_id) ? $item->premise_id : 0;
      if(isset($item->premise_id)) {
        $cd_item->dph_payer = $this->isPayer($item->premise_id);
      }
      if(isset($item->locationLatitude)) {
        $cd_item->lat = $item->locationLatitude;
      }
      if(isset($item->locationLongitude)) {
        $cd_item->lng = $item->locationLongitude;
      }
      if(isset($item->dispatcherNote)) {
        $cd_item->note = $item->dispatcherNote;
      }
      if (isset($item->pickup_record_local_id)) {
        $cd_item->remote_pickup_record_id = $item->pickup_record_local_id;
      }
      $cd_item->uhrazene_pohledavky = $item->paidClaim;
      $cd_item->cislo_dokladu = isset($item->documentNumber)? $item->documentNumber : '';

      $cd_item->prodej_faktura = $item->isSellInvoiced == 'true' ? 1 : 0;
      $cd_item->vykup_faktura = $item->isBuyoutInvoiced == 'true' ? 1 : 0;
      $cd_item->status = \Cake\Core\Configure::read("select_config.driver_collection_item.handled.code.".$item->handled);

      if (isset($item->reasonNotBuy)) {
        $cd_item->duvod = $item->reasonNotBuy;
      }
      $cd_item->prijato = new Time();
      $cd_item->remote_id = isset($item->cartage_local_id)? $item->cartage_local_id : 0;
      if (isset($item->incomingScans)) {
        $cd_item->incoming_scans = $item->incomingScans;
      }
      if (isset($item->outgoingScans)) {
        $cd_item->outgoing_scans = $item->outgoingScans;
      }

      $map = array(
        'incomeCashNumber'=>'cdo_pokladna_prijem',
        'expenseCashNumber'=>'cdo_pokladna_vydej',
        'incomeInvoiceNumber'=>'cdo_faktura_prijem',
        'expenseInvoiceNumber'=>'cdo_faktura_vydej',
        'expenseCashDebtNumber'=>'cdo_pokladna_dluh',
        'paidCommitment'=>'uhrazene_zavazky'
      );
      foreach($map as $remote => $local) {
        if (isset($item->$remote)) {
          $cd_item->$local = $item->$remote;
        }
      }

      $DCIT->save($cd_item);

      if($rewrite){
        $DCITT->query()->delete()->where(["driver_collection_item_id" => $cd_item->id])->execute();
      }

      if(!empty($item->sells)){
        $this->proc_trades(2, $item->sells, $cd_item->id, $country["id"], $driverCollection->date_start);
      }
      if(!empty($item->buyouts)){
        $this->proc_trades(1, $item->buyouts, $cd_item->id, $country["id"], $driverCollection->date_start, $cd_item->shop_id);
      }
    }

  }

  //trade_type 1 prodej, 2 výkup;
  public function proc_trades($trade_type, $trades, $cdItem_id, $country_id, $date_start, $shop_id = null){

    $DCT = TableRegistry::get("DriverCollections");
    $DCIT = TableRegistry::get("DriverCollectionItems");
    $CoT = TableRegistry::get("Commodities");
    $CaT = TableRegistry::get("Carriers");
    $DCITT = TableRegistry::get("DriverCollectionItemTrades");

    foreach ($trades as $trade)
    {

      if(is_array($trade)){
        $trade = (object) $trade;
      }

      $sell_item = $DCITT->newEntity();
      $sell_item->driver_collection_item_id = $cdItem_id;
      $sell_item->type = $trade_type;
      $sell_item->commodity_id = $trade->product_id;
      $sell_item->amount = $trade->amount;
      $sell_item->price = $trade->price * $trade->amount;

      $dph_type = $CoT->getCountryCommodity($trade->product_id, $country_id)->select("dph")->first();

      if($trade_type == 1){
        $isPayer = $this->isPayer($shop_id);
        if($isPayer == true){
          $sell_item->dph = $this->getDph($country_id, $dph_type["dph"], $date_start);
        }
        else{
          $sell_item->dph = 0;
        }
      }
      else{
        $sell_item->dph = $this->getDph($country_id, $dph_type["dph"], $date_start);
      }


      $DCITT->save($sell_item);
    }
  }

  public function getDph($country_id, $commodity_dph, $date = null){
    $dpht = TableRegistry::get("Dphs");
    $query = $dpht->find()->where(["country_id" => $country_id, "type" => $commodity_dph]);

    if(isset($date)){
      $query->where(["valid_from <=" => $date])->where(["OR" => ["valid_to >=" => $date, "valid_to IS NULL"]]);
    }
    else{
      $query->where(["valid" => 1]);
    }

    $res = $query->select("val")->first();
    return $res["val"];
  }

  public function isPayer($shop_id){
    $st = TableRegistry::get("Shops");
    $shop = $st->find()->where(["Shops.id" => $shop_id])->contain(["Companies"])->select("Companies.dph")->first();
    if(!isset($shop["Companies"]["dph"])){
      $this->log(__("DphComponent->isPayer() Nepodařilo se ověřit plátce dph pro provoz")." ".$shop_id);
    }
    if($shop["Companies"]["dph"] == 1){
      return true;
    }
    else{
      return false;
    }
  }

  public function log($message){
    $dct = TableRegistry::get("DriverCollectionLogs");
    $log = $dct->newEntity(array("message" => $message, "collection_id" => $this->collection_id));
    $dct->save($log);
  }

}