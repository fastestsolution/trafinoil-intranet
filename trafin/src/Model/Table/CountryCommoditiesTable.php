<?php
namespace App\Model\Table;

use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

use Cake\Network\Session;

class CountryCommoditiesTable extends Table
{

	/*** NASTAVENÍ EXPORTU ****/

	public $exportTable = "komodita_stat";
	public $importTable = "country_commodities";
	public $exportPrimaryKey = false;

	// definuje sloupce mezi databázemi
	public $exportSetting = [
		'komodita_id' => 'commodity_id',
		'nazev' => 'name',
		'zkratka' => 'shortcut',
		'stat_kod' => 'country_id',
		'saz_dph' => 'dph',
		'def_cena' => 'price',
		'znz_corr' => 'znz_correction',
		'tolerance' => 'stock_price_tolerance',
	];

	public $exportTransValues = ["stat_kod"];

	public function stat_kod($val){
		if($val == "CZ"){
			return 1;
		}
		if($val == "SK"){
			return 2;
		}
		if($val == "PL"){
			return 3;
		}
	}

	/***** KONEC NASTAVENÍ EXPORTU ****/


	public function initialize(array $config)
	{
    $this->BelongsTo("Commodities");
	}

}
