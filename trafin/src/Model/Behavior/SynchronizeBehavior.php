<?php

namespace App\Model\Behavior;

use Cake\Core\Configure;
use Cake\ORM\Behavior;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\Network\Exception\InternalErrorException;

/**
 * Synchronize behavior
 */
class SynchronizeBehavior extends Behavior {

    public function beforeSave($event, $entity, $options) {
	    if(Configure::read("test") == 0){
		    $this->database = ConnectionManager::get('old');
		    $this->Import = TableRegistry::get("Imports");

		    $this->checkSetting($event->subject());

		    $data = [0 => $entity->toArray($event->subject())];
		    $data = $this->Import->trans($data, $event->subject(), true);
		    if (count($data["d"])) {
			    $result = $this->Import->setData($event->subject()->exportTable, $this->database, $data["d"]);

			    if ($result == true) {
				    return true;
			    } else {
				    $event->stopPropagation();
			    }
		    }
	    }
    }

    private function checkSetting($subject) {
        if (!isset($subject->exportTable)) {
            die(json_encode(["r" => false, "m" => __("Synchronizace: Není definována tabulka exportTable modelu " . $subject->alias())]));
        }
        if (!isset($subject->importTable)) {
            die(json_encode(["r" => false, "m" => __("Synchronizace: Není definována tabulka importTable modelu " . $subject->alias())]));
        }
        if (!isset($subject->exportSetting)) {
            die(json_encode(["r" => false, "m" => __("Synchronizace: Není definováno pole exportSetting modelu " . $subject->alias())]));
        }

        return true;
    }

}
