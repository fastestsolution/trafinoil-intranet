<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Symfony\Component\Console\Helper\Table;

class CollectionTemplatesController extends AppController
{


	public function initialize() {
		parent::initialize();
		$this->Security->config("unlockedActions", ["dndCallback"]);
	}

  public function index()
  {
	  if($this->request->is("ajax")) {
		  $this->viewBuilder()->layout(false)->template("../CollectionTemplates/elements/table");
	  }
	  $title = __("Šablony");
	  $this->loadModel("Carriers");
	  $this->loadModel("Users");
		$carriers = $this->Carriers->find("Carriers")->where(["kos" => 0, "status" => 1])->order("shortcut");
	  $collections = $this->CollectionTemplates->find()->contain("Routes");
		$users = $this->Users->find("list")->toArray();

	  if(isset($this->request->data["user_id"])){
		  $userId =  $this->request->data["user_id"];
	  }
	  else{
		  if(!empty($users)) {
			  reset($users);
			  $userId = key($users);
		  }
		  else{
			  throw new \Exception(__("Nejsou definování žádní uživatelé, kterým lze nastavit šablonu"));
		  }
	  }


    $this->set(compact(["carriers", "collections", "title", "users", "userId"]));
  }

	public function add(){

		if(isset($this->request->query["carrier_id"])){
			$carrier_id = $this->request->query["carrier_id"];
		}
		if(isset($this->request->query["day"])){
			$day = $this->request->query["day"];
		}
		if(isset($this->request->query["user_id"])){
			$user_id = $this->request->query["user_id"];
		}

		$title = __("Naplánovat nový svoz");
		$this->viewBuilder()->layout("ajax");
		$entity = $this->CollectionTemplates->newEntity();

		if($this->request->is("ajax")){
			$this->CollectionTemplates->patchEntity($entity, $this->request->data());
			$this->CollectionTemplates->save($entity);
			die(json_encode(["r" => true]));
		}

		$crt = TableRegistry::get("CarrierRoutes");
		$rt = TableRegistry::get("Routes");
		$carrier_routes = $crt->find()->where(["carrier_id" => $carrier_id])->select("route_id");

		if($carrier_routes->count()){
			$routes = $rt->find("list")->where(["id IN" => $carrier_routes]);
		}
		$this->set(compact("entity", "title", "carrier_id", "day", "routes", "user_id"));
	}

	public function dndCallback(){
		$this->request->allowMethod(['post']);

		if(isset($this->request->data["id"]) && isset($this->request->data["day"]) && isset($this->request->data["carrier_id"])){
			$ctt = TableRegistry::get("CollectionTemplates");
			$ctt->query()->update()
				->set(["carrier_id" => $this->request->data["carrier_id"], "day" => $this->request->data["day"]])
				->where(["id" => $this->request->data["id"]])
				->execute();
			die(json_encode(["r" => true]));

		}else{
			throw new \InvalidArgumentException(__("Pro přesun chybí potřebné argumenty"));
		}
	}


	public function replicate($user_id){
		$this->viewBuilder()->layout("ajax");

		if($this->request->is("post")){
			$date = $this->request->data("date");
			$templates = $this->CollectionTemplates->find()->where(["user_id" => $user_id]);

			if($this->CollectionTemplates->replicate($templates, $date)){
				die("ok");
			}
			else{
				die("ne");
			}

		}

		$this->set("title", __("Replikovat šablonu svozů"));
		$this->set("user_id", $user_id);

	}

}
