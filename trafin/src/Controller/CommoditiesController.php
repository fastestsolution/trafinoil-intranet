<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;

class CommoditiesController extends AppController
{

  public function initialize()
  {
    parent::initialize();
	  $this->set("title", __("Komodity"));
    $this->Auth->allow(["listArticles"]);
    $this->Security->config("unlockedActions", [
        "listArticles"
      ]
    );
  }

  // driver api listArticles()
  public function listArticles(){
    if(isset($_GET["date"])) {
      $dateOfLastSynce = $_GET["date"];
    }

    $this->viewBuilder()->layout("ajax")->template("/test");


    $this->loadModel("Carriers");
    $this->DRIVER_CARRIER = "20b21ace";
    $carrier = $this->Carriers->getCarrierByHash("20b21ace");

    $response = new \stdClass();
    $response->__className = "RestResponse";
    $response->status = 200;

    $commodities = $this->Commodities->getCountryCommodity(null, $carrier->region->country_id);
    if($commodities) {
      $response->total = $commodities->count();
      $response->data = array();

      foreach ($commodities as $k => $c) {
        if(isset($c->commodity->id)) {
          $commodity = new \stdClass();
          $commodity->__className = "Article";
          $commodity->name = $c->name;
          $commodity->short_name = $c->shortcut;
          $commodity->unit = $c->commodity->unit;
          $commodity->vat = $c->dph;
          $commodity->direction = intval($c->commodity->so);
          $commodity->default_price = $c->price;
          $commodity->package = intval($c->commodity->in_pack);
          $commodity->container = intval($c->commodity->pack);
          $commodity->deleted = $c->commodity->kos;
          $commodity->order = $c->commodity->ord;
          $commodity->id = $c->commodity->id;

          $response->data[] = $commodity;
        }
      }
    }

    die(json_encode($response));

  }

  public function index()
  {
	  $query = $this->Commodities->find()->where(["kos" => 0]);

	  $bool = Configure::read("select_config.bool");

	  $table = $this->AutoTable->newTable();
		$table->data($query);
	  $table->action("edit");
	  $table->topAction('edit', __('Přidat'));
	  $table->listValue("sale", $bool);
	  $table->listValue("divisible", $bool);
	  $table->listValue("so", $bool);
	  $table->listValue("pack", $bool);
	  $table->listValue("not_in_stats", $bool);
	  $table->listValue("primar", $bool);
	  $table->listValue("driver", $bool);
	  $table->listValue('bill_type', $this->bill_type);
	  $table->listValue("co_type", Configure::read("select_config.co_type"));
	  $table->filter("name", __("Název"));
	  $table->filter("unit", __("Jednotka"));


//	  $options = array(
//		  'top_action'=>[
//			  'edit'=>__('Přidat'),
//		  ],
//		  'filtr'=>[
//			  'name'=>__('Název').'|Routes__name|text_like',
//			  //'size'=>__('Palet').'|Cars__size|text',
//			  //'car_type'=>__('Typ').'|Cars__car_type|select|car_type_list',
//		  ],
//		  'list'=>[
//			  'sale'=> [0 => __("ne"), 1 => __("ano")],
//			  'divisible'=> [0 => __("ne"), 1 => __("ano")],
//			  'so'=> [0 => __("ne"), 1 => __("ano")],
//			  'pack'=> [0 => __("ne"), 1 => __("ano")],
//			  'not_in_stats'=> [0 => __("ne"), 1 => __("ano")],
//			  'co_type' => [
//
//			  ],
//			  'primar'=> [0 => __("ne"), 1 => __("ano")],
//			  'driver'=> [0 => __("ne"), 1 => __("ano")],
//			  'bill_type' => $this->bill_type
//		  ],
//		  'posibility'=>[
//			  'edit'=> '<span class="fa fa-edit">',
//			  'trash'=>__('Smazat'),
//		  ],
//	  );

	  $this->AutoTable->render($table);
  }

 
 
  public function edit($id=null){
    $this->set("title", __("Editace komodity"));
    $this->viewBuilder()->layout("ajax");


		if ($id != null){
			$entity = $this->Commodities->find()->where(["id" => $id])->contain(["CountryCommodities" => function($q){return $q->order("country_id DESC");}])->first();
		}
	  else{
		  $entity = $this->Commodities->newEntity();
	  }
	
		if ($this->request->is("ajax")){
		  $this->Commodities->patchEntity($entity, $this->request->data(), ["associated" => ["CountryCommodities"]]);
		  $this->check_error($entity);
		  if ($result = $this->Commodities->save($entity)) {
	        die(json_encode(['r'=>true,'m'=>__('Uloženo'),'id'=>$entity->id]));
			} else {
	        die(json_encode(['r'=>false,'m'=>__('Chyba uložení')]));
		  }
    }

	  $packs = $this->Commodities->find("list")->where(["pack" => 1])->toArray();

    $this->set(compact("entity", "packs"));
  }


  public function editAuth($user_id){

    $user = $this->Users->get($user_id);
    $uat = TableRegistry::get("UserAuths");
    $userAuth = $uat->findByUserId($user_id)->first();

    if($this->request->is("post")){
      if($this->Users->editAuth($userAuth->id, $this->request->data())){
        $this->Flash->success(__("Práva byla úspěšně upravena."));
        $this->redirect(['controller' => 'users', 'action' => 'edit', $user_id]);
      }
    }

    $this->set("user_auth", unserialize($userAuth->auth));
    $this->set("auths", Configure::read("Auths.Auths"));
    $this->set("auth_names", Configure::read("Auths.AuthNames"));
    $this->set("user", $user);
  }

}
