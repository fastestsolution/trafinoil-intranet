<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Core\Exception\Exception;
use Cake\Utility\Security;
use Cake\Datasource\ConnectionManager;
use Cake\Network\Exception\InternalErrorException;
use Cake\Network\Exception\NotFoundException;
use Cake\I18n\Time;

class DriverImportsController extends AppController
{

  public function initialize()
  {
	  $this->loadModel("DriverCollections");
	  $this->loadModel("DriverCollectionItems");
	  $this->loadModel("Commodities");
	  $this->loadModel("Carriers");
	  $this->loadModel("DriverCollectionItemTrades");

	  $this->loadComponent("Dph");

    $this->viewBuilder()->template("index");
    parent::initialize();
  }

  public function import(){
	  $handle = opendir("driver_data");

	  while(false !== ($entry = readdir($handle))){
		  if($entry != "." && $entry != "..") {
			  if(preg_match("/^debug_delivery_submit/", $entry)){
					if($file = (file_get_contents("driver_data/".$entry))){
						$data = json_decode($file);
						$this->importDeliverySubmit($data, $entry);
					}
				  else{
					  echo $entry." se nepovedlo přečíst";
				  }
			  }
		  }
	  }
  }

	public function importDeliverySubmit($data, $name = null){

		foreach($data as $item){

			if(!isset($item->cartage_id)){
				$item->cartage_id = 0;
			}

			if(!isset($driverCollection) || $item->cartage_id != $driverCollection["collection_id"]) {
				$driverCollection = $this->DriverCollections->find()->where(["collection_id" => $item->cartage_id])->select(["id", "carrier_id", "date_start", "collection_id"])->first();
			}
			if(!$driverCollection){
			  if(isset($name)) {
          $carrier_id = @ltrim(substr(substr($name, 22), 0, 3), '0');
        }
				$driverCollection = $this->DriverCollections->newEntity();
				$driverCollection->collection_id = $item->cartage_id;
				$driverCollection->carrier_id = $carrier_id;
				$driverCollection->date_start = $item->time;
				$driverCollection->status = 1;
				$driverCollection->remote_id = $item->cartage_local_id;
				$this->DriverCollections->save($driverCollection);
			}

			$country = $this->Carriers->find()->where(["Carriers.id" => $driverCollection->carrier_id])->select(["id" => "Regions.country_id"])->hydrate(false)->contain("Regions")->first();
			$cd_item = $this->DriverCollectionItems->newEntity();


			// přepis svozu
			$rewrite = false;
			if (isset($item->pickup_record_local_id) && $item->pickup_record_local_id > 0)
			{
				$test = $this->DriverCollectionItems->find()->where(["collection_id" => $driverCollection->collection_id, "remote_pickup_record_id" => $item->pickup_record_local_id])->first();
				if(count($test)){
					$this->DriverCollectionItems->patchEntity($cd_item, $test->toArray());
					$rewrite = true;
				}
			}
			if ($rewrite == false &&  isset($item->premise_local_id) && $item->premise_local_id > 0)
			{
				$test = $this->DriverCollectionItems->find()->where(["collection_id" => $driverCollection->collection_id, "remote_premise_id" => $item->premise_local_id])->first();
				if (count($test))
				{
					$this->DriverCollectionItems->patchEntity($cd_item, $test->toArray());
					$rewrite = true;
				}
			}

			if (!$rewrite)
			{
				$prev_zast = $this->DriverCollectionItems->find()->where(["collection_id" => $driverCollection->collection_id])->count();
				$cd_item->poradi = $prev_zast + 1;
			}

			/****** konec přepis svozu **/

			$cd_item->driver_collection_id = $driverCollection->id;
			$cd_item->collection_id = $driverCollection->collection_id;
			$cd_item->carrier_id = $driverCollection->carrier_id;
			$cd_item->time = new Time($item->time);
			$cd_item->shop_id = isset($item->premise_id) ? $item->premise_id : 0;
			if(isset($item->premise_id)) {
				$cd_item->dph_payer = $this->Dph->isPayer($item->premise_id);
			}
			if(isset($item->locationLatitude)) {
				$cd_item->lat = $item->locationLatitude;
			}
			if(isset($item->locationLongitude)) {
				$cd_item->lng = $item->locationLongitude;
			}
			if(isset($item->dispatcherNote)) {
				$cd_item->note = $item->dispatcherNote;
			}
			if (isset($item->pickup_record_local_id)) {
				$cd_item->remote_pickup_record_id = $item->pickup_record_local_id;
			}
			$cd_item->uhrazene_pohledavky = $item->paidClaim;
			$cd_item->cislo_dokladu = isset($item->documentNumber)? $item->documentNumber : '';

			$cd_item->prodej_faktura = $item->isSellInvoiced == 'true' ? 1 : 0;
			$cd_item->vykup_faktura = $item->isBuyoutInvoiced == 'true' ? 1 : 0;
			$cd_item->status = Configure::read("select_config.driver_collection_item.handled.code.".$item->handled);

			if (isset($item->reasonNotBuy)) {
				$cd_item->duvod = $item->reasonNotBuy;
			}
			$cd_item->prijato = new Time();
			$cd_item->remote_id = isset($item->cartage_local_id)? $item->cartage_local_id : 0;
			if (isset($item->incomingScans)) {
				$cd_item->incoming_scans = $item->incomingScans;
			}
			if (isset($item->outgoingScans)) {
				$cd_item->outgoing_scans = $item->outgoingScans;
			}

			$map = array(
				'incomeCashNumber'=>'cdo_pokladna_prijem',
				'expenseCashNumber'=>'cdo_pokladna_vydej',
				'incomeInvoiceNumber'=>'cdo_faktura_prijem',
				'expenseInvoiceNumber'=>'cdo_faktura_vydej',
				'expenseCashDebtNumber'=>'cdo_pokladna_dluh',
				'paidCommitment'=>'uhrazene_zavazky'
			);
			foreach($map as $remote => $local) {
				if (isset($item->$remote)) {
					$cd_item->$local = $item->$remote;
				}
			}

			$this->DriverCollectionItems->save($cd_item);

			if($rewrite){
				$this->DriverCollectionItemTrades->query()->delete()->where(["driver_collection_item_id" => $cd_item->id])->execute();
			}

			if(!empty($item->sells)){
				$this->proc_trades(2, $item->sells, $cd_item->id, $country["id"], $driverCollection->date_start);
			}
			if(!empty($item->buyouts)){
				$this->proc_trades(1, $item->buyouts, $cd_item->id, $country["id"], $driverCollection->date_start, $cd_item->shop_id);
			}
		}

	}

	//trade_type 1 prodej, 2 výkup;
	function proc_trades($trade_type, $trades, $cdItem_id, $country_id, $date_start, $shop_id = null){
		foreach ($trades as $trade)
		{
			$sell_item = $this->DriverCollectionItemTrades->newEntity();
			$sell_item->driver_collection_item_id = $cdItem_id;
			$sell_item->type = $trade_type;
			$sell_item->commodity_id = $trade->product_id;
			$sell_item->amount = $trade->amount;
			$sell_item->price = $trade->price * $trade->amount;

			$dph_type = $this->Commodities->getCountryCommodity($trade->product_id, $country_id)->select("dph")->first();

			if($trade_type == 1){
				$isPayer = $this->Dph->isPayer($shop_id);
				if($isPayer == true){
					$sell_item->dph = $this->Dph->getDph($country_id, $dph_type["dph"], $date_start);
				}
				else{
					$sell_item->dph = 0;
				}
			}
			else{
				$sell_item->dph = $this->Dph->getDph($country_id, $dph_type["dph"], $date_start);
			}


			$this->DriverCollectionItemTrades->save($sell_item);
		}
	}

}