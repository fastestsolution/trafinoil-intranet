<?php
namespace App\Controller\Component;

use Cake\Mailer\Email;
use Cake\Controller\Component;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;


class ViewIndexComponent extends Component{

  public function initialize(array $config)
  {
    // config
    $this->controller = $this->_registry->getController();
  }

  public function data($controller, array $options = null){
    // načte tabulku a připravý pro dotazy
    $table = TableRegistry::get($controller);
    $query = $table->find();

    // připravý se conditions, pokud žádné nejsou, doplní se jen kos = 0 pokud není
    if(!isset($options["conditions"][$controller.".kos"])){
      $options["conditions"] = array($controller.".kos" => 0);
    }
    $conditions = $this->convert_conditions($options["conditions"]);

    if(isset($options["contain"])){
      $query->contain($options["contain"]);
    }

    // pokud chceme jenom některé sloupce
    if(isset($options["select"])){
      $query->select($options["select"]);
    }

    // pokud jsou nějaké podmínky
    if(isset($options["conditions"])){
      $query->where($options["conditions"]);
    }
    if(isset($conditions)){
      $query->where($conditions);
    }

    return $query;
  }

  public function render($data = null, $params = null){

	  $this->controller->set('items', $this->controller->paginate($data));
    $this->controller->set('params_viewIndex', $params);

    if ($this->request->is('ajax')) {
      $this->controller->viewBuilder()->layout(false);
      $this->controller->render('/ViewIndex/items');
    } else {
      $this->controller->render('/ViewIndex/index');
    }
  }


  public function convert_conditions($con = null){
    $conditions = array();
    $disable_keys = [
      'h',
      'sort',
      'direction',
    ];

    if (isset($this->request->query) && !empty($this->request->query)){
      //pr($this->request);

      foreach($this->request->query AS $key=>$value){
	      if($key == "page"){
		      continue;
	      }
        $key = explode('|',$key);
        if (in_array($key[0],$disable_keys)){
          continue;
        }

        $key[0] = strtr($key[0],array('__'=>'.'));
        //pr($key);

        // like conditions
	      if(isset($key[1])){
	        if ($key[1] == 'like'){
	          $conditions[$key[0].' '.$key[1]] = '%'.$value.'%';
	        }
	        else if($key[1] == 'opt'){
						$chunks = explode(".", $key[0]);
		        if($value == 1) {
			        $conditions[$chunks[0] . "." . $chunks[1] . " IN"][] = $chunks[2];
		        }
	        }
	        else if ($key[1] == 'date'){
	          $value_tmp = explode('.',$value);
	          $conditions[$key[0]] = $value_tmp[2].'-'.$value_tmp[1].'-'.$value_tmp[0];
	          //pr($conditions);
	        }
	      }
	      else {
          $conditions[$key[0]] = $value;
				}
      }
    }

    return $conditions;
  }


}