<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\Entity;
use Cake\Network\Exception\NotFoundException;
use Cake\Network\Exception\ServiceUnavailableException;

Class DriverLogComponent extends Component{

  private $log;
  private $jsonLog;

  public function __construct($request, $action)
  {
    $this->log = fopen(ROOT."/logs/driver_logs/logs/".date("Y-m-d-H:i:s")."--{$action["action"]}--.txt", "w");
    $this->jsonlog = fopen(ROOT."/logs/driver_logs/json/".date("Y-m-d-H:i:s")."--{$action["action"]}--.txt", "w");
  }

  public function set($data){
    fwrite($this->log, print_r($data, true));
    fwrite($this->jsonlog, json_encode($data));
  }

  public function error($data){
    fwrite($this->log, $data);
  }
}