<?php

namespace App\Controller\Component;

use Cake\Mailer\Email;
use Cake\Controller\Component;
use Cake\ORM\Entity;
use Cake\Network\Exception\NotFoundException;
use Cake\Network\Exception\ServiceUnavailableException;
use Cake\ORM\TableRegistry;
use Symfony\Component\Console\Helper\Table;

class DphComponent extends Component {

	/**
	 * @param $country_id
	 * @param $commodity_dph
	 * @param null $date
	 * @return int
	 */
	public function getDph($country_id, $commodity_dph, $date = null){
		$dpht = TableRegistry::get("Dphs");
		$query = $dpht->find()->where(["country_id" => $country_id, "type" => $commodity_dph]);

		if(isset($date)){
			$query->where(["valid_from <=" => $date])->where(["OR" => ["valid_to >=" => $date, "valid_to IS NULL"]]);
		}
		else{
			$query->where(["valid" => 1]);
		}

		$res = $query->select("val")->first();
		return $res["val"];
	}

	public function isPayer($shop_id){
		$st = TableRegistry::get("Shops");
		$shop = $st->find()->where(["Shops.id" => $shop_id])->contain(["Companies"])->select("Companies.dph")->first();
		if(!isset($shop["Companies"]["dph"])){
			$this->log(__("DphComponent->isPayer() Nepodařilo se ověřit plátce dph pro provoz")." ".$shop_id, "warning", ["imports"]);
		}
		if($shop["Companies"]["dph"] == 1){
			return true;
		}
		else{
			return false;
		}
	}
}
