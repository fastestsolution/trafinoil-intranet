<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Event\Event;


/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController
{

  public function beforeFilter(Event $event) {
    if (in_array($this->request->action, ['imptest'])) {
      $this->eventManager()->off($this->Csrf);
    }
  }

  public function initialize()
  {
    parent::initialize();
    $this->Auth->allow("imptest");
    // $this->loadHelper("Session");
  }

  public function imptest(){
    die(json_encode(array("message" => "Prislo ti to?")));
  }

  /**
   * Displays a view
   *
   * @return void|\Cake\Network\Response
   * @throws \Cake\Network\Exception\NotFoundException When the view file could not
   *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
   */
  public function display()
  {
    $path = func_get_args();

    $count = count($path);
    if (!$count) {
      return $this->redirect('/');
    }
    $page = $subpage = null;

    if (!empty($path[0])) {
      $page = $path[0];
    }
    if (!empty($path[1])) {
      $subpage = $path[1];
    }
    $this->set(compact('page', 'subpage'));

    try {
      $this->render(implode('/', $path));
    } catch (MissingTemplateException $e) {
      if (Configure::read('debug')) {
        throw $e;
      }
      throw new NotFoundException();
    }
  }

  public function homepage()
  {
    $this->set('title', appName);
    $this->redirect(["controller" => "collections", "action" => "index"]);
  }

  public function googleNav()
  {
	  $data = file_get_contents('https://maps.googleapis.com/maps/api/directions/json?origin=sydney,au&destination=perth,au&waypoints=via:-37.81223%2C144.96254%7Cvia:-34.92788%2C138.60008&key=AIzaSyAmW3rvqxi1_T_Eyaz07bp1atOFKhbW9rU');
	  die($data);
  }

  public function closeModal()
  {
    $this->set('title', appName);
  }

  public function ares($ic)
  {
    //$ic = '28591232';
    $this->loadComponent('Ares');
    $result = $this->Ares->search($ic);
    die(json_encode($result));
  }
  /**
   * Overeni zda je firma platcem DPH, bude volano pravidelne cronem...
   */
   public function aresCron($offset = 1)
  {
    $limit = 6;
    
    $this->loadModel('Companies');
    $companies = $this->Companies->find("all", array('fields'=> ['id', 'ic', 'dph'], 'limit'=>$limit, 'offset'=>$offset, 'conditions'=>array('LENGTH(ic)'=>8, 'ic !='=>'00000000')));
    $count = $companies->count();
    
    $this->loadComponent('Ares');
    
    echo 'Zpracovavam '. $limit . ' pozadavku z ' . $count .' ( strana '.$offset.' z '.ceil($count / $limit).' ) <br/>';
    foreach($companies as $company){
        try{
            $ic = trim($company->ic);
            if(strlen($ic) == 8){
                $result = $this->Ares->checkDph($ic);
            }else{
                $result['message'] = 'Spatny format ic';
            }
        }catch(Exception $e){
            $result = $e->getMessage();
        }
        echo $company->ic . ' > ' . $result['message'] . ' DPH v DB "'. $company->dph .'"<br/>';
        
        if($company->dph != intval($result['return'])){
            echo 'KONFLIKT! ';

            $update_company = $this->Companies->get($company->id); 
            $update_company->dph = intval($result['return']);
            if($this->Companies->save($update_company)){
                echo 'Hodnota v DB opravena<br/>';
            }else{
                echo 'Nepodařilo se upravit hodnotu v DB!<br/>';
            }
        }else{
            echo 'OK<br/>';
        }
    }
    echo 'Cas zpracovani pozadavku: '. round((microtime(true) - $_SERVER['REQUEST_TIME']), 2) .' sekund <br/>';
    die();
  }
  
  // trash function
  public function trash($model, $id)
  {
    $this->loadModel($model);

    if (!$this->$model->query()
      ->update()
      ->set(['kos' => 1])
      ->where(['id' => $id])
      ->execute()
    ) {
      die(json_encode(array('result' => false, 'message' => __('Chyba smazání'))));

    }

    die(json_encode(array('result' => true, 'message' => __('Položka byla smazána'))));
  }

	public function help(){

	}

}
