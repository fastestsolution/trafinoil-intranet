<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Exception\Exception;
use Cake\Utility\Security;
use Cake\Datasource\ConnectionManager;
use Cake\Network\Exception\InternalErrorException;
use Cake\Network\Exception\NotFoundException;

class ImportsController extends AppController
{

  public $tableEntity;
  public $from = 0;

  public function initialize()
  {
    $this->viewBuilder()->template("index");
    parent::initialize();
  }


  // řídí průběh importu, pokud chceš importovat zavolej tuto fci s příslušnými parametry
  public function import($tableEntity){
      $this->tableEntity = $this->loadModel($tableEntity);
      $this->database = ConnectionManager::get('old');

      if(!isset($this->tableEntity->exportSetting)){
        throw new InternalErrorException(__("Není definováno pole pro převod databáze exportSetting, nebo voláte neexistující Entitu v url"));
      }

      if(!isset($this->tableEntity->exportTable)){
        throw new InternalErrorException(__("Není definována tabulka v původní databázi exportTable"));
      }

      if($data = $this->Imports->getData($this->tableEntity, $this->database, $this->request->query)){
        $this->database = ConnectionManager::get('default');
        $this->Imports->setData($this->tableEntity->importTable, $this->database, $data["d"]);
      }
      else{
        throw new NotFoundException(__('Žádná data k importu'));
      }
  }
}