<?php
namespace App\Controller;

use App\Controller\AppController;
use AutoTable;
use Cake\Core\Configure;

class SuppliersController extends AppController
{
  public function initialize()
  {
    parent::initialize();

    $this->Auth->allow(["listSuppliers"]);
    $this->Security->config("unlockedActions", [
        "listSuppliers"
      ]
    );

  }

  public function listSuppliers(){

    $response = new \stdClass();
    $response->__className = "RestResponse";
    $response->status = 200;
    $suppliers = $this->Suppliers->find()->where(["kos" => 0]);
    $response->total = $suppliers->count();
    if($response->total){
      $response->data = array();
      foreach($suppliers as $s){
        $supplier = new \stdClass();
        $supplier->id = intval($s->id);
        $supplier->name = $s->name;
        $supplier->street = $s->street;
        $supplier->city = $s->city;
        $supplier->postcode = $s->psc;
        $supplier->ico = $s->ic;
        $supplier->dic = $s->dic;
        $supplier->bank = $s->bank_account;
        $supplier->deleted = boolval($s->kos);

        $response->data[] = $supplier;
      }
    }

    die(json_encode($response));
  }
}