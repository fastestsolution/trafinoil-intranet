<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Database\Schema\Table;
use Cake\Event\Event;
use Cake\I18n\Date;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

Date::$defaultLocale = 'cs-CZ';
Date::setToStringFormat('dd.MM.YYYY');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    public function beforeFilter(Event $event)
    {
			$this->load_select_config();
			$this->load_system_id();
			$this->Security->config('unlockedActions', [
				'autocomplete',
				'autocompleteMesto',
				'edit',
			]);

	    $this->load_js();
	    $this->loadVars();
    }

    public function initialize()
    {
        parent::initialize();
	      $this->loadComponent('Security');
        $this->loadComponent('Paginator',
          [
            'limit' => 20
          ]);
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
	      $this->loadComponent('Csrf');
        $this->loadComponent('AutoTable');
        
		    $this->loadComponent('Auth', [
          'unauthorizedRedirect' => "/",
          'authError' => __("Nemáte přístup do této stránky"),
          'loginRedirect' => ['controller' => 'collections', 'action' => '/'],
          'authorize' => [
            'Controller'
          ],
			    'storage' => 'Session'
        ]);

	      $this->Security->config('unlockedActions', ['find']);
			
        if($this->request->is("ajax")){
            $this->viewBuilder()->layout("ajax");
        }

		    if(isset($_GET["back"])){
			    $this->set("back", $_GET["back"]);
		    }
		    if(isset($_GET["tab"])){
			    $this->set("tab", $_GET["tab"]);
		    }

        $this->set("title", appName);
    }

    public function isAuthorized($user = null){
        $this->set("authUser", $user);
        return true;

        $auth = $this->Auth->user("auth");
        $controller = strtolower($this->request->controller);
        $action = strtolower($this->request->action);

        if(isset($auth["all"]) && $auth["all"]["all"] == 1){
            return true;
        }
        else {
            if (isset($auth[$controller]) && isset($auth[$controller][$action]) && $auth[$controller][$action] == 1) {
                return true;
            } else {
                return false;
            }
        }
    }
	
	private function load_select_config(){
		$sc = Configure::read('select_config');
		foreach($sc AS $k=>$item){
			$this->$k = $item;
			$this->set($k,$item);
		}
		
	}
	
	private function load_system_id(){
		//pr($_SERVER);die();
		if ($this->request->session()->check('System.system_id')) {
			$this->system_id = $this->request->session()->read('System.system_id');
			
			
			$this->set('system_id',$this->system_id);
			
		} else {
			$this->request->session()->write('System.system_id', 1);
			
		}
	}

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeRender(Event $event)
    {

	      if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }


	public function check_error($entity=null){
		if ($entity != null && $entity->errors()){
			$message = '';
			$invalid_list = array();
			//pr($entity->errors());
			$convert_valid = [
				'date_'=>'date-',
			];

			foreach($entity->errors() AS $k=>$mm){
				foreach($mm AS $mk=>$m){
					if (is_array($m)){
						$invalid_list[] = $k.'-'.strtr($mk,$convert_valid);
						foreach($m AS $m2){
						$message .= $m2.'<br />';

						}

					} else {
						$message .= $m.'<br />';

					}

				}
				$invalid_list[] = strtr($k,$convert_valid);
			}
			die(json_encode(['r'=>false,'m'=>$message,'invalid'=>$invalid_list]));
		}
	}

	public function result($r = true, $id = null, $message = null, array $jsFunction = null, array $options = null){

		$m = [];
		$m["r"] = $r;

		if(isset($message)){
			$m["m"] = $message;
		}
		if(isset($id)){
			$m["id"] = $id;
		}
		if(isset($jsFunction)){
				$m["s"] = "after_".key($jsFunction)."(".$jsFunction[key($jsFunction)].")";
		}
		if(isset($options)){
			foreach($options as $k => $v){
				$m[$k] = $v;
			}
		}


		die(json_encode($m));
	}

	private function loadVars(){
		$this->regions = TableRegistry::get("regions")->find("list");
		$this->set("regions", $this->regions);

		$this->carriers = TableRegistry::get("Carriers")->find("list");
		$this->set("carriers", $this->carriers);
	}
	
	private function load_js(){
        require_once('min/lib/Minify.php');
        require_once('min/lib/Minify/Source.php');
        require_once('min/utils.php');

        $jsUri = Minify_getUri('js'); // a key in groupsConfig.php
        $this->set('jsUri',$jsUri);		

        $cssUri = Minify_getUri('css'); // a key in groupsConfig.php
        $this->set('cssUri',$cssUri);		
  }
}
