<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Core\Plugin;
use Cake\Routing\Router;

/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 */
Router::defaultRouteClass('DashedRoute');

Router::scope('/', function ($routes) {

    $routes->connect('/', ['controller' => 'Pages', 'action' => 'homepage']);
	  $routes->connect('/trash/*', ['controller' => 'Pages', 'action' => 'trash']);
	  $routes->connect('/close_modal/', ['controller' => 'Pages', 'action' => 'close_modal']);

    $routes->connect('/driver/', ['controller' => 'collections', 'action' => 'configuration']);
    $routes->connect('/driver/send-event/*', ['controller' => 'collections', 'action' => 'sendEvent']);
    $routes->connect('/driver/delivery-plan/*', ['controller' => 'collections', 'action' => 'listCartages']);
    $routes->connect('/driver/delivery-submit/*', ['controller' => 'collections', 'action' => 'deliverySubmit']);
    $routes->connect('/driver/articles/*', ['controller' => 'commodities', 'action' => 'listArticles']);
    $routes->connect('/driver/suppliers/*', ['controller' => 'suppliers', 'action' => 'listSuppliers']);



    $routes->fallbacks('DashedRoute');
});

Router::scope('/uzivatele', function ($routes) {
    $routes->connect('/', ['controller' => 'users', 'action' => 'index']);
    $routes->connect('/:novy', ['controller' => 'users', 'action' => 'add']);
    $routes->connect('/:upravit/:id', ['controller' => 'users', 'action' => 'edit']);
});
Router::scope('/dashboard', function ($routes) {
    //$routes->connect('/', ['controller' => 'users', 'action' => 'index']);
});

Router::scope('/imports', function ($routes) {
  $routes->connect('/:tableEntity/*', ['controller' => 'imports', 'action' => 'import'], ['pass' => ['tableEntity', 'type']]);
});

	//$routes->connect('/*', ['controller' => 'Pages', 'action' => 'homepage']);

Plugin::routes();
