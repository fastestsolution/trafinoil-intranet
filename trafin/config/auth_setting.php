<?php
$auths = [
  'All' => __("Vše"),
	'Users' => __("Uživatelé"),
	'Carriers' => __("Dopravci"),
	'Shops' => __("Obchody"),
	'Companies' => __("Firmy"),
	'Routes' => __("Trasy"),
	'Collections' => __("Svozy"),
	'Commodities' => __("Komodity"),
	'Authorizations' => __("Oprávnění")
];
