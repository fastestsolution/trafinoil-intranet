var api_key = 'AIzaSyAmW3rvqxi1_T_Eyaz07bp1atOFKhbW9rU';
  
var Map = new Class({

  Implements:[Options,Events],
  options: {
	route_id:0,
  },
  step_index: 0,
  debug: true,
  directionsDisplay : {},
  trasa_delka: 0,
  
  color_list : {
	0: '#FFF',
	1: '#8bc34a',
	2: '#FEFF86',
	3: '#FF6666',
	4: '#FF9944',
	5: '#8899FF',
	6: '#FF77AA',
	7: '#77EEFF',
	99: '#999999',
	8: '#BF9230',  
  },
	

  initialize:function(element, options){
    this.setOptions(options);
	this.default_map(element);
	this.load_data();
	
	this.side_panel();
	
	this.directionsService = new google.maps.DirectionsService;
	
  },
  
  // side panel
  side_panel: function(){
	if ($('side_panel')){
		VarsModal.side_panel = $('side_panel');
		//console.log(VarsModal.side_panel);
	}
  },
  
  // create default map
  default_map: function(element){
	  VarsModal.gmap = new google.maps.Map(element, {zoom : 8, center: {lat: 49.7838146, lng: 18.6152801}});
	
  },
  
  // load data from JSON
  load_data: function(){
	VarsModal.gps_coords = {}
	VarsModal.map_load_data = new Request.JSON({
		url:this.options.url+this.options.id,
		onComplete :VarsModal.complete_load_map_data = (function(json){
			//console.log(json);
			VarsModal.gps_coords = json;
			json = null;
			data = null;
			
			this.create_markers();
			
			
		}).bind(this)
	});
	VarsModal.map_load_data.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
	VarsModal.map_load_data.send();	
	  
  },
  
  // create markers from JSON data
  create_markers: function(){
	//this.create_polyline();
	this.get_navigation();
			  
  },
  
  
  // get navigation
  get_navigation: function(){
	
	//VarsModal.stepDisplay = new google.maps.InfoWindow;

	this.calculate_route();

  },
  
  // render path direction
  renderDirections: function(result) { 
        var directionsRenderer = new google.maps.DirectionsRenderer({
			suppressMarkers:true
			
		}); 
        directionsRenderer.setMap(VarsModal.gmap); 
        directionsRenderer.setDirections(result); 
		
		for (var b=0 ; b<result.routes[0].legs.length; b++){
			this.trasa_delka += result.routes[0].legs[b].distance.value;
		}
		
		$('delka_trasa').set('text','Delka trasy: '+(this.trasa_delka/1000)+'km');
		if (this.debug){
			console.log('Delka trasa',this.trasa_delka+' m');
			console.log('Delka trasa',(this.trasa_delka/1000)+'km');
		}
  }, 
  
  // calculate route
  calculate_route: function(){
	step = this.step;
	first = null;
	last = null;
	VarsModal.waypts = [];
	VarsModal.markers = [];
	var wpcount = 0;
	var count_total = Object.getLength(VarsModal.gps_coords);
	var path = VarsModal.gps_coords;
	var waypoint = [];
	//console.log(count_total);
		
		// first item
    console.log(path[this.step_index]);
		if (this.debug) console.log('first step ',path[this.step_index].Shops.name);
		first = new google.maps.LatLng(path[this.step_index].Shops.lat,path[this.step_index].Shops.lng);
		
		// marker create
		this.create_own_marker(path[this.step_index],this.step_index);
		
		// waypoints
		while (this.step_index < count_total - 2 && wpcount < 8){
			
			this.step_index++;
			if (this.debug) console.log(path[this.step_index].Shops.name);
			waypoint.push({
				location: new google.maps.LatLng(path[this.step_index].Shops.lat.toFloat(),path[this.step_index].Shops.lng.toFloat()),
				stopover: true
			});
			wpcount++;
			
			// marker create
			this.create_own_marker(path[this.step_index],this.step_index);
			
		}
		
		this.step_index++;
		
		// last item
		if (this.debug) console.log('last step ',path[this.step_index].Shops.name);
		last = new google.maps.LatLng(path[this.step_index].Shops.lat,path[this.step_index].Shops.lng);
		
		// marker create
		this.create_own_marker(path[this.step_index],this.step_index);
		
		
		if (this.debug){
			//console.log(first);
			//console.log(waypoint);
			//console.log(last);
		}
		
		//this.step_index++;
		
		this.directionsService.route({
			origin: first,
			destination: last,
			travelMode: google.maps.TravelMode.DRIVING,
			waypoints: waypoint

		}, (function(response, status) {
			if (status === google.maps.DirectionsStatus.OK) {
				//console.log(step);
				this.renderDirections(response);
				this.step_index++;
				if (this.step_index < (count_total - 1)){
					this.calculate_route.delay(100,this);
				}
			  //showSteps(response, markerArray, stepDisplay, map);
			} else {
			  //window.alert('Directions request failed due to ' + status);
			}
		}).bind(this));
		
		
	
  },
  
  // create own marker
  create_own_marker: function(item,i){
		if (item.Shops.color){
			color = this.color_list[item.Shops.color];
		} else {
			color = '#2C4CA4'
		}
		image = 'data:image/svg+xml,';
		image_data = '<svg xmlns="http://www.w3.org/2000/svg" width="38" height="38" viewBox="0 0 38 38"><path fill="'+color+'" stroke="#ccc" stroke-width=".5" d="M34.305 16.234c0 8.83-15.148 19.158-15.148 19.158S3.507 25.065 3.507 16.1c0-8.505 6.894-14.304 15.4-14.304 8.504 0 15.398 5.933 15.398 14.438z"/><text transform="translate(19 18.5)" fill="#fff" style="font-family: Arial, sans-serif;font-weight:bold;text-align:center;" font-size="12" text-anchor="middle">'+(i+1).toString()+'</text></svg>';
		image = image + encodeURIComponent(image_data);
		//console.log(image);
		

		marker_id = item.Shops.id;
		marker = new google.maps.Marker({
			position: new google.maps.LatLng(item.Shops.lat.toFloat(),item.Shops.lng.toFloat()),
			title: item.Shops.name,
			icon: image,
			marker_id: marker_id,
		});
		marker.setMap(VarsModal.gmap);
		
		this.click_marker(marker,marker_id);
		
		VarsModal.markers.push(marker);
		  
  },
  
  // click marker on map and show side detail
  click_marker: function(marker,marker_id){
		google.maps.event.addListener(marker, 'click',(function(event){
		//marker.addListener('click', VarsModal.click_marker = (function(marker_id){
			//console.log(marker);
			VarsModal.side_panel.addClass('preloader');
					
			VarsModal.map_load_marker = new Request({
				url:'/shops/view/'+marker.marker_id,
				onComplete :VarsModal.complete_load_marker = (function(json){
					
					VarsModal.side_panel.removeClass('preloader');
					console.log(json);
					json = null;
					data = null;
					
				}).bind(this)
			});
			VarsModal.map_load_marker.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
			VarsModal.map_load_marker.send();	
		
		}).bind(this));
		
  },
  
  // create polyline zatim se nepouziva
  create_polyline: function(){
	  var coords = [];
	  Object.each(VarsModal.gps_coords,VarsModal.gps_coords_each = function(item){
		coords.push({'lat':item.lat.toFloat(),'lng':item.lng.toFloat()});
	  });
	  
	  //console.log(coords);
	  VarsModal.routePath = new google.maps.Polyline({
		path: coords,
		geodesic: true,
		strokeColor: '#FF0000',
		strokeOpacity: 1.0,
		strokeWeight: 2
	  });
	var path = VarsModal.routePath.getPath();
	var path_encode = google.maps.geometry.encoding.encodePath(path);
	  //VarsModal.routePath.setMap(VarsModal.gmap);

  },

});

window.addEvent("domready", function() {
  
  if (typeof google === 'object' && typeof google.maps === 'object'){
	mapInit();
	
  } else {
	var script = document.createElement('script');
	script.type = 'text/javascript';
	script.src = 'https://maps.googleapis.com/maps/api/js?libraries=geometry&' +'key='+api_key+'&callback=mapInit';
	document.body.appendChild(script);
    
  }
});

