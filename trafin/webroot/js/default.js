/**
 * Created by Marin Hrabal on 13.1.2016.
 */

function date_picker(){
	//window.addEvent('domready', function() {
	
	if ($$('.date_range').length>0){
	var picker = new Picker.Date.Range($$('.date_range'), {
		timePicker: false,
		columns: 3,
		months: lang.mesice, 
		months_title: lang.mesice, 
		months_abbr:lang.mesice_short,
		days_abbr:lang.dny,
		shortDate: '%Y-%m-%d',
		dateOrder: ['year', 'month','date'],
		shortTime: '%H:%M:%S',
		format:'%Y-%m-%d',
		allowEmpty:true,
		positionOffset: {x: 5, y: 0}
	});
	}
	if ($$('.date').length>0){
	window.picker = new Picker.Date($$('.date'), {
		timePicker: false,
		columns: 1,
		//months: lang[CURRENT_LANGUAGE].mesice, 
		//'abbr',{
		months_abbr:lang.mesice_short,
		days_abbr:lang.dny,
		//},
		//shortDate: '%d-%m-%Y',
		//dateOrder: ['date', 'month','year`'],
		//shortTime: '%H:%M:%S',
		format:'%d.%m.%Y',
		allowEmpty:true,
		positionOffset: {x: 5, y: 0}
	});
	
	}
	if ($$('.date_time').length>0){
	$$('.date_time').each(function(item){
		if (item.value == '0000-00-00 00:00:00')
			item.value = '';
	});
	window.picker = new Picker.Date($$('.date_time'), {
		
		timePicker: true,
		columns: 1,
		months: lang.mesice, 
		months_abbr:lang.mesice_short,
		days_abbr:lang.dny,
			//months_title: ['ja', 'fe', 'ma', 'ap', 'me', 'jn', 'jl', 'au', 'ok', 'se', 'no', 'de'],
			//days_abbr: ['zon', 'maa', 'din', 'woe', 'don', 'vri', 'zat'],
		
		
		shortDate: '%Y-%m-%d',
		dateOrder: ['year', 'month','date'],
		shortTime: '%H:%M:%S',
		format:'%Y-%m-%d %H:%M:%S',
		allowEmpty:true,
		
		//months_abbr: ['ja', 'fe', 'ma', 'ap', 'me', 'jn', 'jl', 'au', 'ok', 'se', 'no', 'de'],
		
		positionOffset: {x: 5, y: 0}
	});
	}
	
	//});
}

function count_maxlength(){
	// počítadlo kolik znaků ještě můžeš zadat u inputů
	  if($$("input, textarea")[0]){
		$$("input[type=text], textarea").each(function(inp){
		  if($(inp).hasClass("maxlength")){
			var counter = new Element("span.counter");
			counter.set("html", $(inp).get("maxlength"));
			$(inp).addEvent("keyup", function(){
			  counter.set("html", $(inp).get("maxlength") - $(inp).get("value").length);
			});
			$(inp).getParent().adopt(counter);
		  }
		})
	  }
}	


function FstAlert(value,modal){
	
	//console.log($(window));
	//console.log($(parent.window));
	new mBox.Notice({
		type: 'ok',
		target:((modal)?$(window.parent.document):$(window)),
		inject:((modal)?window.parent.document.body:null),
		fadeDuration: 1500,
		position: {
				x: 'center',
				y: 'center'
			},
		content: value
	});
}
function FstError(value,modal){
	new mBox.Notice({
		type: 'error',
		target:((modal)?$(window.parent.document):$(window)),
		inject:((modal)?window.parent.document.body:null),
		fadeDuration: 1500,
		position: {
				x: 'center',
				y: 'center'
			},
		content: value
	});
}

function initGetGps(){
  map.addListener('click', function(event) {
    var lat = event.latLng.lat();
    var lng = event.latLng.lng();
    $("shops-lat").set("value", lat);
    $("shops-lng").set("value", lng);
    $("gps-button").click();
  });

}

 
window.addEvent("domready", function() {
	count_maxlength();
});

// after collection controler addNotInRouteShop() method
function after_add_not_in_route_shop(){
  var closeModalDiv = $(parent.document.body).getElement('#close_modal');
  _click(closeModalDiv);

  window.location.reload();

}






