var lang = {
	'opravdu_smazat':'Opravdu smazat položku?',
	'opravdu_poslat_ridic':'Opravdu poslat řidiči?',
	'opravdu_poslat_sklad':'Opravdu poslat do skladu?',
	'vaha':'Váha',
	'rozmer':'Palet',
	'ridic_obsazen':'Řidič je již přiřazen',
	'neni_ridic':'Řidič není přiřazen',
	'opravdu_smazat':'Opravdu smazat?',
	'opravdu_smazat_vratit':'Opravdu smazat a vrátit zpět do výběru zakázek?',
	'opravdu_smazat_vratit_car':'Opravdu smazat a vrátit zpět do výběru automobilů?',
	'auto_ma_zakazky':'Auto má naloženy zakázky, odeberte zákázky',
	'neni_vybrana_adresa':'Není vybrána adresa',
	'mesice' : ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
	'mesice_short' : ['led', 'úno', 'bře', 'dub', 'kvě', 'čer', 'červ', 'srp', 'zář', 'říj', 'lis', 'pro'],
	'dny' :  ['ne', 'po', 'út', 'st', 'čt', 'pá', 'so'],
		
}