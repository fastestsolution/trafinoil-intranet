/*
Fst Context Menu
created Jakub Tyson Fastest Solution 
copyright 2016
*/
var FstMenu = this.FstMenu = new Class({
	Implements:[Options,Events],
	debug : false,
	
	// init fce
	initialize:function(options){
		this.setOptions(options);
		this.elem = options.element;
		
		this.init_events();
	},
	
	
	// init event to element right click
	init_events: function(){
		if (this.elem){
			this.elem.addEvent('contextmenu',this.open_menu.bind(this));
		}
	},
	
	// open context menu on cursor position 
	open_menu: function(e){
		e.stop();
		this.targetEl = e.target;
		this.pos_x = e.client.x;
		this.pos_y = e.client.y;
		
		this.create_menu();
		this.menu_events();
		this.close_menu_body();
	
	},
	
	// create ul>li list to menu
	create_menu: function(){
		this.menu = new Element('ul',{'id':'context_menu'}).inject($('addon'));
		this.menu.setStyles({
			'top':this.pos_y,
			'left':this.pos_x+10
		});
		if (this.options.list){
			Object.each(this.options.list,(function(item,k){
				new Element('li',{'data-fce':item.fce}).set('text',item.name).inject(this.menu);
				
			}).bind(this));
		}
		
	
	},
	
	// menu events on LI click
	menu_events: function(){
		this.menu.getElements('li').addEvent('click',(function(e){
			var fce = e.target.get('data-fce');
			if (typeof window[fce] != 'undefined'){
				window[fce](this.targetEl);
				
				this.close_menu();
			} else {
				alert('Funkce neni definovana');
			}
		}).bind(this));
	},
	
	// close context menu
	close_menu: function(){
		
		if (this.menu){
			this.menu.getElements('li').each(function(item){
				item.removeEvents('click');
			});
			this.menu.destroy();
		}
	},
	
	// close context menu from body click
	close_menu_body: function(){
		$('body').addEvent('click',(function(e){
			if (this.menu){
				if (this.get_inner(e.client,this.menu)){
					//console.log('je');
				} else {
					//console.log('neni');
					this.close_menu();
				}
			}
		}).bind(this));
	},
	
	// check if cursor inside element
	get_inner: function(cpos,element){
		var pos = element.getPosition();
		var size = element.getSize();
		if ((cpos.x > pos.x && cpos.x < pos.x+size.x) && (cpos.y > pos.y && cpos.y < pos.y+size.y)){
			return true;
		} else {
			return false;
		}
	}
	
	
	
	
		
	
	
});

function send_selected(target){
	//console.log(target);
	var car_list = [];
	$('load_canvas').getElements('.car').each(function(car){
		if (car.getElement('.checkbox').checked){
			car_list.push(car.get('data-id'));
		}
	});
	if (car_list.length > 0){
		console.log(car_list);
		alert('odeslat vybranne auta: '+car_list.join(','));
	} else {
		alert('nejsou vybranne auta');
		
	}
}

window.addEvent('domready', function () {
	//window.fstMenu = new FstMenu();
	//var s = $('body').getDimensions({computeSize: true});
	//console.log(s);
	//window.fstsystem.alert_fce('test');
});