/*
Fst History
created Jakub Tyson Fastest Solution
copyright 2016
*/
var FstSystem = this.FstSystem = new Class({
	Implements:[Options,Events],
	debug : false,

	// init fce
	initialize:function(options){
		this.setOptions(options);
		//console.log('run');

		this.table_items();
		this.init_complete_history();
		//console.log(window.getDimensions());
	},


	// init all table items
	table_items: function(){
		var myURI = new URI(window.location);

		// nastaven�
		var base = "https://"+myURI.parsed.host;
		this.modal = window.modal = new frameModal("modalWindow", {base : base});

		this.table_icons();
		this.table_mouse();
		this.table_filtration();

		this.table_top_action();


	},

	// top table action
	table_top_action: function(){
		date_picker();

		$$('.autocom').each(function(item){
			var id = item.get('id')+'s';
			//console.log(id);

			var myAutocompleter = new GooCompleter(item.get('id'), {
				action: item.get('data-url')+'/'+item.get('data-type'),	// JSON source
				param: 'search',			// Param string to send
				preloader: 'auto_loading_'+item.get('id'),
				fire_event: function(result){
					if ($(id)){
						$(id).value = result.key;
						$(id).fireEvent('change',$(id));
					}
					//console.log($(id));
					//console.log(result);

				},
				minlen: 0,
				listbox_offset: { y: 2 },	// Listbox offset for Y
				delay: 500					// Request delay to 0.5 seconds
			});
		});

	},

	// init after fst_history
	init_complete_history: function(){
		window.fsthistory.complete_fce = (function(){
			this.table_items();
		}).bind(this);
		//console.log(window.fsthistory);
	},

	// close modal
	close_modal: function(){
		if ($('close_modal')){
			_click($('close_modal'));
		}
	},


	// tabulky ovl�dan� my��tkem
	table_mouse: function(){
		if($$(".item-row")[0]){
			var c = new Clickable();
			var timer;

			$$(".item-row").addEvent("click", (function(e){
			  clearTimeout(timer);
			  timer = (function(){
				c.select(e.target.getParent('tr'));
			  }).delay(200, this);
			}).bind(this))

			$$(".item-row").addEvent("dblclick", (function(e){
			  clearTimeout(timer);

				_click(e.target.getParent('tr').getElement('.edit'));

			}).bind(this));

		  }
	},

	// table icons function
	table_icons: function(){
		if($$("a")){
			$$("a").addEvent("click", (Vars.click_icon_fce = function (e) {
				// ajax linky pro otevreni modalnich oken
				if (e.target.get("ajax")) {
					var url = e.target.get("href");

					this.modal.show(e.target);
					this.modal.setContent(url);
					e.stop();
				}
        else if(e.target.parentElement.get("ajax")){
          var url = e.target.parentElement.get("href");
          this.modal.show(e.target.parentElement);
          this.modal.setContent(url);
          e.stop();
        }
				// trash button
				if (e.target.hasClass('trash_button')){
					e.stop();
					var parent = e.target.getParent('tr');
					if (confirm(lang['opravdu_smazat'])){
						Vars.delete_reg = new Request.JSON({
							url:e.target.href,
							onComplete: (Vars.delete_req_complete = function(json){
								if (json.result == true){
									FstAlert(json.message);
									parent.destroy();
								} else {

									FstError(json.message);

								}
								area_data = null;
								json = null;
							}).bind(this)
						});
						Vars.delete_reg.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
						Vars.delete_reg.send();

					}
				}
			}).bind(this));
		}

	},

	// modal save events
	modal_save: function(e){
		e.stop();
		button_preloader(e.target);

    if(e.target.getParent('form').getElementById("event_type")){
      var t = e.target.getParent('form').getElementById("event_type");
      t.value = e.target.get("name");
    }

		VarsModal.save_reg = new Request.JSON({
				url:e.target.getParent('form').action,
				onComplete: (VarsModal.save_reg_complete = function(json){
					button_preloader(e.target);
					if (json.r == true){
            if(json.u){
              window.location.replace(json.u);
              return true;
            }

            var closeModalDiv = $(parent.document.body).getElement('#close_modal');
						if (closeModalDiv){
              Object.each(json, function(v, k){
                closeModalDiv.set( "data-"+k, v);
              });

              _click(closeModalDiv);
							//$(parent.document.body).getElement('#close_modal').fireEvent('click',$(parent.document.body).getElement('#close_modal'));
						}
						//FstAlert(json.m,true);

					} else {

						FstError(json.m);
						if (json.invalid){
							$$('.invalid').removeClass('invalid');
							json.invalid.each(function(item){
								//console.log(item);
								if($(item)){
									$(item).addClass('invalid');
								}
							});
						}
					}
					json = null;
				}).bind(this)
			});
			VarsModal.save_reg.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
			VarsModal.save_reg.post(e.target.getParent('form'));
	},

	// filtration table
	table_filtration: function(){
		if ($('filtration')){
      if($("FiltrCancel")) {
        $('FiltrCancel').removeEvents('click');
        $('FiltrCancel').addEvent('click', function (e) {
          e.stop();
          window.fsthistory.clear_history();
        });
      }
		}
	},

	drag_elements_events:function(){
		// show map
		$$('.show_map').removeEvents('click');
		$$('.show_map').addEvent('click',this.show_map.bind(this));

		$$('.element_drag').removeEvents('mouseenter');
		$$('.element_drag').addEvents({
			'mouseenter':function(e){
				if (e.target.getElement('.detail_hover')){
					e.target.getElement('.detail_hover').removeClass('none');
				}
			},
			'mouseleave':function(e){
				if (e.target.getElement('.detail_hover')){
					e.target.getElement('.detail_hover').addClass('none');
				}
			}
		});

	},

	// show map
	show_map: function(e){
		if (e){
			e.stop();
			if ($('show_map')){
				$('show_map').destroy();
			}
			if (!e.target.hasClass('open')){
				new Element('id',{'id':'show_map','data-lat':e.target.get('data-lat'),'data-lng':e.target.get('data-lng')}).inject(e.target);
				e.target.addClass('open');
			} else {
				e.target.removeClass('open');

			}
		}
		if (typeof google == 'undefined'){

			var script = document.createElement("script");
			script.type = "text/javascript";
			script.src = "http://maps.googleapis.com/maps/api/js?v=3&libraries=places,geometry&callback=init_google";
			document.body.appendChild(script);

		} else {
			//alert('');
			this.create_map();
		}
	},

	// create map
	create_map: function(){
		//var myLatLng = {lat: 49.8144897, lng: 18.2092833};
		//console.log($('show_map').get('data-lng'));
		//console.log(myLatLng);
		if ($('show_map')){
			var myLatLng = {lat: $('show_map').get('data-lat').toFloat(), lng: $('show_map').get('data-lng').toFloat()};

			var map = new google.maps.Map($('show_map'), {
				zoom: 4,
				disableDefaultUI: true,
				center: myLatLng
			});

			var marker = new google.maps.Marker({
				position: myLatLng,
				map: map,
				title: 'Adresa'
			});

			map.setZoom(12);
		}


	}





});

function init_google(){
	window.fstsystem.show_map();
}

//})();
window.addEvent('domready', function () {
	window.fstsystem = new FstSystem();
	//var s = $('body').getDimensions({computeSize: true});
	//console.log(s);
	//window.fstsystem.alert_fce('test');
});


function _click(el){

		if (Browser.name == 'ie' || Browser.name == 'unknown'){
			el.click();
		} else {
			event = document.createEvent( 'HTMLEvents' );
			event.initEvent( 'click', true, true );
			el.dispatchEvent( event );

		}
}