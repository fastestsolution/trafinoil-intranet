function after_collections_callEnd(id, not){
  var tr = $("all-shops").getElement("tr[data-id="+id+"]");
  if (tr) {
    tr.getElement(".fa-phone").getParent().hide();
    tr.getElement(".fa-check").getParent().hide();
    tr.getElements("input").set("disabled", true).set("placeholder", "");

    if (not != 1) {
      tr.inject($("in-shops").getElement("tbody"));
    }
    else {
      tr.inject($("out-shops").getElement("tbody"));
    }

    checkRows();
    send_sorting();
  }
}

function after_collections_callEnd_not(id){
  after_collections_callEnd(id, 1)
}

function after_collections_call_revert(id){
  $("fst_hide").show();
  var tr = $$(".collection-shops tr[data-id="+id+"]");
  tr.getElement(".fa-phone").getParent().show();
  tr.getElement(".fa-check").getParent().show();
  var inpts = tr.getElements("input");
  inpts.each(function(inp){
    inp.set("disabled", false);
  });

  tr.inject("all-shops");
  checkRows();
  send_sorting();
}

function checkRows(){
  var check  = $$("#all-shops tbody").getElements("tr");
  if(check.length < 1){
    $("fst_hide").hide();
  }

  var checkDone  = $("in-shops").getElement("tbody").getElements("tr");
  $("shop-don-in-count").innerHTML = ((checkDone.length));
  if(checkDone.length > 24){
    $("saveCollection").removeClass("disabled");
  }
  else{
    $("saveCollection").addClass("disabled");
  }

  if($("out-shops").getElement("tbody").getElements("tr")) {
    var checkOut = $("out-shops").getElement("tbody").getElements("tr");
    $("shop-don-out-count").innerHTML = (checkOut.length);
  }
  else{
    $("shop-don-out-count").innerHTML = (0);
  }
}

window.addEvent('domready', function(){
  $$(".collection-shops .fa-remove, .collection-shops .fa-check").addEvent("click", function(e){
    var completedata;
    var this_tr = $(this).getParent("tr");
    var req = new Request.JSON({
      url: $(this).getParent().get("href"),
      data:
      {
        "call_note" : this_tr.getElement(".call_note").value,
        "marketing_note" : this_tr.getElement(".marketing_note").value,
        "shop_note" : this_tr.getElement(".shop_note").value,
      },
      onComplete : completedata = (function(json){
        if(json.r == true){
          eval(json.s);
        }
      }).bind(this)
    });
    req.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
    req.send();
    e.stop();
  });


  $$("#start-type, #end-type").addEvent("change", function(){
    var req = new Request.JSON({
      url: "/collections/get-adress/",
      data:
      {
        "type" : this.value,
        "carrier" : $("carrier-id").value
      },
      onComplete : function(json){
        var adress = "";
        if(json.adress) {
          adress = json.adress;
        }

        if (this.get("id") == "start-type") {
          $("start").value = adress;
        }
        if (this.get("id") == "end-type") {
          $("end").value = adress;
        }
      }.bind(this)
    });
    req.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
    req.send();
  });

});