﻿var Multiselect = new Class({

  initialize:function(element, options){
    this.element = element;
    this.divOptionWrapper = new Element("div", {"class" : "optionWrapper"});
    this.divWrapper = new Element("div", {"class" : "multiselect"});
    this.selectedWrapper = new Element("div", {"class" : "selectedWrapper"});

    this.injectWrappers();
    this.makeSelectbox();
    this.setActions();

  },

  injectWrappers : function(){

    var elementWrapper = this.element.getParent();
    this.divWrapper.inject(elementWrapper, 'before').adopt(this.selectedWrapper).adopt(this.divOptionWrapper);
    elementWrapper.hide();
  },

  makeSelectbox : function(){
    var options = $(this.element).getElements("option");
    var DivOptions;
    var wrapper = this.divOptionWrapper;
    if(options.length > 0){
      options.each(function(o){

        var option = new Element("div", {"class" : "option"+((o.get("selected"))? " selected" : ""), "data-value" : o.value, "html" : o.innerHTML});
        wrapper.adopt(option);

        if(o.get("selected")){
          this.setDiv(o.value, o.innerHTML);
        }

      }.bind(this));
    }
  },

  setActions : function(){
   var element = this.element;

   var options = this.divOptionWrapper.getElements(".option").addEvent("click", function(e){
    if($(e.target).hasClass("selected")){
      $(e.target).removeClass("selected");
      element.getElement("option[value="+e.target.get("data-value")+"]").set("selected", "");
      this.removeDiv(e.target.get("data-value"));
    }
     else{
      $(e.target).addClass("selected");
      element.getElement("option[value="+e.target.get("data-value")+"]").set("selected", "selected  ");

      this.setDiv(e.target.get("data-value"), e.target.innerHTML);
    }
   }.bind(this));

   this.selectedWrapper.addEvent("click", function(e){
     this.divOptionWrapper.toggle();
   }.bind(this));

  },

  setDiv : function(value, text){
    var div = new Element("div", {"class" : "option-selected", "data-value" : value, "html" : text});
    this.selectedWrapper.adopt(div);
  },

  removeDiv : function(value){
    this.selectedWrapper.getElement(".option-selected[data-value="+value+"]").destroy();
  }
});