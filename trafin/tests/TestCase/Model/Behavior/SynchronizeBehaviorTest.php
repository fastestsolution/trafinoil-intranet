<?php
namespace App\Test\TestCase\Model\Behavior;

use App\Model\Behavior\SynchronizeBehavior;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Behavior\SynchronizeBehavior Test Case
 */
class SynchronizeBehaviorTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Behavior\SynchronizeBehavior
     */
    public $Synchronize;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->Synchronize = new SynchronizeBehavior();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Synchronize);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
