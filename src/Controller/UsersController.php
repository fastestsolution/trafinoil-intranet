<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;

class UsersController extends AppController
{

  public function initialize()
  {
    parent::initialize();
    //$this->Auth->allow("edit");
   // $this->loadHelper("Session");
  }

	public function login(){
    $this->viewBuilder()->layout("ajax");
	  $this->set("title", __("Přihlášení do systému ".appName));

	if ($this->request->is('post')) {
     // $pass = '0b4e6ea0e1b4dd85d938207f9b42d359';
	  //pr($pass);
//	  if ($this->request->data['username'] == 'superadmin' && md5($this->request->data['password']) == $pass){
//		$user = [
//			'id'=>-1,
//			'username'=>'Superadmin',
//			'name'=>'Superadmin',
//			'group_id'=>1,
//			'system_id'=>1,
//		];
//	  } else {

			$user = $this->Auth->identify();
	  //}
	  if ($user) {
        // pokud chceš řešit oprávnění, tohle zakomentuj a to podtím odkomentuj
        $this->Auth->setUser($user);
        //pr($user);
		//pr($this->disable_menu);
		if (isset($this->disable_menu[$user['group_id']])){
			$user['disable_menu'] = $this->disable_menu[$user['group_id']];

		}
		$this->Auth->setUser($user);
		//pr($user);
		//die();
		return $this->redirect($this->Auth->redirectUrl());

        //$this->loadModel("UserAuths");
        // připravené pro rozšířená oprávnění odkomentuj pokud chceš pracovat s oprávněními.
        // v akci add připiš vytvoření defaulního oprávnění při registraci
        // pokud uživatel nemá definována žádná oprávnění, tohle hodí exception!
        //$auths = $this->UserAuths->findByUserId($user["id"])->select(["auth"])->first();

        //        if(!empty($auths)){
        //          $user["auth"] = unserialize($auths->auth);
        //          $this->Auth->setUser($user);
        //          return $this->redirect($this->Auth->redirectUrl());
        //        }
        //        else {
        //          throw new NotFoundException(__("Uživatel nemá definované oprávnění"));
        //        }
      } else {
        $this->Flash->error(__('Uživatelské jméno nebo heslo je nesprávně zadané'), [
          'key' => 'auth'
        ]);
      }
    }
  }

  public function logout(){
    return $this->redirect($this->Auth->logout());
  }

  public function index()
  {
	  $data = $this->Users->find()->select(["id", "username", "name", "phone"]);

	  $table = $this->AutoTable->newTable();
	  $table->data($data);
	  $table->action("edit");
	  $table->topAction("edit", __("Nový uživatel"));
	  $this->AutoTable->render($table);
  }



  public function edit($id=null){
	  $this->set("title", __("Nový uživatel"));
    $this->viewBuilder()->layout("ajax");
    $entity = $this->Users->newEntity();
	if ($id != null){
		$entity = $this->Users->get($id);
		$entity->password2 = $entity->password;
		$this->set("title", __("Editace uživatele").$entity->name);
	}

	if ($this->request->is("post")){
	  $this->Users->patchEntity($entity, $this->request->data());
	  $this->check_error($entity);

	  if ($result = $this->Users->save($entity)){
        die(json_encode(['r'=>true,'m'=>__('Uloženo'),'id'=>$entity->id]));
		} else {
        die(json_encode(['r'=>false,'m'=>__('Chyba uložení')]));
	  }
    }

    $this->set(compact("entity"));
  }


  public function editAuth($user_id){

    $user = $this->Users->get($user_id);
    $uat = TableRegistry::get("UserAuths");
    $userAuth = $uat->findByUserId($user_id)->first();

    if($this->request->is("post")){
      if($this->Users->editAuth($userAuth->id, $this->request->data())){
        $this->Flash->success(__("Práva byla úspěšně upravena."));
        $this->redirect(['controller' => 'users', 'action' => 'edit', $user_id]);
      }
    }

    $this->set("user_auth", unserialize($userAuth->auth));
    $this->set("auths", Configure::read("Auths.Auths"));
    $this->set("auth_names", Configure::read("Auths.AuthNames"));
    $this->set("user", $user);
  }

}
