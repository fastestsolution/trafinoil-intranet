<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Utility\Security;

class CarriersController extends AppController
{

  public function initialize()
  {

	  parent::initialize();

	  $this->set("title", __("Dopravci"));

    if(isset($_GET["back"])){
      $this->set("back", $_GET["back"]);
    }

    $this->Security->config('unlockedActions', ['find']);
  }

  public function index(){
    $query = $this->Carriers->find()->select([
      'id',
	    'shortcut',
      'name',
	    'c_name',
	    'ic',
	    'stock_id',
	    'psc',
	    'town',
	    'street',
	    'mobile',
	    'phone',
	    'bank_account',
	    'email'
    ])
    ->where(["status" => 1]);

	  $table = $this->AutoTable->newTable();
	  $table->data($query);
	  $table->action("edit");
	  $table->topAction("edit", __("Nový dopravce"));

    $this->AutoTable->render($table);
  }

  public function edit($id = null){
    $this->viewBuilder()->layout("ajax");

    if(isset($id)){
      $entity = $this->Carriers->find()->where(["Carriers.id" => $id])->first();
	    $this->set("title", $entity->name);
    }
    else{
      $entity = $this->Carriers->newEntity();
	    $this->set("title", __("Nový dopravce"));
    }

    if($this->request->is("ajax")){
      $this->Carriers->patchEntity($entity, $this->request->data());
      $this->check_error($entity);
      if($this->Carriers->save($entity)){
        die(json_encode(['r'=>true,'m'=>__('Uloženo'), 'u' => ((isset($this->request->data["back"]))? $this->request->data["back"]."?company_id=".$entity->id : null), 'id'=>$entity->id]));
      } else {
        die(json_encode(['r'=>false,'m'=>__('Chyba uložení')]));
      }
    }

    $this->set("entity", $entity);
  }

  public function find(){
    if(isset($this->request->data["search"])){
      $finds = $this->Carriers->find("list")->where(["Carriers.name LIKE" => "%".$this->request->data["search"]."%"])->orWhere(["ic" => $this->request->data["search"]])->hydrate(false);

      if($finds->count() > 0){
        die(json_encode($finds->toArray()));
      }
      else{
        die("ne");
      }
      die();
    }
  }

}