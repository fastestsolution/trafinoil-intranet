<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;

class AuthorizationsController extends AppController
{

  public function initialize()
  {
    parent::initialize();
  }

  public function index()
  {
	  $data = $this->Authorizations->find();

	  $options = array(
		  'top_action'=>[
			  'edit'=>__('Přidat'),
		  ],
		  'filtr'=>[
			  'name'=>__('Název').'|Routes__name|text_like',
			  //'size'=>__('Palet').'|Cars__size|text',
			  //'car_type'=>__('Typ').'|Cars__car_type|select|car_type_list',
		  ],
		  'posibility'=>[
			  'edit'=> '<span class="fa fa-edit">',
			  'trash'=>__('Smazat'),
		  ],
	  );

	  $this->AutoTable->render($data, $options);
  }
 
 
  public function edit($id=null){
    $this->set("title", __("Editace oprávnění"));
    $this->viewBuilder()->layout("ajax");

	  if ($id != null){
			$entity = $this->Authorizations->get($id);
		}
	  else{
		  $this->set("title", __("Nové oprávnění"));
		  $entity = $this->Authorizations->newEntity();
	  }

		if ($this->request->is("ajax")){
		  $this->Authorizations->patchEntity($entity, $this->request->data());
		  $this->check_error($entity);

		  if ($result = $this->Authorizations->save($entity)) {
	        die(json_encode(['r'=>true,'m'=>__('Uloženo'),'id'=>$entity->id]));
			} else {
	        die(json_encode(['r'=>false,'m'=>__('Chyba uložení')]));
		  }
    }else {
			$ut = TableRegistry::get("Users");
			$users = $ut->find()->join(["UserAuthorizations" =>
				[ "table" => "user_authorizations",
					"conditions" => "UserAuthorizations.user_id = Users.id"
				]])->where(["UserAuthorizations.authorization_id" => $id])->hydrate(false)->toArray();
		}



    $this->set(compact("entity", "users"));
  }
}
