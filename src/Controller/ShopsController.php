<?php
namespace App\Controller;

use App\Controller\AppController;
use AutoTable;
use Cake\Core\Configure;


/*
 * Martin Hrabal
 * Controller Shops - seznam provozoven
 * */
class ShopsController extends AppController
{

  public function initialize(){
    parent::initialize();
	  $this->set("title", __("Provozovny"));
  }

  public function index(){

    $this->loadModel("Suppliers");
    $this->suppliers = $this->Suppliers->find("list")->toArray();
    $this->set("suppliers", $this->suppliers);


	  $table = $this->AutoTable->newTable();

    $query = $this->Shops->find()
      ->select(
      [
        "Shops.id", "Shops.name",
        "Shops.street", "Shops.town",
	      "color" => "Shops.color"
      ]
    )
      ->contain("Companies")
      ->join([
        "ShopPacks" => [
          'table' => 'shop_packs',
          'type' => 'left',
          'conditions' => 'ShopPacks.shop_id = Shops.id'
        ]
      ])
      ->group("Shops.id");

	  $table->data($query);
	  $table->topAction("edit", __("Nová provozovna"));
	  $table->filter("id", __("Id"));
	  $table->filter("ic", __("Ič"));
	  $table->filter("name", __("Název"));
	  $table->filter("phone", __("Telefon"));
	  $table->filter("region_id", __("Region"));
	  $table->filter("town", __("Obec"));
	  $table->filter("street", __("Ulice"));
	  $table->filter("country_id", __("Stát"));
	  $table->filter("color", __("Barva"), ["options" => Configure::read("select_config.shop_colors"), "empty" => __("Všechny")]);
	  $table->filter("carrier_id", __("Dopravce"), ["empty" => __("Všechny")]);
	  $table->filter("supplier_id", __("Dodavatel"), ["empty" => __("Všichni")]);
	  $table->filter("user_id", __("Obchodník"),  ["empty" => __("Všichni")]);
	  $table->filter("franchise_id", __("Franšíza") , ["empty" => __("Všechny")]);
	  $table->filter("kos", __("Stav"));
	  $table->filter("route_id", __("Trasa"));
	  $table->filter("iscc", __("ISSC"), ["options" => Configure::read("select_config.bool")]);

	  $table->action("edit");
	  $table->listValue("iscc", Configure::read("select_config.bool"));

    $this->AutoTable->render($table);
  }

  public function edit($id = null){
	  $title = __("Editace provozovny");
    $this->viewBuilder()->layout("ajax");

    if(isset($id)){
      $entity = $this->Shops->find()
        ->where(["Shops.id" => $id])
        ->contain(["Companies", 'ShopPacks', 'ShopPotentials'])
        ->first();

	      $title = $entity->name;
    }

    else{
      $entity = $this->Shops->newEntity();
    }

    if(isset($this->request->query["company_id"])){
      $this->loadModel("Companies");
      $company = $this->Companies->find()->where(['id' => $this->request->query["company_id"]])->select(["name"])->first();
      $entity = $this->Shops->newEntity(['company' => ['name' => ""]]);
      $entity->company_id = $this->request->query["company_id"];
      $entity->company->name = $company->name;
    }

    if($this->request->is("ajax")){
      $this->Shops->patchEntity($entity, $this->request->data(), ['associated' => ['ShopPacks', 'ShopPotentials']]);
      $this->check_error($entity);
      if($this->Shops->save($entity,
        [
          'associated' => ['ShopPacks', 'ShopPotentials']
        ])
      ){
        die(json_encode(['r'=>true,'m'=>__('Uloženo'),'id'=>$entity->id]));
      }
      die(json_encode(['r'=>false,'m'=>__('Chyba uložení'),'id'=>$entity->id]));
    }

    $this->loadModel("Suppliers");
    $suppliers = $this->Suppliers->find("list")->where(["kos" => 0]);

    $this->loadModel("Users");
    $users = $this->Users->find("list")->where(["kos" => 0]);

    $this->set("users", $users);
    $this->set("suppliers", $suppliers);
    $this->set("entity", $entity);
	  $this->set("title", $title);
  }

	public function view($shop_id){
		$this->viewBuilder()->layout(false)->template("map-view");
		if(!isset($shop_id)){
			throw new Exception(__("ID provozovny není zadáno"));
		}
		$d = $this->Shops->find()->where(["id" => $shop_id]);

		die(json_encode(["data" => $d, "html" => $this->render()->body()]));
	}



}

