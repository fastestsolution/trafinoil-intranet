<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Exception\Exception;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;

class CallsController extends AppController
{

	public function call($shop_id, $collection_id = null, $route_id = null){
		if(!isset($shop_id)){
			throw new Exception(__("ID provozovny není zadáno"));
		}
		$this->viewBuilder()->layout("ajax");

		$entity = $this->Calls->newEntity();

		if($this->request->is("ajax")) {

			$this->request->data["date"] = new Time();
			$this->request->data["user_id"] = $this->Auth->user("id");

			$this->Calls->patchEntity($entity, $this->request->data());
			if($this->Calls->save($entity)) {
				// pokud klikl, že nechce zařadit do svozu
				if($this->request->data("event_type") == "not"){
					$resFn = "collections_callEnd_not";
				}
				else{
					$resFn = "collections_callEnd";
				}

				$this->loadModel("CollectionShops");
				if($this->CollectionShops->set($this->request->data("shop_id"), $this->request->data("collection_id"), $this->request->data("route_id"), (($this->request->data("event_type") == "not")? 0 : 1))){
					parent::result(true, $entity->id, null, [$resFn => $this->request->data("shop_id")]);
				}
			}
			else{
				parent::result(false, __("Nepovedlo se uložit záznam"));
			}
		}
		else{
			$st = TableRegistry::get("Shops");
			$shop = $st->find()->where(["id" => $shop_id])->contain("ShopPotentials")->first()->toArray();
			$title = __("Hovory - Provozovna ").$shop["name"];

			$this->set(compact(["entity", "shop_id", "shop", "title", "collection_id", "route_id"]));
		}

	}

}

