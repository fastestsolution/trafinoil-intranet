<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security;
use Symfony\Component\Console\Helper\Table;
use Cake\Event\Event;

class RoutesController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->set("title", __("Trasy"));
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->Security->config("unlockedActions", ["addShopToRoute"]);
    }

    public function index() {
        $query = $this->Routes->find()->select(["id", "name", "region_id", "distance"])->where(["status" => 1, "kos IS NULL"]);
		    $table = $this->AutoTable->newTable();
		    $table->data($query);
		    $table->action("edit");
	      $table->action("/collections/add/", '<span class="fa fa-truck" title="' . __("Nový svoz") . '">');
		    $table->topAction("edit", __("Nová trasa"));

	      $table->listValue("region_id", $this->regions->toArray());
	      $table->postCol("CarrierRoutes", 'Routes.CarriersWithIds');


        $options = array(
            'top_action' => [
                'edit' => __('Přidat'),
            ],
            'filtr' => [
                'name' => __('Název') . '|Routes__name|text_like',
            ],
            'list' => [
                'region_id' => $this->regions->toArray(),
            ],
            'posibility' => [
                'edit' => '<span class="fa fa-edit" title="' . __("Upravit") . '">',
	              '/collections/add/' => '<span class="fa fa-truck" title="' . __("Nový svoz") . '">',
                'trash' => __('Smazat'),
            ],
            'post_cols' => [0 => ['name' => 'CarrierRoutes', ]]
        );

        $this->AutoTable->render($table);
    }

    public function edit($id = null) {

        $this->viewBuilder()->layout("ajax");

        if (isset($id)) {
            $this->loadModel("CarrierRoutes");
            $entity = $this->Routes->find()->where(["Routes.id" => $id])->first();
            $this->set("title", __("Trasa") . " " . $entity->name);
            $this->set("selected_carriers", $this->Routes->getCarriers($id)->toArray());
        } else {
            $entity = $this->Routes->newEntity();
            $this->set("title", __("Nová trasa"));
        }

        if ($this->request->is("ajax")) {
            if (isset($this->request->data["shops_sort"])) {
                $rs = TableRegistry::get("RouteShops");
                $sort = explode(",", $this->request->data["shops_sort"]);
                foreach ($sort as $k => $id) {
                    $rs->query()->update()->set(["ord" => $k])->where(["shop_id" => $id, "route_id" => $this->request->data["id"]])->execute();
                }
            }

            $this->Routes->patchEntity($entity, $this->request->data());


            $this->check_error($entity);
            if ($this->Routes->save($entity)) {

                // překontroluje a uloží přidružené dopravce
                $this->Routes->saveAssocCarriers($entity->id, $this->request->data["carriers_ids"]);

                die(json_encode(['r' => true, 'm' => __('Uloženo'), 'id' => $entity->id]));
            } else {
                die(json_encode(['r' => false, 'm' => __('Chyba uložení')]));
            }
        } else {
            $this->loadModel("RouteShops");
            $shopsInRoute = $this->RouteShops->find()->where(["route_id" => $id, "RouteShops.kos IS NULL"])
	            ->select($this->RouteShops)
	            ->select($this->RouteShops->Shops)
	            ->select(["Companies.id", "Companies.name", "Companies.dph"])
	            ->join([
		            "Shops" =>
		            [
			            'table' => "shops",
			            'conditions' => 'Shops.id = RouteShops.shop_id'
		            ],
		            "Companies" =>
			            [
				            'table' => "companies",
				            'conditions' => 'Companies.id = Shops.company_id'
			            ],
	            ])
	            ->order("ord");

            $this->set("shopsInRoute", $shopsInRoute);
        }

        $this->set("entity", $entity);
    }

    public function find() {
        if (isset($this->request->data["search"])) {
            $finds = $this->Routes->find("list")->where(["Routes.name LIKE" => "%" . $this->request->data["search"] . "%"])->orWhere(["ic" => $this->request->data["search"]])->hydrate(false);

            if ($finds->count() > 0) {
                die(json_encode($finds->toArray()));
            } else {
                die("ne");
            }
            die();
        }
    }

    public function addShopToRoute($route_id, $shop_id = null) {
        $this->set("title", __('Přidat provoz do trasy'));
        $this->viewBuilder()->layout("ajax");

        $this->set("back", $back = "/routes/edit/" . $route_id . "?tab=provozovny");

        if (!isset($shop_id) && !isset($this->request->data["shops"])) {
            // již přidané provozovny
            $rs = TableRegistry::get("RouteShops");
            $rsq = $rs->find()->where(["route_id" => $route_id])->select("shop_id");

            $data = $this->AutoTable->data("Shops")
                    ->select(
                            [
                                "Shops.id", "Shops.name", "Shops.bcontact_name",
                                "Shops.psc", "Shops.town", "Shops.street",
                                "Shops.region", "iscc",
                                "Shops.bcontact_mobile", "Shops.bcontact_phone", "Shops.contact_mobile", "bcontact_email"
                            ]
                    )
                    ->contain("Companies")
                    ->group("Shops.id")
                    ->where(["Shops.id NOT IN" => $rsq]);


            $options = array(
                'menu' => [
                    "sendSelected" => ['name' => __("Odeslat vybrané"), 'url' => '/routes/add-shop-to-route/' . $route_id],
                    "deselect" => ['name' => __('Zrušit výběr')]
                ],
                'filtr' => [
                    'name' => __('Název') . '|Shops__name|text_like',
                    'ic' => __('IČO') . '|Companies__ic|text_like',
                    'street' => __('Ulice a čp.') . '|Shops__street|text_like',
                    'town' => __('Město') . '|Shops__town|text_like',
                    'country_id' => __('Stát') . '|Companies__coutry_id|select|countries',
                ],
                'list' => [
                    'iscc' => [0 => __("ne"), 1 => __("ano")],
                ],
                'links' => [
                    'add-shop-to-route/' . $route_id => '<span class="fa fa-check">',
                ],
            );

            $this->AutoTable->render($data, $options);
        } else {
            $rs = TableRegistry::get("RouteShops");
            if (isset($this->request->data["shops"])) {
                foreach ($this->request->data["shops"] as $shop_id) {
                    $entity = $rs->newEntity();
                    $entity->route_id = $route_id;
                    $entity->shop_id = $shop_id;
                    $rs->save($entity);
                }
                die(json_encode(["r" => true, 'u' => $back]));
            } else {
                if (isset($shop_id)) {
                    $entity = $rs->newEntity();
                    $entity->route_id = $route_id;
                    $entity->shop_id = $shop_id;
                    $rs->save($entity);
                    $this->redirect($back);
                }
            }
        }
    }

    public function getShopsInRoute($route_id = null, $shop_id = null) {
        $result = $this->Routes->getShopsInRoute($route_id, $shop_id);
        die(json_encode($result));
    }

    public function map() {
        $this->viewBuilder()->template("modals/map");
    }

		public function countShops($route_id){
			$this->viewBuilder()->layout(false);
			$this->loadModel("Routes");
			$shops = $this->Routes->countShops($route_id);
			$this->set("shops", $shops);
		}

}
