<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Utility\Security;

class CompaniesController extends AppController
{

  public function initialize()
  {
	  parent::initialize();
    $this->set("title", __("Firmy"));
  }

  public function index(){
    $query = $this->Companies->find()->select([
      'id',
      'name',
      'contact_name',
      'ic',
      'dph',
      'town',
      'street',
      'mobile',
      'phone',
      'email'
    ]);

	  $table = $this->AutoTable->newTable();
	  $table->data($query);
	  $table->topAction("edit", __("Nová firma"));
	  $table->action("edit");
	  $table->filter("name", __("Název"));
	  $table->filter("ico", __("IČ"));
	  $table->filter("town", __("Město"));
	  $table->filter("street", __("ulice"));
	  $table->filter("mobil", __("Mobil"));
	  $table->filter("phone", __("phone"));
	  $table->filter("email", __("Email"));
	  $this->AutoTable->render($table);

    $options = array(
      'top_action'=>[
        'edit'=>__('Přidat'),
      ],
      'filtr'=>[
        'name'=>__('Název').'|Companies__name|text_like',
      ],
      'list'=>[
        //'car_type'=>$this->car_type_list,
      ],
      'posibility'=>[
        'edit'=> '<span class="fa fa-edit">',
        'trash'=>__('Smazat'),
      ],
    );
  }

  public function edit($id = null){
    $this->viewBuilder()->layout("ajax");

    if(isset($id)){
      $entity = $this->Companies->find()->where(["Companies.id" => $id])->first();
    }
    else{
      $entity = $this->Companies->newEntity();
    }

    if($this->request->is("ajax")){
      $this->Companies->patchEntity($entity, $this->request->data());
      $this->check_error($entity);
      if($this->Companies->save($entity)){
        die(json_encode(['r'=>true,'m'=>__('Uloženo'), 'u' => ((isset($this->request->data["back"]))? $this->request->data["back"]."?company_id=".$entity->id : null), 'id'=>$entity->id]));
      } else {
        die(json_encode(['r'=>false,'m'=>__('Chyba uložení')]));
      }
    }

    $this->set("entity", $entity);
  }

  public function find(){
    if(isset($this->request->data["search"])){
      $companies = $this->Companies->find("list")->where(["Companies.name LIKE" => "%".$this->request->data["search"]."%"])->orWhere(["ic" => $this->request->data["search"]])->hydrate(false);

      if($companies->count() > 0){
        die(json_encode($companies->toArray()));
      }
      else{
        die("ne");
      }
      die();
    }
  }

}