<?php
namespace App\Controller\Component;

use Cake\Mailer\Email;
use Cake\Controller\Component;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;


class AutoTableComponent extends Component{



  public function initialize(array $config)
  {
    // config
    $this->controller = $this->_registry->getController();
  }

	public function newTable(){
		return new AutoTable($this->controller);
	}




  public function render($table){

	  if(!empty($this->request->query)){
		  $table->query->where($this->convert_conditions());
	  }

	  $this->controller->set('items', $this->controller->paginate($table->query));
	  unset($table->query);
    $this->controller->set('params', $table);

    if ($this->request->is('ajax')) {
      $this->controller->viewBuilder()->layout(false);
      $this->controller->render('/AutoTable/items');
    } else {
      $this->controller->render('/AutoTable/index');
    }
  }




  public function convert_conditions($con = null){
    $conditions = array();
    $disable_keys = [
      'h',
      'sort',
      'direction',
    ];

    if (isset($this->request->query) && !empty($this->request->query)){
      //pr($this->request);

      foreach($this->request->query AS $key=>$value){
	      if($key == "page"){
		      continue;
	      }
        $key = explode('|',$key);
        if (in_array($key[0],$disable_keys)){
          continue;
        }

        $key[0] = strtr($key[0],array('__'=>'.'));
        //pr($key);

        // like conditions
	      if(isset($key[1])){
	        if ($key[1] == 'like'){
	          $conditions[$key[0].' '.$key[1]] = '%'.$value.'%';
	        }
	        else if($key[1] == 'opt'){
						$chunks = explode(".", $key[0]);
		        if($value == 1) {
			        $conditions[$chunks[0] . "." . $chunks[1] . " IN"][] = $chunks[2];
		        }
	        }
	        else if ($key[1] == 'date'){
	          $value_tmp = explode('.',$value);
	          $conditions[$key[0]] = $value_tmp[2].'-'.$value_tmp[1].'-'.$value_tmp[0];
	          //pr($conditions);
	        }
	      }
	      else {
          $conditions[$key[0]] = $value;
				}
      }
    }

    return $conditions;
  }
}

Class AutoTable{
	public $query;
	public $actions = [];
	public $topActions = [];
	public $filters = [];
	public $controller = "";
	public $postCols = [];

	public function __construct($controller)
	{
		$this->controller = $controller->name;
	}


	public function data($query){
		$this->query = $query;
	}

	public function topAction($action, $html = null){
		if(!isset($html)){
			$html = $this->findAction($action);
		}
		$this->topActions[$action] = $html;
	}

	public function filter($key, $name, array $opt = null){

		$model = $this->controller;
		$type = "text";
		$options = null;
		$col = $key;


		$this->filters[$key] = ["name" => $name, "col" => $col, "model" => $model, "type" => $type];

		if(isset($opt)){
			if(isset($opt["options"])){
				$this->filters[$key]["options"] = $opt["options"];
			}
			if(isset($opt["col"])){
				$this->filters[$key]["col"] = $opt["col"];
			}
			if(isset($opt["model"])){
				$this->filters[$key]["model"] =  $opt["model"];
			}
			if(isset($opt["type"])){
				$this->filters[$key]["type"] =  $opt["type"];
			}
			if(isset($opt["empty"])){
				$this->filters[$key]["empty"] = $opt["empty"];
			}

		}
	}

	public function action($action, $html = null){
		if(!isset($html)){
			$html = $this->findAction($action);
		}
		$this->actions[$action] = $html;
	}

	public function listValue($name, array $list){
		$this->listValues[$name] = $list;
	}

	public function postCol($name, $method){
		$this->postCols[$name] = $method;
	}

	private function findAction($action){
		$actions = [
			'edit' => '<span class="fa fa-edit"></span>'
		];

		if(isset($actions[$action])){
			return $actions[$action];
		}
		else{
			Throw new Exception(__("Undefined action. Specify your own html or use predefined action."));
		}
	}

}