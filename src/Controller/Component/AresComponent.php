<?php

namespace App\Controller\Component;

use Cake\Mailer\Email;
use Cake\Controller\Component;
use Cake\ORM\Entity;
use Cake\Network\Exception\NotFoundException;
use Cake\Network\Exception\ServiceUnavailableException;

class AresComponent extends Component {

    var $ares_url = 'http://wwwinfo.mfcr.cz/cgi-bin/ares/darv_bas.cgi?ico=';

    //var $ares_url = 'http://www.fastest.cz';

    public function search($ic = 28591232) {
        $this->ic = $ic;
        $this->getData();
        $this->parseData();

        return $this->result;
    }

    private function getData() {

        if ($curl = curl_init($this->ares_url . $this->ic)) {
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $content = curl_exec($curl);
            //$info = curl_getinfo($curl);
            curl_close($curl);
            $this->data = $content;
            //pr($content);
        }
    }

    private function parseData() {
        $result = array();
        if (isset($this->data)) {
            $xml = @simplexml_load_string($this->data);
            $ns = $xml->getDocNamespaces();

            $data = $xml->children($ns['are']);
            $el = $data->children($ns['D'])->VBAS;

            if (strval($el->ICO) == $this->ic) {

                $result['ico'] = strval($el->ICO);
                $result['dic'] = strval($el->DIC);
                $result['firma'] = strval($el->OF);
                $result['ulice'] = strval($el->AA->NU) . ' ' . strval($el->AA->CD) . '/' . strval($el->AA->CO);
                $result['mesto'] = strval($el->AA->N) . '-' . strval($el->AA->NCO);
                $result['psc'] = strval($el->AA->PSC);
                $result['stat'] = strval($el->AA->NS);
                $result['dph'] = substr(strval($el->PSU), 5, 1) == 'A' ? 1 : 0; //Platce dph je urcen 6. pismenem z atributu PSU
                $result['result'] = true;
            } else {
                $result['result'] = false;
                $result['message'] = 'IČ firmy nebylo nalezeno';
            }
        } else {
            $result['result'] = false;
            $result['message'] = 'Databáze ARES není dostupná';
        }
        $this->result = $result;
    }

    public function checkDph($ic) {
        $this->ic = $ic;
        $this->getData();

        $result = array();
        if (isset($this->data)) {
            $xml = @simplexml_load_string($this->data);
            $ns = $xml->getDocNamespaces();

            $data = $xml->children($ns['are']);
            $el = $data->children($ns['D'])->VBAS;

            if (strval($el->ICO) == $this->ic) {

                $dph = substr(strval($el->PSU), 5, 1); //Platce dph je urcen 6. pismenem z atributu PSU
                if ($dph == 'A') {
                    $result['return'] = true;    //Je platcem DPH
                    $result['message'] = 'Je platcem DPH';
                } else {
                    $result['return'] = false;   //Neni platcem DPH
                    $result['message'] = 'Neni platcem DPH';
                }
            } else {
                $result['return'] = false;
                $result['message'] = 'Firma s IC ' . $this->ic . ' nebyla nenalezena';
            }
        } else {
            throw new ServiceUnavailableException(__('Databáze ARES není dostupná'));
        }
        
        return $result;
    }

}
