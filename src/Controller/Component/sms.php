<?php
namespace App\Controller\Component;

use Cake\Controller\Component;

class SmsComponent
{
  var $text_body = null;
  var $to = null;
  var $controller;

  function startup(&$controller)
  {
    $this->controller = &$controller;
    //$htmlRequest = new HTTP_Request();
  }

  function send($user, $recipient_list = array(), $replace_list = array())
  {
    $output = array();
    $message = 'Vaše objednávka je na cestě.';
    foreach ($recipient_list as $phone_number) {
      $SMS = '<?xml version="1.0" encoding="utf-8" ?> <batch id=""> <request>textSMS</request>  <recipient>+' . $phone_number . '</recipient> <content>' . $message . '</content>  <udh />  <delivery_report>0</delivery_report><custid>Pokladna - ' . $user . '</custid>   </batch>';
      $context = stream_context_create(array('http' => array('method' => "POST", 'Host' => 'apiserver', 'header' => "Content-Type: text/xml", 'content' => $SMS)));


      $file = file_get_contents('http://directsmsapi.sluzba.cz/apifastest/receiver.asp', false, $context);
      $doc = simplexml_load_string($file);
      switch ($doc->status) {
        case '200':
          $output[] = array('result' => true);
          break;
        case '400':
          $output[] = array('result' => false, 'message' => 'Neplatné telefonní císlo', 'phone' => $phone_number);
        default:
          $output[] = array('result' => false, 'message' => 'Neošetrená chyba pri odesílání SMS', 'phone' => $phone_number);
          break;
      }
    }
    return $output;
  }
}

?>