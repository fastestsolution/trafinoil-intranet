<?php

namespace App\Controller\Component;

use Cake\Mailer\Email;
use Cake\Controller\Component;
use Cake\ORM\Entity;


class EmailComponent extends Component
{
  public $fromEmail = "info@smartee.cz";
  public $from = "Smartee";


  public function send($to, array $options = null){
    $email = new Email();
    $email->from($this->fromEmail, $this->from);
    $email->to($to)->emailFormat('html');

    if(isset($options["template"])){
      $email->template($options["template"]);
    }

    $email->send();
  }
}


