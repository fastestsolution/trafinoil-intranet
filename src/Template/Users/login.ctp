
<div id="login_box">
	<?php echo '<h1>'.appName.'</h1>';?>
	<?= $this->Flash->render('auth') ?>
    <?= $this->Form->create(null,['class'=>'login']) ?>
    <fieldset>
        <legend><?= __('Přihlašte se prosím') ?></legend>
        <?= $this->Form->input('username', ['label' => __('Váš email')]) ?>
        <?= $this->Form->input('password', ['label' => __('Váše heslo')]) ?>
    </fieldset>
    <?= $this->Form->button(__('Přihlásit se')); ?>
    <?= $this->Form->end() ?>
</div>
