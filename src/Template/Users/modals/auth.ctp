<div class="row">
	<div class="col two">
<?php

foreach(\Cake\Core\Configure::read("Auths") as $auth => $name){
	echo $this->Form->input("users.auth.".$auth, ["options" => [__('Zakázáno'), __("Pouze zobrazit"), __("Zobrazit a upravovat")], "label" => $name]);
}
?>
	</div>
</div>