<?php  
if (isset($items) && count($items)>0){

echo '<table class="table-formated autotable">';

		// záhlaví tabulky;
		$first = $items->first()->toArray();
		foreach($first as $key => $item){
			if($key != "color") {
				$th = $this->Autotable->th($key, $item);
			}
		}

		// vytvoří nadpis i pro sloupce které nelze načíst pomocí normálního dotazu. Ty se pak načtou pomocí postQuery ve funkce AutoTable::td()
		if(isset($params->postCols)){
			foreach($params->postCols as $k => $i){
				$th = $this->Autotable->th($k);
			}
		}


		echo '<thead>';
			echo '<tr>';
				echo $this->Autotable->tableth;
				if ((isset($params->actions) && !empty($params->actions)) || (isset($params->links) && !empty($params->links))){
					echo '<th class="poss"></th>';
				}
			echo '</tr>';
			
		echo '</thead>';
		
		echo '<tbody id="items">'; 
			
			foreach($items as $key => $item){
				if(isset($item->color)){
					$color = $shop_colors_code[$item->color];
					unset($item->color);
				}
				echo '<tr class="item-row '.(($key++ % 2) ? "even" : "odd")." ".((isset($color))? $color : null).'" click-id="'.$item['id'].'">';
					echo $this->Autotable->td($item->toArray(),$params);

					if (isset($params->actions) || isset($params->actions)){
						echo '<td class="poss">';

						if(@!empty($params->actions)) {
							foreach ($params->actions AS $key => $pos) {
								if ($key == 'trash') {
									$trash = $this->Html->link('<span class="fa fa-trash">', '/' . $key . '/' . $this->request->params['controller'] . '/' . $item['id'], ['escape' => false, 'class' => 'btn xs trash_button', 'onclick' => 'return false;']);
								} else{
									if($key[0] == "/") {
										$url = $key."/".$item["id"];
									}
									else{
										$url = ["action" => $key, $item["id"]];
									}
									echo $this->Html->link($pos, $url, ['class' => 'btn xs ' . $key, 'ajax' => true, 'escape' => false,]);
								}
							}
						}
						if (!empty($params->links)){
							foreach($params->links AS $key=>$pos){
								if($key[0] == "/") {
									$url = $key."/".$item["id"];
								}
								else{
									$url = ["action" => $key, $item["id"]];
								}
								echo $this->Html->link($pos, $url, ['class' => 'btn xs '.$key, 'escape' => false,]);
							}
						}

						if(isset($trash)){ echo $trash;}
						echo '</td>';
					}

				echo '</tr>';
			}
		
		echo '</tbody>';

	echo '</table>';
	echo '<ul class="pagination">';
	echo $this->Paginator->numbers(["modulus" => 40]);
	echo '</ul>';


} else {
	echo '<div class="noresult">'.__('Nenalezeny žádné záznamy, zkuste upravit filtraci').'</div>';
}

if(isset($params->menu)){
	echo $this->element("../AutoTable/elements/menu", ["menu" => $params->menu]);
}

?>