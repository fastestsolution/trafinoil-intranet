<div class="row">
<h2><?= __("Nastavení Drivera") ?></h2>
  <div class="two col">
    <?= $this->Form->input("driver_interval", ['label' => __('Interval synchronizace')]); ?>
  </div>
  <div class="two col">
    <?= $this->Form->input("driver_synctime", ['label' => __('Čas synchronizace')]); ?>
  </div>
  <div class="two col">
    <?= $this->Form->input("driver_id", ['type' => 'text', 'label' => __('Číslo certifikátu')]); ?>
  </div>
  <div class="twelve col">
    <?= $this->Form->input("driver_resetdb", ['type' => 'checkbox', 'label' => __('Vymazání lokální databáze')]); ?>
  </div>
  <div class="twelve col">
    <?= $this->Form->input("driver_fullsync", ['type' => 'checkbox', 'label' => __('Vnucení plné synchronizace')]); ?>
  </div>
  <div class="twelve col">
    <?= $this->Form->input("scanner_force", ['type' => 'checkbox', 'label' => __('Vnucení skenu čár. kódů')]); ?>
  </div>







</div>
