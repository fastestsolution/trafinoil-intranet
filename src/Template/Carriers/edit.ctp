<?php
if(isset($back)){
  echo $this->Html->link(__("Zpět"), $back, ['class' => 'btn']);
}
echo $this->Form->create($entity);
if(isset($back)){
  echo $this->Form->hidden("back", ['value' => $back]);
}
echo $this->element('modal',['load' => ['obecne'=>__('Dopravce'), 'driver' => __("Driver - nastavení")]]);
echo $this->Form->hidden("id");
?>
<?php
echo $this->Form->button(__("Uložit"), ["class" => "btn", "id" => "SaveModal"]);
echo $this->Form->end();

