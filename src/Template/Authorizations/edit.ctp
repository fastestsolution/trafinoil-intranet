<?php

if(isset($back)){
	echo $this->Html->link(__("Zpět"), $back, ['class' => 'btn']);
}
echo $this->Form->create($entity);
if(isset($back)){
	echo $this->Form->hidden("back", ['value' => $back]);
}
echo $this->element('modal',['load'=>['opravneni'=> __('Oprávnění'), 'members'=> __('Členové skupiny')]]);
?>
<?php
echo $this->Form->button(__("Uložit"), ["class" => "btn block", "id" => "SaveModal"]);
echo $this->Form->end();

