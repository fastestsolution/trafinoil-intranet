<div class="col six">
	<div class="top-actions">
	 <?= $this->Html->link(__("Přidat"), ["controller" => "collections", "action" => "add", $entity->id], ['class' => 'btn', 'escape' => false, 'title' => __("Vytvořit novou svozku")]) ?>
	</div>
	<div id="fst_history">
		<table class="table-formated">
			<tr>
				<th>id</th>
				<th><?= __("Jméno") ?></th>
				<th><?= __("login") ?></th>
				<th width="30px"></th>
			</tr>
			<?php

			if(count($users)){
				foreach($users as $user){
					echo '<tr>';
					echo '<td>'.$user["id"].'</td>';
					echo '<td>'.$user["name"].'</td>';
					echo '<td>'.$user["username"].'</td>';
					echo '<td>'.$this->Html->link('<span class="fa fa-remove"></span>', ["action" => "remove"], ["escape" => false, "class" => "btn xs edit"]).'</td>';
					echo '</tr>';
				}
			}
			?>
		</table>
	</div>
</div>