<?php
	/*
if(isset($carriers)){
echo '<table class="table-formated collection-table">';
	// nadpisy dopravců
	echo '<tr>';
		echo '<td class="w65">'.$this->Html->link(__("Replikovat"), ["action" => "replicate", $userId], ["class" => "btn", "data-modal-width" => "400px", "ajax" => true]).'</td>';
		foreach($carriers as $carrier){
		echo '<th class="tcenter" width="'.(100 / $carriers->count()).'%">'.$carrier->shortcut.'</th>';
		}
		echo '</tr>';

	// jednotlivé dny
	for($day = 1; $day <= 28; $day++){
	echo '<tr class="day'.(($day%7 != 0)? ($day%7) : 7).'" data-day="'.$day.'">';
		echo '<th class="date">'.$days["name"][($day%7)].'</th>';

		// vytváří td pro dopravce v daném dni
		foreach($carriers as $carrier){
		echo '<td><span class="wrap droppable"  data-carrier="'.$carrier->id.'">';
			// prochází vybrané svozy a pokud sedí datum a id dopravce, vypíše se
			foreach($collections as $collection){
			//pr($collection);
			if($day == $collection->day && $collection->carrier_id == $carrier->id){
			echo '<span class="collection draggable" data-id="'.$collection->id.'">';
						echo '<span class="route-name">'.@$collection->route->name.'</span>';
						echo '</span>';
			}
			}
			echo $this->Html->link('<i class="fa fa-plus-circle"></i>',
			$this->Url->build(["controller" => "CollectionTemplates", "action" => "add", $carrier->id, $day, $userId]),
			["ajax" => true, "data-modal-width" => "400px", "class" => "add_item", "escape" => false]);
			echo '</span></td>';
		}

		echo '</tr>';
	}
	echo '</table>';
}*/

?>

	<?php
	$date_th = 110;
	$td = 200;

	$table1 = "";
	$table2 = "";

	$w = sizeof($carriers->toArray()) * $td;

// pokud existují nějací dopravci a je zadané datum od. Povinně
	if(isset($carriers)){
		$table2 .= '<div id="right-c" class="table-formated collection-table" style="width:100%;">';
		// nadpisy dopravců
		$table2 .= '<div class="tr" style="width:'.$w.'px">';
		$table1 .= '<div class="tr"><div class="th" style="width: '.$date_th.'px">'.$this->Html->link(__("Replikovat"), ["action" => "replicate", $userId], ["class" => "btn xs", "data-modal-width" => "400px", "ajax" => true]).'</div></div>';
		foreach($carriers as $carrier){
			$table2 .= '<div class="th tcenter" style="width:'.$td.'px">'.((!empty($carrier->shortcut))? $carrier->shortcut : $carrier->c_name ).'</div>';
		}
		$table2 .= '</div>';

		// jednotlivé dny
		for($day = 1; $day <= 28; $day++){
			$table2 .= '<div class="tr day'.(($day%7 != 0)? ($day%7) : 7).'" data-day="'.$day.'" style="width:'.$w.'px">';
			$table1 .= '<div class="tr day'.(($day%7 != 0)? ($day%7) : 7).'"><div class="th date-th". style="width: '.$date_th.'px">'.$days["name"][($day%7)].'</div></div>';
			foreach($carriers as $carrier){
				$table2 .= '<div class="td" style="width: '.$td.'px"><span class="wrap droppable" data-carrier="'.$carrier->id.'">';
				foreach($collections as $collection){
					if($day == $collection->day && $collection->carrier_id == $carrier->id){
						$table2 .= '<span class="collection draggable" data-id="'.$collection->id.'">';
						$table2 .= '<span class="route-name">'.@$collection->route->name.'</span>';
						$table2 .= '</span>';
					}
				}
				$table2 .= $this->Html->link('<i class="fa fa-plus-circle"></i>',
					["action" => "add", "?"=> ["carrier_id" => $carrier->id, "day" => $day, "user_id" => $userId]],
					["ajax" => true, "data-modal-width" => "400px", "class" => "add_item", "escape" => false]);
				$table2 .= '</span></div>';
			}



			$table2 .= '</div>';
		}
		$table2 .= '</div>';
	}
	echo '<div class="table-formated" style="width:'.$date_th.'px; position:absolute; left: 5px; z-index:10;">';
	echo $table1;
	echo '</div>';
	echo $table2;
?>


<?php
echo $this->Html->script("CollectionTemplates/scripts.js");
?>