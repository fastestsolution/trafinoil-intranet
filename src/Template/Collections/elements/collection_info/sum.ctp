<div class="nine col">
	<table class="table-formated">
		<thead class="bordered">
		<tr>
			<th width="30%" rowspan="2"><?= __('VÝKUPY') ?></th>
			<th rowspan="2"><?= __('mj') ?></th>
			<th rowspan="2"><?= __('mn') ?></th>
			<th colspan="2" rowspan="1"><?= __('za hotové') ?></th>
			<th colspan="2" rowspan="1"><?= __('na dluh') ?></th>
			<th colspan="3" rowspan="1"><?= __('součty') ?></th>
			<th colspan="2" rowspan="1"><?= __('dluhy') ?></th>
		</tr>
		<tr>
			<th width="10%"><?= __('od plátců DPH') ?></th>
			<th width="10%"><?= __('od neplátců DPH') ?></th>
			<th width="10%"><?= __('od plátců DPH') ?></th>
			<th width="10%"><?= __('od neplátců DPH') ?></th>
			<th width="10%"><?= __('celkem vykoupeno s DPH') ?></th>
			<th width="10%"><?= __('celkem vykoupeno bez DPH') ?></th>
			<th width="10%"><?= __('prům. výkup') ?></th>
			<th width="10%"><?= __('vznik dluhu') ?></th>
			<th width="10%"><?= __('úhrada dluhu') ?></th>
		</tr>
		</thead>
		<tbody>
		<?php

			$total = [
				'cash' => [0,1],
				'debt' => [0,1],
				'price' => 0,
				'price_vat' => 0,
				'total_debt' => 0
			];

			foreach($trades[1] as $buy){
				$total["cash"][1] += $buy["cash"][1];
				$total["cash"][0] += $buy["cash"][0];
				$total["debt"][1] += $buy["debt"][1];
				$total["debt"][0] += $buy["debt"][0];
				$total["price_vat"] += $buy["price_vat"];
				$total["price"] += $buy["price"];
				$total["total_debt"] += ($buy["debt"][1] + $buy["debt"][0]);

				?>
				<tr>
					<td><?= $buy["name"] ?></td>
					<td><?= $buy["unit"] ?></td>
					<td><?= $buy["amount"] ?></td>
					<td><?= $this->Fastest->round($buy["cash"][1]); ?></td>
					<td><?= $this->Fastest->round($buy["cash"][0]); ?></td>
					<td><?= $buy["debt"][1]; ?></td>
					<td><?= $this->Fastest->round($buy["debt"][0]); ?></td>
					<td><?= $this->Fastest->round($buy["price_vat"]); ?></td>
					<td><?= $this->Fastest->round($buy["price"]); ?></td>
					<td><?= $this->Fastest->round(($buy["price"] / $buy["amount"]), 2) ?></td>
					<td><?= $this->Fastest->round(($buy["debt"][1] + $buy["debt"][0])); ?></td>
					<td></td>
				</tr>
				<?php
			}
		?>
		<tr>
			<th colspan="3">Celkem</th>
			<th><?= $this->Fastest->round($total["cash"][1]); ?></th>
			<th><?= $this->Fastest->round($total["cash"][0]); ?></th>
			<th><?= $this->Fastest->round($total["debt"][1]); ?></th>
			<th><?= $this->Fastest->round($total["debt"][0]); ?></th>
			<th><?= $this->Fastest->round($total["price_vat"]); ?></th>
			<th><?= $this->Fastest->round($total["price"]); ?></th>
			<th></th>
			<th><?= $this->Fastest->round($total["total_debt"]) ?></th>
			<th></th>
		</tr>
		</tbody>
	</table>
</div>
<div class="nine col">
	<table class="table-formated">
		<thead class="bordered">
		<tr>
			<th width="30%"><?= __('PRODEJ') ?></th>
			<th><?= __('mj') ?></th>
			<th><?= __('mn') ?></th>
			<th><?= __('počet balení') ?></th>
			<th><?= __('za hotové') ?></th>
			<th><?= __('na fakturu') ?></th>
			<th><?= __('prodej celkem') ?></th>
			<th><?= __('prům. cena') ?></th>
			<th><?= __('prům. cena bez dph') ?></th>
			<th><?= __('vznik dluhu') ?></th>
		</tr>
		</thead>
		<tbody>
		<?php

			$total_sell["amount"] = 0;
			$total_sell["price_hotove"] = 0;
			$total_sell["price_faktura"] = 0;
			$total_sell["price_total"] = 0;

			foreach($trades[2] as $sell){

				$total_sell["amount"] += $sell["amount"];
				$total_sell["price_hotove"] += $sell["price_hotove"];
				$total_sell["price_faktura"] += $sell["price_faktura"];
				$total_sell["price_total"] += ($sell["price_hotove"] + $sell["price_faktura"]);


				?>
				<tr>
					<td><?= $sell["name"]; ?></td>
					<td><?= $sell["unit"]; ?></td>
					<td><?= $sell["amount"]; ?></td>
					<td><?= $sell["amount"] / $sell["pack"]; ?></td>
					<td><?= $sell["price_hotove"]; ?></td>
					<td><?= $sell["price_faktura"]; ?></td>
					<td><?= $price = $sell["price_hotove"] + $sell["price_faktura"]; ?></td>
					<td><?= $this->Fastest->round($price / $sell["amount"], 2) ?></td>
					<td></td>
					<td><?= $sell["price_faktura"] ?></td>
				</tr>
			<?php
			}
			?>
		<tr>
			<th colspan="2">Celkem</th>
			<th><?= $this->Fastest->round($total_sell["amount"]); ?></th>
			<th></th>
			<th><?= $this->Fastest->round($total_sell["price_hotove"]); ?></th>
			<th><?= $this->Fastest->round($total_sell["price_faktura"]); ?></th>
			<th><?= $this->Fastest->round($total_sell["price_total"]); ?></th>
			<th></th>
			<th></th>
			<th><?= $sell["price_faktura"] ?></th>
		</tr>
		</tbody>
	</table>
</div>