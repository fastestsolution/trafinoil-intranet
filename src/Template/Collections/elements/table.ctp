<?php
	$date_th = 110;
	$td = 200;

	$table1 = "";
	$table2 = "";

	$w = sizeof($carriers->toArray()) * $td;

// pokud existují nějací dopravci a je zadané datum od. Povinně
	if(isset($carriers) && isset($date_from)){
		$table2 .= '<div id="right-c" class="table-formated collection-table" style="width:100%;">';
		// nadpisy dopravců
		$table2 .= '<div class="tr" style="width:'.$w.'px">';
		$table1 .= '<div class="tr"><div class="th" style="width: '.$date_th.'px"></div></div>';
		foreach($carriers as $carrier){
			$table2 .= '<div class="th tcenter" style="width:'.$td.'px">'.((!empty($carrier->shortcut))? $carrier->shortcut : $carrier->c_name ).'</div>';
		}
		$table2 .= '</div>';

		// jednotlivé dny
		for($day = 1; $day <= 28; $day++){
			$table2 .= '<div data-day="'.$date_from.'" style="width:'.$w.'px" class="tr day'.($date_from->i18nFormat("c")).(($date_from->isToday())? " today" : null).'">';
			$table1 .= '<div class="tr day'.$date_from->i18nFormat("c").'"><div class="th date-th". style="width: '.$date_th.'px">'.$date_from->i18nFormat("ccc d.M.Y").'</div></div>';
			foreach($carriers as $carrier){
				$table2 .= '<div class="td" style="width: '.$td.'px"><span class="wrap droppable" data-carrier="'.$carrier->id.'">';
				foreach($collections as $collection){
					if($collection["date_plan"] == $date_from && $collection["carrier_id"] == $carrier->id){
						$table2 .= $this->Element->collection($collection["route"]["name"], $collection["id"], $collection["status"], true, ["url" => ["action" => "edit", $collection["id"]]]);
					}
				}
				$table2 .= $this->Html->link('<i class="fa fa-plus-circle"></i>',
					["action" => "add", "?"=> ["carrier_id" => $carrier->id, "day" => $date_from->format("d.m.Y")]],
					["ajax" => true, "data-modal-width" => "400px", "class" => "add_item", "escape" => false]);
				$table2 .= '</span></div>';
			}



			$table2 .= '</div>';
			$date_from->modify("+1 day");
		}
		$table2 .= '</div>';
	}
	echo '<div class="table-formated" style="z-index: 200; width:'.$date_th.'px; position:absolute; left: 5px;">';
	echo $table1;
	echo '</div>';
	echo $table2;
?>
