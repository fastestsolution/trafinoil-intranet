<?php
if(count($trades)){

	$trade = [1 => "", 2 => ""];

	foreach($trades as $t){
			$trade[$t["DriverCollectionItemTrades"]["type"]] .= '<tr><td>'.$t["Commodities"]["name"].'</td><td>'.$t["DriverCollectionItemTrades"]["amount"].'</td><td>'.$t["DriverCollectionItemTrades"]["price"].'</td></tr>';
	}
}

?>
<div class="row">
	<div class="col four">
		<table class="table-formated">
			<thead>
				<tr>
					<th colspan="4"><?= __("Výkupy") ?></th>
				</tr>
				<tr>
					<th><?= __("komodita") ?></th>
					<th><?= __("množství") ?></th>
					<th><?= __("cena") ?></th>
				</tr>
			</thead>
			<?php echo $trade[1]; ?>
		</table>
	</div>
	<div class="col four">
		<table class="table-formated">
			<thead>
			<tr>
				<th colspan="4"><?= __("Prodeje") ?></th>
			</tr>
			<tr>
				<th><?= __("komodita") ?></th>
				<th><?= __("množství") ?></th>
				<th><?= __("cena") ?></th>
			</tr>
			</thead>
			<?php echo $trade[2]; ?>
		</table>
	</div>
</div>
