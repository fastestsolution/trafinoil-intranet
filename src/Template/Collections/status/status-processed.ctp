<div class="layout" id="collections-layout">
	<div class="top-actions"></div>
	<?= $this->Form->create($entity); ?>
	<div class="row route-info">
		<div class="four col">
			<?php
				if(isset($route)){
					$regions = $regions->toList();
					echo '<table class="table-formated horizontal secondary">';
					echo '<tr>';
					echo '<th width="30%">'.__("Region").'</th>';
					echo '<td>'.$regions[$route->region_id].'</td>';
					echo '</tr>';
					echo '<tr>';
					echo '<th>'.__("Trasa").'</th>';
					echo '<td>'.$route->name.'</td>';
					echo '</tr>';
					echo '<tr>';
					echo '<th>'.__("Stav").'</th>';
					echo '<td>'.__("Zpracovávaný").'</td>';
					echo '</tr>';
					echo '<tr>';
					echo '<th>'.__("Optimální vzdálenost").'</th>';
					echo '<td></td>';
					echo '</tr>';
					echo '<tr>';
					echo '<th>'.__("Ujeto").'</th>';
					echo '<td></td>';
					echo '</tr>';

					echo '</tr>';
					echo '</table>';
				}
			?>
		</div>
		<div class="four col">
			<?php
				if(isset($route)){
					$carriers->toList();
					echo '<table class="table-formated horizontal secondary">';
					echo '<tr>';
					echo '<th width="30%">'.__("Datum").'</th>';
					echo '<td>'.$this->Form->input("date_plan", ["type" => "text", "label" => false, "value" => $date, "class" => "date", "required" => true, "placeholder" => __("Vyberte datum")]).'</td>';
					echo '</tr>';
					echo '<tr>';
					echo '<th>'.__("Dopravce").'</th>';
					echo '<td>'.$this->Form->select("carrier_id", $carriers, ["default" => @((isset($carrier_id))? $carrier_id : $route->carrier_routes[0]->carrier_id)]).'</td>';
					echo '</tr>';
					echo '</table>';
				}
			?>
		</div>
		<div class="four col">
			<?php
				if(isset($route)){
					$carriers->toList();
					echo '<table class="table-formated horizontal secondary">';
					echo '<tr>';
					echo '<th width="30%">'.__("Start").'</th>';
					echo '<td></td>';
					echo '</tr>';
					echo '<tr>';
					echo '<th>'.__("Cíl").'</th>';
					echo '<td></td>';
					echo '<tr>';
					echo '<th>'.__("Sklad výdej").'</th>';
					echo '<td></td>';
					echo '</tr>';
					echo '<tr>';
					echo '<th>'.__("Sklad příjem").'</th>';
					echo '<td></td>';
					echo '</tr>';
					echo '</table>';
				}
			?>
		</div>
	</div>

	<div class="row">
		<div class="twelve col">
			<div id="filtration">
				<?php
					echo $this->element("color_filter", ["filter_hide" => 1]);
				?>
		</div>
			<?php echo $this->element("../Collections/shops"); ?>
	</div>
</div>
<?= $this->Form->button(__("Uložit"), ["class" => "btn", "value" => 1, "name" => "status"]) ?>
<?= $this->Form->button(__("Uložit řidiči ke stažení"), ["class" => "btn", "value" => 2, "name" => "status"]) ?>
<?= $this->Form->end(); ?>
</div>
<script type="text/javascript">
	date_picker();
	dp.setTitle('text');
</script>
