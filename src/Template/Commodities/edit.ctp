<?php

if(isset($back)){
  echo $this->Html->link(__("Zpět"), $back, ['class' => 'btn']);
}
echo $this->Form->create($entity);
if(isset($back)){
  echo $this->Form->hidden("back", ['value' => $back]);
}
echo $this->element('modal',['load'=>['obecne'=>__('Komodita'), 'ceny' => __("Ceny")]]);
?>
<?php
echo $this->Form->button(__("Uložit"), ["class" => "btn block", "id" => "SaveModal"]);
echo $this->Form->end();

