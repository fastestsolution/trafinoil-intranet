<div class="row">
  <h2><?= __("Jednotka") ?></h2>
	<div class="one col">
		<?= $this->Form->input("Commodities.unit", ['label' => __('Hlavní jednotka')]); ?>
	</div>
	<div class="one col">
		<?= $this->Form->input("Commodities.unit2", ['label' => __('Sekundární jednotka')]); ?>
	</div>
	<div class="one col">
		<?= $this->Form->input("Commodities.unit2_to_unit", ['label' => __('vztah mezi jednot.')]); ?>
	</div>
</div>
<div class="row">
	<h2><?= __("Nastavení obalu") ?></h2>
	<div class="one col">
		<?= $this->Form->input("Commodities.in_pack", ['label' => __('Počet v Balení')]); ?>
	</div>
	<div class="one col">
		<?= $this->Form->input("Commodities.pack_type", ['label' => __('Obal vazba'), "options" => $packs]); ?>
	</div>
	<div class="one col">
		<br />
		<br />
		<?= $this->Form->input("Commodities.divisible", ['label' => __('Dělitelné balení')]); ?>
	</div>
	<div class="one col">
		<br />
		<br />
		<?= $this->Form->input("Commodities.pack", ['type' => 'checkbox', 'label' => __('Komodita je obal')]); ?>
	</div>
</div>
<div class="row">
	<h2><?= __("Obchod") ?></h2>
	<div class="one col">
		<?= $this->Form->input("Commodities.bill_type", ['label' => __('Účet')]); ?>
	</div>
	<div class="one col">
		<?= $this->Form->input("Commodities.sale", ['label' => __('Výkup/prodej'), "options" => [__("Výkup"), __("Prodej")]]); ?>
	</div>
	<div class="one col">
		<?= $this->Form->input("Commodities.co_type", ["options" => [], 'label' => __('Typ čistého oleje')]); ?>
	</div>
	<div class="one col">
		<br />
		<br />
		<?= $this->Form->input("Commodities.so", ["type" => "checkbox", 'label' => __('Špinavý olej')]); ?>
	</div>
</div>
<div class="row">
	<h2><?= __("Příznaky") ?></h2>
	<div class="one col">
		<?= $this->Form->input("Commodities.not_in_stats", ['type' => 'checkbox', 'label' => __('Vyřadit ze statistik')]); ?>
	</div>
	<div class="one col">
		<?= $this->Form->input("Commodities.primar", ['type' => 'checkbox', 'label' => __('Primární')]); ?>
	</div>
	<div class="one col">
		<?= $this->Form->input("Commodities.driver", ['type' => 'checkbox', 'label' => __('Driver')]); ?>
	</div>
</div>
