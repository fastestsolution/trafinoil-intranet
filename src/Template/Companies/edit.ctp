<?php
if(isset($back)){
  echo $this->Html->link(__("Zpět"), $back, ['class' => 'btn']);
}
echo $this->Form->create($entity);
if(isset($back)){
  echo $this->Form->hidden("back", ['value' => $back]);
}
echo $this->Form->hidden("id");
?>
<div class="row">
  <br /><br />
  <div class="three col">
    <?= $this->Form->input("ic", ['label' => __('IČO'), 'class' => 'find_ares']); ?>
  </div>
  <div class="six col">
    <?= $this->Form->input("name", ['label' => __('Název firmy'), 'class' => 'ares_firma']); ?>
  </div>
  <div class="three col">
    <?= $this->Form->input("dic", ['label' => __('DIČ'), 'class' => 'ares_dic']); ?>
  </div>
  <div class="three col">
    <?= $this->Form->input("street", ['label' => __('Ulice'), 'class' => 'ares_ulice']); ?>
  </div>
  <div class="three col">
    <?= $this->Form->input("town", ['label' => __('Město'), 'class' => 'ares_mesto']); ?>
  </div>
  <div class="three col">
    <?= $this->Form->input("psc", ['label' => __('PSČ'), 'class' => 'ares_psc']); ?>
  </div>
  <div class="three col">
    <?= $this->Form->input("country_id", ['label' => __('Stát'), 'class' => 'ares_stat', 'options' => Cake\Core\Configure::read("select_config.countries")]); ?>
  </div>
</div>

<div class="row">
  <h2><?= __("Další informace") ?></h2>
  <div class="three col">
    <?= $this->Form->input("contact_name", ['label' => __('Kontaktní osoba')]); ?>
  </div>
  <div class="three col">
    <?= $this->Form->input("phone", ['label' => __('Telefon')]); ?>
  </div>
  <div class="three col">
    <?= $this->Form->input("mobile", ['label' => __('Mobil')]); ?>
  </div>
  <div class="three col">
    <?= $this->Form->input("email", ['label' => __('Email')]); ?>
  </div>
  <div class="three col">
    <?= $this->Form->input("www", ['label' => __('WWW')]); ?>
  </div>
  <div class="two col">
    <?= $this->Form->input("dph", ['label' => __('Plátce DPH'),  'class' => 'ares_dph', 'options' => \Cake\Core\Configure::read("select_config.bool")]); ?>
  </div>
  <div class="seven col">
    <?= $this->Form->input("note", ['label' => __('Poznámka')]); ?>
  </div>
</div>

<?php
echo $this->Form->button(__("Uložit"), ["class" => "btn", "id" => "SaveModal"]);
echo $this->Form->end();

