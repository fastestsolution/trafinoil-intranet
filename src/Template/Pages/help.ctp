<div class="layout">
	<h1><?= __("Nápověda"); ?></h1>

	<h2><?= __("Provozovny"); ?></h2>
	<h3><?= __("Barvy provozoven"); ?></h3>
	<ul>
		<li><?= __("zelené objednávky, hnědé, asia jdou automaticky do svozu") ?></li>
		<li><?= __("bílá obchodní zastávka, nové provozovny nebo možnost obchodu kde to zkouší") ?></li>
		<li><?= __("žlutá nepravidelná, sezooní nebo s nízkou frekvencí") ?></li>
		<li><?= __("zelená pravidelná") ?></li>
		<li><?= __("hnědá nová smlouva, neví jak bude dobrá") ?></li>
		<li><?= __("červená měli smlouvu, ale ukončili spolupráci, nevaří, nebo něco takového") ?></li>
		<li><?= __("růžová konkurence, už to hodněkrát zkoušeli") ?></li>
		<li><?= __("šedá duplicita, omyl") ?></li>
		<li><?= __("bledě modrá(asia) - asia bistra, vietnamci, potenciálně vysoká produkce") ?></li>
		<li><?= __("tmavě modrá ostrý konkurenční boj (ceny ve ztrátách)") ?></li>
	</ul>

	<h2><?= __("Svozy"); ?></h2>
	<h3><?= __("Barvy svozů"); ?></h3>
	<ul>
		<li><?= __("bílí navrhovaný (šablonizovaný)") ?></li>
		<li><?= __("jemně žlutá (zpracovávaný a ať se zobrazí počet zpracovaných provozoven ku celkovému počtu)") ?></li>
		<li><?= __("žlutý plánovaný (řidič ještě neotevřel)") ?></li>
		<li><?= __("přidat ikonku že už byl stažený") ?></li>
		<li><?= __("oranžová otevřený (řidič si otevřel zobrazit procento hotových (všech které už řešil, i vynechané))") ?></li>
		<li><?= __("zelená ukončený (zavřeno řidičem)") ?></li>
		<li><?= __("tyrkysová potvrzeno dispečerem") ?></li>
		<li><?= __("modrá uzamčený") ?></li>
</div>