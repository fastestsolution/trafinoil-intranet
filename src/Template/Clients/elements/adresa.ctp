
<fieldset>
	<legend><?php echo __("Přidat / editovat adresu")?></legend>
	<div class="sll"> 
		<?php echo $this->Form->input("adr.ulice".$type, ['label' => __("Ulice").':','tabindex'=>30,'class'=>'add_adr add_ins autocomplete_ulice','data-type'=>'ulice']); ?>
		<?php echo $this->Form->input("adr.psc".$type, ['label' => __("PSČ").':','tabindex'=>32,'class'=>'add_adr add_ins','data-type'=>'psc']); ?>
		<?php echo $this->Form->input("adr.firma".$type, ['label' => __("Firma").':','tabindex'=>35,'class'=>'add_adr add_ins','data-type'=>'firma']); ?>
		<?php echo $this->Form->button(__('Přidat'), ['class' => 'btn fleft AddAddress']);?>
		<?php echo $this->Form->button(__('Smazat'), ['class' => 'btn fleft DeleteAddress']);?>
	</div>
	<div class="slr"> 
		<?php echo $this->Form->input("adr.mesto".$type, ['label' => __("Město").':','tabindex'=>31,'class'=>'add_adr add_ins','data-type'=>'mesto']); ?>
		<?php echo $this->Form->input("adr.stat".$type, ['label' => __("Stát").':','tabindex'=>34,'class'=>'add_adr add_ins','data-type'=>'stat']); ?>
		<?php echo $this->Form->hidden("adr.lat".$type, ['id'=>'adr-lat','class'=>'add_adr add_ins','data-type'=>'lat']); ?>
		<?php echo $this->Form->hidden("adr.lng".$type, ['id'=>'adr-lng','class'=>'add_adr add_ins','data-type'=>'lng']); ?>
		<?php echo $this->Form->hidden("adr.type".$type, ['id'=>'adr-type','class'=>'add_adr add_ins','data-type'=>'type','value'=>(isset($type)?$type:'')]); ?>
	</div>
</fieldset>