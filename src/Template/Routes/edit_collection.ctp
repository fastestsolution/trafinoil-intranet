<div class="layout" id="collections-layout">
	<?= $this->Form->create($entity); ?>
	<div class="row route-info">
		<div class="four col">
			<?php
				if(isset($route)){
						$regions = $regions->toList();
						echo '<table>';
							echo '<tr>';
								echo '<th>'.__("Region").'</th>';
								echo '<td>'.$regions[$route->region_id].'</td>';
							echo '</tr>';
							echo '<tr>';
								echo '<th>'.__("Trasa").'</th>';
								echo '<td>'.$route->name.'</td>';
							echo '</tr>';
							echo '<tr>';
								echo '<th>'.__("Stav").'</th>';
								echo '<td>'.$collection_status[$route->status].'</td>';
							echo '</tr>';
							echo '<tr>';
								echo '<th>'.__("Optimální vzdálenost").'</th>';
								echo '<td></td>';
							echo '</tr>';
							echo '<tr>';
								echo '<th>'.__("Ujeto").'</th>';
								echo '<td></td>';
							echo '</tr>';

							echo '</tr>';
						echo '</table>';
				}
			?>
		</div>
		<div class="four col">
			<?php
			if(isset($route)){
				$carriers->toList();
				echo '<table>';
				echo '<tr>';
				echo '<th>'.__("Datum").'</th>';
				echo '<td>'.$this->Form->input("date_plan", ["type" => "text", "label" => false, "required" => true]).'</td>';
				echo '</tr>';
				echo '<tr>';
				echo '<th>'.__("Dopravce").'</th>';
				echo '<td>'.$this->Form->select("carrier_id", $carriers, ["default" => @$route->carrier_routes[0]->carrier_id]).'</td>';
				echo '</tr>';
				echo '</table>';
			}
			?>
		</div>
		<div class="four col">
			<?php
			if(isset($route)){
				$carriers->toList();
				echo '<table>';
				echo '<tr>';
				echo '<th>'.__("Start").'</th>';
				echo '<td></td>';
				echo '</tr>';
				echo '<tr>';
				echo '<th>'.__("Cíl").'</th>';
				echo '<td></td>';
				echo '<tr>';
				echo '<th>'.__("Sklad výdej").'</th>';
				echo '<td></td>';
				echo '</tr>';
				echo '<tr>';
				echo '<th>'.__("Sklad příjem").'</th>';
				echo '<td></td>';
				echo '</tr>';
				echo '</table>';
			}
			?>
		</div>
	</div>

	<div class="row">
		<div class="twelve col">
			<table class="table-formated">
				<tr>
					<th>#</th>
					<th><?= __("Provozovna") ?></th>
					<th class="tcenter"><?= __("Obj.") ?></th>
					<th width="60" class="tcenter"><?= __("Zastav") ?></th>
					<th><?= __("poznámka svozu / provozovny") ?></th>
					<th><?= __("Marketingová poznámka") ?></th>
				</tr>
			<?php
				if(count($route->route_shops) > 0){
					foreach($route->route_shops as $k => $rs){
						//pr($rs);
						echo '<tr class="'.$shop_colors_code[$rs->shop->color].'">';
							echo '<td>'.($k+1).'</td>';
							echo '<td>'.$rs->shop->name.'</td>';
							echo '<td class="tcenter"></td>';
							echo '<td class="reset tcenter">'.$this->Form->checkbox("collections.add.$rs->id", ["type" => "checkbox", "label" => false, 'div' => false]).'</td>';
							echo '<td>'.
                                                                $this->Form->input("collection_shop.shop_note", ["value" => $rs->shop->note, "label" => false]).
								$this->Form->input("collection_shop.shop_note", ["value" => $rs->shop->note, "label" => false]).
                                                             '</td>';
							echo '<td>'.$this->Form->input("collection_shop.marketing_note", ["value" => "", "label" => false]).'</td>';
						echo '</tr>';
					}
				}
			?>
			</table>
		</div>
	</div>
	<?= $this->Form->button(__("Uložit")) ?>
	<?= $this->Form->end(); ?>
</div>

<script type="text/javascript">
	var dp = new Picker.Date($('date-plan'), {
		timePicker: false,
		positionOffset: {x: 5, y: 0},
		pickerClass: 'datepicker_vista',
		useFadeInOut: !Browser.ie,
		draggable : false,
		format : "%e.%m.%Y",
		days_abbr : ["Po", "Út", "St", "Čt", "Pá", "So", "Ne"],
		blockKeydown : true,
		startDay : 0
	});

	dp.setTitle('text');

</script>
