<?php

if(isset($back)){
  echo $this->Html->link(__("Zpět"), $back, ['class' => 'btn']);
}
echo $this->Form->create($entity, ['id'=>'base_route_form']);
if(isset($back)){
  echo $this->Form->hidden("back", ['value' => $back]);
}
echo $this->element('modal',[
    'load'=>['obecne'=>__('Dopravce'), 
    'provozovny' => __("Provozovny v trase"), 
    'map' => __("Mapa")]]);

echo $this->Form->hidden("id",['id'=>'MapId']);
?>
<?php
echo $this->Form->button(__("Uložit"), ["class" => "btn block", "id" => "SaveModal"]);
echo $this->Form->end();

