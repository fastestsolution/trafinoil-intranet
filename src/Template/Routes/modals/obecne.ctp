<div class="row">
  <h2><?= __("Trasa") ?></h2>

	<div class="three col">
		<?= $this->Form->input("name", ['label' => __('Název trasy')]); ?>
	</div>
	<div class="two col">
    <?= $this->Form->input("region_id", ['label' => __('Region'), 'options' => $regions]); ?>
  </div>
  <div class="two col">
	  <br /><br />
    <?= $this->Form->input("status", ['label' => __('Aktvní'), 'class' => '']); ?>
  </div>
	<div class="clear"></div>

	<h2><?= __("Dopravce") ?></h2>
	<div class="five col">
		<?= $this->Form->input("carriers_ids", ['options' => $carriers,  'multiple' => true, 'id' => 'carrier-routes', 'default' => @$selected_carriers]); ?>
	</div>
	<script type="text/javascript">
		var multiselect = new Multiselect($("carrier-routes"));
	</script>
</div>
