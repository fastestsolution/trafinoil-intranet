
<?php
echo $this->Html->script("Routes/auto-save");

echo '<div class="top-actions">';
echo $this->Html->link(__("Přidat provozy"), ["controller" => 'routes', 'action' => 'addShopToRoute', $entity->id], ['data-id'=>$entity->id,'class' => 'btn add_shop', 'onclick'=>'return autoSave(this)','escape' => false, 'title' => __("Přidat provoz do trasy")]);
echo $this->Html->link(__("Tisk"), ["controller" => 'routes', 'action' => 'addShopToRoute', $entity->id], ['class' => 'btn', 'escape' => false, 'title' => __("Přidat provoz do trasy")]);
echo $this->Html->link(__("Výpočet trasy"), ["controller" => 'routes', 'action' => 'addShopToRoute', $entity->id], ['class' => 'btn', 'escape' => false, 'title' => __("Přidat provoz do trasy")]);
//echo $this->Html->link(__("Nová svozka"), ["controller" => "collections", "action" => "add", $entity->id], ['class' => 'btn', 'escape' => false, 'title' => __("Vytvořit novou svozku")]);
echo '</div>';

echo $this->Form->hidden("shops_sort", ["id" => "shops-sort"]);

if(isset($shopsInRoute)){
	echo '<table class="table-formated" id="drag">';
		echo '<thead>';
			echo '<tr>';
			echo '<th>'.__("Pořadí").'</th>';
			echo '<th>'.__("Provozovna").'</th>';
			echo '<th>'.__("Firma").'</th>';
			echo '<th>'.__("DPH").'</th>';
			echo '<th>'.__("Telefon").'</th>';
			echo '<th>'.__("Použitý olej").'</th>';
			echo '<th>'.__("Použitý tuk").'</th>';


			echo '<th>'.__("Fritovací olej<br /> DK OIL - 10 L").'</th>';
			echo '<th>'.__("Fritovací olej<br /> APETOL - 10 L").'</th>';
			echo '<th>'.__("Fritovací olej<br /> APETOL - 50 L").'</th>';
			echo '<th>'.__("Fritovací olej<br /> Řepka/Palma - 10 L").'</th>';

			echo '<th>'.__("barely").'</th>';
			echo '<th>'.__("posl.svoz").'</th>';
			echo '<th>'.__("poznámka").'</th>';
			echo '<th>'.__("Fritovací olej<br /> Řepka/Palma - 10 L").'</th>';
			echo '</tr>';
		echo '</thead>';
		echo '<tbody>';

	foreach($shopsInRoute as $k => $v){

		echo '<tr class="'.$shop_colors_code[$v["Shops"]["color"]].'" data-id="'.$v->shop_id.'">';
			echo '<td class="ord">'.($k+1).'</td>';
			echo '<td>'.$this->Html->link($v["Shops"]["name"], ["controller" => "shops", "action" => "edit", $v->shop_id, "back" => $this->Fastest->backUrl(["tab" => "provozovny"])]).'</td>';
			echo '<td>'.((isset($v["Companies"]))? $this->Html->link($v["Companies"]["name"], ["controller" => "companies", "action" => "edit", $v["Companies"]["id"], 'back' => $this->Fastest->backUrl(["tab" => "provozovny"])]) : null).'</td>';
			echo '<td>'.$bool[$v["Companies"]["dph"]].'</td>';
			echo '<td>'.$v["Shops"]["bcontact_phone"].' '.$v["Shops"]["bcontact_mobile"].'</td>';
			echo '<td></td>';
			echo '<td></td>';
			echo '<td></td>';
			echo '<td></td>';
			echo '<td></td>';
			echo '<td></td>';
			echo '<td></td>';
			echo '<td></td>';
			echo '<td></td>';
			echo '<td></td>';
		echo '</tr>';
	}
	echo '</tbody>';
	echo '</table>';
}

echo $this->Html->script("Routes/shops-order");
?>