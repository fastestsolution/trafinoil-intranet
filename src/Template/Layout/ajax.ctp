<!DOCTYPE html>
<html>
<head>
  <?= $this->Html->charset() ?>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>
    <?= $title ?>
  </title>
  <?php  ?>
  <script type="text/javascript">less = { env: 'development' };</script>
  <?php  ?>
  <?= $this->Html->meta('icon') ?>
  
  <link rel="stylesheet/less" type="text/css" href="<?php echo $cssUri;?>"  media="screen" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <?php /*<?= $this->Html->css("default.css", ["rel" => "stylesheet/less"]); ?>*/?>

  <?= $this->Html->script("less"); ?>
  <?= $this->Html->script($jsUri); ?>

  <?= $this->fetch('meta') ?>
  <?= $this->fetch('css') ?>
  <?= $this->fetch('script') ?>
  <?php
		if (isset($scripts)){if (is_array($scripts)){foreach ($scripts as $link){echo $this->Html->script($link);}} else {echo $this->Html->script($scripts);}}
		if (isset($styles)){if (is_array($styles)){foreach ($styles as $style){echo $this->Html->css($style);} } else {echo $this->Html->css($styles);}} 
	?>
</head>
<body class="ajax <?= strtolower($this->request->controller."-".$this->request->action) ?>">
<div class="header"><h1><?= $title ?></h1></div>
<div class="container clearfix">
	<div id="modal_in">
		<?= $this->fetch('content') ?>
	</div>
</div>
<?php
echo $this->Html->script("fst_system/modal_window");
?>
</body>
</html>