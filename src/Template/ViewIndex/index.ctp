<?php  
echo '<div class="layout">';
echo '<div class="top-actions">';
if (isset($params_viewIndex['top_action'])){
	foreach($params_viewIndex['top_action'] AS $key=>$item){
		echo $this->Html->link($item, ['action' => $key], ['class' => 'btn', 'ajax' => true]);
	}
}
if(isset($back)){
	echo $this->Html->link(__("Zpět"), $back, ["class" => "btn"]);
}
echo '</div>';
echo $this->element('../ViewIndex/elements/filtr');

echo '<div id="fst_history">';
//pr($params_viewIndex);


if(isset($items) && $items->count() > 0){
	echo $this->element('../ViewIndex/items');
} else {
	echo '<div class="noresult">'.__('Nenalezeny žádné záznamy').'</div>';
}

echo '</div>';
?>