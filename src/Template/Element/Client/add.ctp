<?php
echo $this->Form->create($client);
echo '<h2>'.__("Základní údaje").'</h2>';
echo $this->Form->hidden("user_id", ['value' => $this->request->Session()->read("Auth.User.id")]);
echo $this->Form->input("name", ['label' => __("Název")]);
echo $this->Form->input("ic", ['label' => __("IČ")]);

echo '<h2>'.__("Adresa").'</h2>';
echo $this->Form->input("adress", ['label' => __("Adresa")]);
echo $this->Form->input("town", ['label' => __("Město")]);
echo $this->Form->input("psc", ['label' => __("PSČ")]);
echo $this->Form->input("country", ['label' => __("Země")]);

echo $this->Html->link(__("Přidat kontakt"), ["controller" => "clients", "action" => "add_contact"], ["id" => 'add-contact']);
echo '<div id="client-contacts"></div>';

echo $this->Form->button(__('Uložit'), ['class' => 'btn']);
echo $this->Form->end();

echo $this->Html->script("Clients/add");