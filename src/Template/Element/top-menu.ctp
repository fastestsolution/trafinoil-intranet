<div class="top-menu">
  <ul>
    <li><?= $this->Html->link(__("Provozovny"), ["controller" => "shops"]) ?></li>
    <li><?= $this->Html->link(__("Firmy"), ["controller" => "companies"]) ?></li>
    <li><?= $this->Html->link(__("Dopravci"), ["controller" => "carriers"]) ?></li>
	  <li><?= $this->Html->link(__("Trasy"), ["controller" => "routes"]) ?></li>
	  <li><?= $this->Html->link(__("Svozy"), ["controller" => "collections"]) ?></li>
	  <li><?= $this->Html->link(__("Šablony"), ["controller" => "collectionTemplates"]) ?></li>
	  <li><?= $this->Html->link(__("Komodity"), ["controller" => "Commodities"]) ?></li>
	  <li><?= $this->Html->link(__("Uživatelé"), ["controller" => "users"]) ?></li>
	  <li><?= $this->Html->link(__("Skupiny oprávnění"), ["controller" => "authorizations"]) ?></li>
  </ul>
  <ul class="user">
    <li class="username"><?= $authUser["username"] ?></li>
    <li><?= $this->Html->link('<span class="fa fa-info"></span>', ["controller" => "pages", "action" => "help"], ["escape" => false]) ?></li>
    <li><?= $this->Html->link('<span class="fa fa-power-off"></span>', ["controller" => "users", "action" => "logout"], ["escape" => false]) ?></li>
  </ul>
</div>