<?php
// tenhle komunikuje s fst_history
if(!isset($filter_hide)) {
	foreach ($shop_colors_code as $k => $v) {
		echo $this->Form->input("Shops.color.$k", ["type" => "checkbox", "class" => "fst_h", "data-fparams" => "Shops__color__$k|opt", "label" => false, 'templates' => [
			'inputContainer' => '<div class="input color ' . $v . '">{{content}}</div>'
		],]);
	}
}
// tenhle pouze skrývá řádky v tabulce dle filtru
else{
	foreach ($shop_colors_code as $k => $v) {
		echo $this->Form->input("Shops.color.$k", ["type" => "checkbox", "class" => "color_hds filter_hide", "value" => $v, "checked" => true, "label" => false, 'templates' => [
			'inputContainer' => '<div class="input color ' . $v . '">{{content}}</div>'
		],]);
	}

	echo $this->Html->script("filter_hide");
}

	?>


