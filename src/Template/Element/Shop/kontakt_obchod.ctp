<table class="table-formated">
	<tr>
		<th colspan="2"><?= __("Obchod (Majitel/provozní)") ?></th>
	</tr>
	<tr>
		<td width="80px"><strong><?= __("Jméno:") ?></strong></td>
		<td><?= $this->Form->input("Shops.bcontact_name", ['label' => false]); ?></td>
	</tr>
	<tr>
		<td><strong><?= __("Telefon:") ?></strong></td>
		<td><?= $this->Form->input("Shops.bcontact_phone", ['label' => false]); ?></td>
	</tr>
	<tr>
		<td><strong><?= __("mobil:") ?></strong></td>
		<td><?= $this->Form->input("Shops.bcontact_mobile", ['label' => false]); ?></td>
	</tr>
</table>