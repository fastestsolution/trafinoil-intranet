<?php

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;
use Cake\View\Helper\UrlHelper;

class FastestHelper extends Helper
{

  public $helpers = ["Url"];
  public $tableth = "";
  private $cname = [];

	public function price($price,$mena_suf = array()){
		
		//pr($mena_suf);
		$symbol_before = '';
		$kurz = null;
		$count = 1;
		$decimal = 0;
		$symbol_after = ',-';
		extract($mena_suf);
		
    	$white = array(" " => "");
		if ($kurz!=null)
			$price = ($price/strtr($kurz,',','.'))*$count;
		if ($price != null)
			return $symbol_before.number_format(strtr($price,$white), $decimal, '.', ' ').$symbol_after;
		else
			return $symbol_before.number_format(strtr(0.00,$white), $decimal, '.', ' ').$symbol_after;	
	}

	public function backUrl($options = null){
		$pass = implode("/", $this->request->pass);
		return $this->Url->build(['controller' => $this->request->controller, 'action' => $this->request->action, $pass, '?' => $options ]);
	}

	public function round($a, $d = null){
		return round($a, $d);
	}

}