<?php

namespace App\View\Helper;

use Cake\Core\Configure;
use Cake\View\Helper;
use Cake\View\View;
use Cake\ORM\TableRegistry;

class ElementHelper extends Helper
{

	public $helpers = ['Html', 'Url', 'Form'];

	public function collection($name, $id, $status, $draggable = true, $options = null)
	{
		if ($draggable == true) {
			$d =  '<span class="draggable" data-id ="'.$id.'">';
		}

		$d .= $this->Html->link('<span class="route-name">' . $name . '</span>', $options["url"], ["escape" => false, "class" => "collection ".Configure::read("select_config.collection_status.class.".$status)]);
		if ($draggable) {
			$d .= '</span>';
		}

		return $d;
	}

	public function collectionShopTr($rs, $collection_id, $route_id, $added = false){
		echo '<tr class="'.Configure::read("select_config.shop_colors_code.".$rs["Shops"]["color"]).'" data-id="'.$rs["Shops"]["id"].'">';
		echo '<td></td>';
		echo '<td>'.$rs["Shops"]["name"].'</td>';
		echo '<td class="tcenter"></td>';
		echo '<td>'
			.$this->Form->input("call_note".$rs["Shops"]["id"], ["value" => ((isset($rs["Shops"]["calls"][0]))? $rs["Shops"]["calls"][0]["note"] : null), "label" => false, "placeholder" => (($added == false)? __("Poznámka hovoru") : null), "class" => "call_note", "disabled" => (($added == true)? true : false)]).
			$this->Form->input("shop_note".$rs["Shops"]["id"], ["value" => $rs["Shops"]["note"], "label" => false, "class" => "shop_note", "placeholder" => (($added == false)? __("Poznámka provozovny") : null), "disabled" => (($added == true)? true : false)]).
			'</td>';
		echo '<td>'.$this->Form->input("marketing_note".$rs["Shops"]["id"], ["value" => "", "label" => false, "class" => "marketing_note", "placeholder" => (($added == false)? __("Marketingova poznámka") : null), "disabled" => (($added == true)? true : false)]).'</td>';
		echo '<td class="poss">'.
			$this->Html->link('<span class="fa fa-phone"></span>', ["controller" => "calls", "action" => "call", $rs["Shops"]["id"], "collection_id" => $collection_id, "route_id" => $route_id], [ "data-modal-width" => "1500px", "ajax" => true, "escape" => false, "style" => (($added == true)? "display:none" : "")]).
			$this->Html->link('<span class="fa fa-remove"></span>', ["controller" => "collections", "action" => "shopToCollection", "shop_id" => $rs["Shops"]["id"], "collection_id" => $collection_id, "route_id" => $route_id, "in" => 0], ["escape" => false]).
			$this->Html->link('<span class="fa fa-check"></span>', ["controller" => "collections", "action" => "shopToCollection", "shop_id" => $rs["Shops"]["id"], "collection_id" => $collection_id, "route_id" => $route_id, "in" => 1], ["escape" => false, "style" => (($added == true)? "display:none" : "")]).
			'</td>';
		echo '</tr>';
	}
}