<?php
namespace App\View\Cell;

use Cake\View\Cell;

class ClientCell extends Cell
{

  public function display($group_id)
  {
    $this->loadModel("Clients");
    $this->loadModel("GroupUsers");

    $gus = $this->GroupUsers->find()->where(["group_id" => $group_id])->combine("id", "user_id")->toArray();
    $clients = $this->Clients->find()->where(["user_id IN" => $gus])->combine("id", "name");

    $client = $this->Clients->newEntity();

    $this->set("client", $client);
    $this->set("clients", $clients);
  }

}