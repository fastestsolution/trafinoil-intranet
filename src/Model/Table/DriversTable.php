<?php

namespace App\Model\Table;

use App\Model\Entity\User;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use Cake\Core\Exception\Exception;

use Cake\Network\Session;

class DriversTable extends Table
{

  public $exportTable = "sez_dopr";
  public $importTable = "carriers";
  public $exportPrimaryKey = "dop_id";

  public $exportSetting = [
    'dop_id' => 'id',
    'dop_typ' => 'type',
    'dop_stav' => 'status',
    'dop_jmeno' => 'c_name',
    'dop_zkratka' => 'shortcut',
    'dop_nazev' => 'name',
    'dop_ulice' => 'street',
    'dop_mesto' => 'town',
    'dop_psc' => 'psc',
    'dop_region_id' => 'region_id',
    'dop_ic' => 'ic',
    'dop_dic' => 'dic',
    //'' => 'phone',
    'dop_mobil' => 'mobile',
    'dop_email' => 'email',
    'dop_web' => 'www',
    //'' => 'bank_account',
    //'' => 'spz',
    'dop_pozn' => 'note',
    'dop_driver_id' => 'driver_id',
    'dop_driver_interval' => 'driver_interval',
    'dop_driver_sync_time' => 'driver_sync_time',
    'dop_driver_resetdb' => 'driver_resetdb',
    'dop_driver_fullsync' => 'driver_fullsync',
    'dop_sklad_aid' => 'stock_id',
    'dop_scanner_force' => 'scanner_force',
    'dop_naklad1' => 'cargo1',
    'dop_naklad2' => 'cargo2',
    'dop_naklad3' => 'cargo3',
    'dop_driver_resetpremises' => 'driver_resetpremises',
    'dop_redirect' => 'redirect',
    'dop_obchodnik_id' => 'dealer_id',
    //'' => 'kos'
  ];

}