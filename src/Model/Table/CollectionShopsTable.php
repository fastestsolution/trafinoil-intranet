<?php
namespace App\Model\Table;

use App\Model\Entity\User;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

use Cake\Network\Session;

class CollectionShopsTable extends Table
{

  //public $exportTable = "sez_firmy";
  //public $importTable = "companies";

//  public $exportSetting = [
//    'fir_id' => 'id', // primary key musí být první
//    //'fir_ipvzn' => null,
//    //'fir_dcvzn' => null,
//    //'fir_jmvzn' => null,
//    //'fir_ipzme' => null,
//    //'fir_dczme' => null,
//    //'fir_jmzme' => null,
//    //'fir_typ' => null,
//    'fir_stav' => 'kos',
//    'fir_nazev' => 'name',
//    'fir_jmeno' => 'contact_name',
//    'fir_ulice' => 'street',
//    'fir_mesto' => 'town',
//    'fir_stat' => 'country_id',
//    //'fir_psc' => null,
//    'fir_ico' => 'ic',
//    'fir_dic' => 'dic',
//    'fir_tel' => 'phone',
//    'fir_mobil' => 'mobile',
//    'fir_email' => 'email',
//    'fir_web' => 'www',
//    'fir_pozn' => 'note',
//    'fir_dph' => 'dph',
//    'fir_lastcheck' => 'lastcheck_dph'
//  ];


	// ukládá shop do svozu + uloží novou poznámku k provozovně pokud není null
	public function set($shop_id, $collection_id, $route_id = null, $in = null, $shop_note = null, $marketing_note = null, $call_note = null){
		$cst = TableRegistry::get("CollectionShops");

		if(isset($collection_id)) {
			// kontrola jestli už tam záznam není
			$cs = $cst->find()->where(["shop_id" => $shop_id, "collection_id" => $collection_id])->select(["id"])->first();
			if (!empty($cs)) {
				if($cst->query()->delete()->where(["id" => $cs["id"]])->execute()) {
					return "edit";
				}
				return false;
			}
		}

		$cs = $cst->newEntity();
		$cs->collection_id = $collection_id;
		$cs->shop_id = $shop_id;
		$cs->route_id = $route_id;
		$cs->in_collection = $in;
		$cs->shop_note = $shop_note;
		$cs->marketing_note = $marketing_note;
		$cs->call_note = $call_note;

		if($cst->save($cs)) {
			if (isset($shop_note)) {
				$st = TableRegistry::get("Shops");
				$shop = $st->get($shop_id);
				$shop->note = $shop_note;
				$st->save($shop);
				unset($st);
			}

			return "add";
		}else{
			return false;
		}

	}


  public function initialize(array $config)
  {
	  $this->belongsToMany("Collections");
	  $this->belongsTo("Shops");
    //$this->addBehavior('Synchronize');
  }

}
