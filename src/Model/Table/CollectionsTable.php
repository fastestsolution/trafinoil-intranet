<?php
namespace App\Model\Table;

use App\Model\Entity\User;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

use Cake\Network\Session;

class CollectionsTable extends Table
{

  //public $exportTable = "sez_firmy";
  //public $importTable = "companies";

//  public $exportSetting = [
//    'fir_id' => 'id', // primary key musí být první
//    //'fir_ipvzn' => null,
//    //'fir_dcvzn' => null,
//    //'fir_jmvzn' => null,
//    //'fir_ipzme' => null,
//    //'fir_dczme' => null,
//    //'fir_jmzme' => null,
//    //'fir_typ' => null,
//    'fir_stav' => 'kos',
//    'fir_nazev' => 'name',
//    'fir_jmeno' => 'contact_name',
//    'fir_ulice' => 'street',
//    'fir_mesto' => 'town',
//    'fir_stat' => 'country_id',
//    //'fir_psc' => null,
//    'fir_ico' => 'ic',
//    'fir_dic' => 'dic',
//    'fir_tel' => 'phone',
//    'fir_mobil' => 'mobile',
//    'fir_email' => 'email',
//    'fir_web' => 'www',
//    'fir_pozn' => 'note',
//    'fir_dph' => 'dph',
//    'fir_lastcheck' => 'lastcheck_dph'
//  ];


	public function initialize(array $config)
	{
		parent::initialize($config);
		$this->hasMany("CollectionShops");
		$this->belongsTo("Routes");
		$this->belongsTo("Carriers");
	}

	public function findCollections($query, $opt = null){

		if(isset($opt["date_from"])){
			$query->where(["date_plan >=" => $opt["date_from"]]);
		}
		if(isset($opt["date_to"])){
			$query->where(["date_plan <=" => $opt["date_to"]]);
		}

		return $query->contain("Routes");
	}


	public function findAllPlanedCollections($query, $carrier_id){
        $query->where(["Collections.status" => 2]);
        if(isset($carrier_id)){
            $query->where(["carrier_id" => $carrier_id]);
        }
        return $query->hydrate(false)->contain(array("CollectionShops", "Routes"));
    }


	public function validationCreateCollection(Validator $validator)
  {
    $validator
      ->requirePresence('date_plan', 'create', __("Vyplňte datum svozu"))
      ->notEmpty('date_plan',  __("Vyplňte datum svozu"))
	    ->requirePresence('carrier_id', 'create', __("Vyberte dopravce"))
	    ->notEmpty('carrier_id',  __("Vyberte dopravce"));

    return $validator;
  }

	public function validationEditCollection(Validator $validator)
	{
		$validator
			->notEmpty('date_plan',  __("Vyplňte datum svozu"))
			->notEmpty('carrier_id',  __("Vyberte dopravce"));

		return $validator;
	}

	public function getShopsInCollection($collection_id, $shop_id = null){
		if(!isset($collection_id)){
			throw new Exception(__("ID svozu není zadáno"));
		}

		$rst = TableRegistry::get("CollectionShops");
		$query = $rst
			->find()
			->where(["collection_id" => $collection_id, "in_collection" => 1])
			->contain("Shops")
			->select(["Shops.id", "Shops.name", "Shops.color", "Shops.lat", "Shops.lng"])
			->order("ordering");

		if(isset($shop_id)){
			$query->where(["CollectionShop.shop_id" => $shop_id]);
		}

		return $query;
	}

	public function reorder($collection_id, $orderIds){
		$cst = TableRegistry::get("CollectionShops");
		$shops = $cst->find()->where(["collection_id" => $collection_id, "in_collection" => 1])->order("ordering")->hydrate(false)->select("id")->toArray();

		foreach($orderIds as $k => $id){
			$cst->query()->update()->set(["ordering" => $k])->where(["shop_id" => $id, "collection_id" => $collection_id])->limit(1)->execute();
		}
	}
}
