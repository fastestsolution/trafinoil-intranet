<?php
namespace App\Model\Table;

use App\Model\Entity\User;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

use Cake\Network\Session;

class ShopsTable extends Table
{

	/*** NASTAVENÍ EXPORTU ****/

  public $exportTable = "sez_prov";
  public $importTable = "shops";
	public $exportPrimaryKey = "pro_id";

	// definuje sloupce mezi databázemi
  public $exportSetting = [
    'pro_id' => 'id',
    'pro_idx' => 'company_id',
    //'pro_ipvzn' => '',
    //'pro_dcvzn' => '',
    //'pro_jmvzn' => '',
    //'pro_ipzme' => '',
    //'pro_dczme' => '',
    //'pro_jmzme' => '',
    //'pro_typ' => '',
    'pro_stav' => 'status',
    'pro_nazev' => 'name',
    'pro_jmeno' => 'bcontact_name',
    'pro_ulice' => 'street',
    'pro_mesto' => 'town',
    'pro_mesto_kod' => 'town_code',
    'pro_cobce_kod' => 'town_part',
    'pro_psc' => 'psc',
    //'pro_okresc' => '',
    'pro_okrest' => 'region',
    'pro_tel' => 'bcontact_phone',
    //'pro_fax' => '',
    'pro_mobil' => 'bcontact_mobile',
    'pro_email' => 'bcontact_email',
    'pro_vydej_jmeno' => 'contact_name',
    'pro_vydej_tel' => 'contact_mobile',
    'pro_web' => 'www',
    'pro_pozn' => 'note',
    'pro_platba' => 'payment_type',
    'pro_doprav' => 'carrier_id',
    'pro_otv_doba' => 'opening_hours',
    //'pro_barcel' => '',
    'pro_lat' => 'lat',
    'pro_lng' => 'lng',
    'pro_barva' => 'color',
    'pro_puvod' => 'source',
    'pro_stat' => 'country_id',
    'pro_dat_smlouva' => 'pact_date',
    //'pro_puvid' => '',
    //'pro_objem_mes' => '',
    //'pro_objem_zast' => '',
    'pro_dodavatel_id' => 'supplier_id',
    'pro_obchodnik_id' => 'user_id',
    'pro_iscc' => 'iscc',
    //'pro_zdroj_id' => '',
    //'pro_nahrada' => '',
    'pro_fransiza_id' => 'franchise_id',
    //'pro_objem_mes_co' => '',
    //'pro_objem_zast_co' => ''
  ];

	// definuje pole, kde je potřeba převézt hodnotu sloupce dle databáze
	public $exportTransValues = ['pro_stat', 'pro_fransiza_id'];

	// funkce mění hodnoty při importu pro pole pro_stat
	public function pro_stat($value){
		$trans = ["CZ" => 1, "SK" => 2, "PL" => 3];
		if(isset($trans[$value])) {
			return $trans[$value];
		}
		return null;
	}

	// funkce mění hodnoty při importu pro pole pro_fransiza_id
	public function pro_fransiza_id($value){
		if($value == 0){
			return null;
		}
		return $value;
	}


	/***** KONEC NASTAVENÍ EXPORTU ****/


  public function initialize(array $config)
  {
    $this->hasMany("ShopPacks");
    $this->hasMany("ShopPotentials");
    $this->belongsTo("Companies");
	  $this->hasMany("Calls");
  }



}
