<?php
	namespace App\Model\Table;

	use App\Model\Entity\User;
	use Cake\ORM\Entity;
	use Cake\ORM\Query;
	use Cake\ORM\RulesChecker;
	use Cake\ORM\Table;
	use Cake\Validation\Validator;
	use Cake\ORM\TableRegistry;
	use Cake\I18n\Time;

	use Cake\Network\Session;

	class DriverCollectionsTable extends Table
	{

		public function initialize(array $config)
		{
			$this->hasMany("DriverCollectionItems");
			$this->belongsTo("Collections");
		}


		/*Vrací query pro získání všech potřebných dat detailu svozu*/
		public function findResults($q)
		{
			$q
				->select(['DriverCollections.id', 'DriverCollections.date_start', 'DriverCollections.date_complete'])
				->contain([

					"DriverCollectionItems" => [
						"queryBuilder" => function($q) {
							return $q->order("poradi")->select(
								["DriverCollectionItems.id",
									"DriverCollectionItems.driver_collection_id",
									"DriverCollectionItems.shop_id",
									"DriverCollectionItems.status",
									"DriverCollectionItems.note",
									"DriverCollectionItems.time",
									"DriverCollectionItems.poradi",
									'DriverCollectionItems.vykup_faktura',
									'DriverCollectionItems.dph_payer',
									'CollectionShops.ordering',
									'CollectionShops.call_note',
								])->join(["CollectionShops" => [
									"table" => "collection_shops",
									"type" => "left",
									"conditions" => ["DriverCollectionItems.shop_id = CollectionShops.shop_id and DriverCollectionItems.collection_id = CollectionShops.collection_id"]]]);
						}
					],
					"DriverCollectionItems.DriverCollectionItemTrades.Commodities" =>
						function ($q) {
							return $q->select(
								[ "Commodities.id",
									"Commodities.unit",
									"Commodities.name",
									"Commodities.in_pack",
									'dph_d' => 'dph * 100',
									"price_no_vat" => "price / (DriverCollectionItemTrades.dph * 100 + 100) * 100",
									'DriverCollectionItemTrades.id',
									'DriverCollectionItemTrades.driver_collection_item_id',
									'DriverCollectionItemTrades.commodity_id',
									'DriverCollectionItemTrades.amount',
									'DriverCollectionItemTrades.price',
									'DriverCollectionItemTrades.type',
									'DriverCollectionItemTrades.dph']
							);
						},
					"DriverCollectionItems.Shops" => function ($q) {
						return $q->select(["id", "name"]);
					},
				]);

			return $q;
		}
	}
