<?php
namespace App\Model\Table;

use App\Model\Entity\User;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

use Cake\Network\Session;

class CompaniesTable extends Table
{

  public $exportTable = "sez_firmy";
  public $importTable = "companies";

  public $exportSetting = [
    'fir_id' => 'id', // primary key musí být první
    //'fir_ipvzn' => null,
    //'fir_dcvzn' => null,
    //'fir_jmvzn' => null,
    //'fir_ipzme' => null,
    //'fir_dczme' => null,
    //'fir_jmzme' => null,
    //'fir_typ' => null,
    'fir_stav' => 'status',
    'fir_nazev' => 'name',
    'fir_jmeno' => 'contact_name',
    'fir_ulice' => 'street',
    'fir_mesto' => 'town',
    'fir_stat' => 'country_id',
    //'fir_psc' => null,
    'fir_ico' => 'ic',
    'fir_dic' => 'dic',
    'fir_tel' => 'phone',
    'fir_mobil' => 'mobile',
    'fir_email' => 'email',
    'fir_web' => 'www',
    'fir_pozn' => 'note',
    'fir_dph' => 'dph',
    'fir_lastcheck' => 'lastcheck_dph'
  ];



  public function initialize(array $config)
  {
    $this->addBehavior('Synchronize');
  }

  public function validationDefault(Validator $validator)
  {
    $validator
      //->requirePresence('name', 'create',   __("Název společnosi je povinný"))
      //->notEmpty('name')
      ->add('ic', [ 'unique' => ['rule' => 'validateUnique', 'provider' => 'table', 'message' => __("Firma s tímto IČO již v databázi je")]]);

    return $validator;
  }
}
