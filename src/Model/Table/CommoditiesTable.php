<?php
namespace App\Model\Table;

use App\Model\Entity\User;
use Cake\I18n\Date;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

use Cake\Network\Session;

class CommoditiesTable extends Table
{

	/*** NASTAVENÍ EXPORTU ****/

	public $exportTable = "komodita";
	public $importTable = "commodities";

	public $dependency = [
		'komodita_stat' => 'country_commodities'
	];

	// definuje sloupce mezi databázemi
	public $exportSetting = [
						'id' => 'id',
            'nazev' => 'name',
            'zkratka' => 'shortcut',
            'typ' => 'sale',
            'jednotka' => 'unit',
            'druh_uhr' => 'druh_uhr',
            'zruseno' => 'kos',
            'primarni' => 'primar',
            'driver' => 'driver',
            'ucet' => 'bill_type',
            'poradi' => 'ord',
            'baleni' => 'in_pack',
            'obal' => 'pack',
            'delitelne' => 'divisible',
            'obal_ref' => 'pack_type',
            'sek_jednotka' => 'unit2',
            'sek_jednotka_cnv' => 'unit2_to_unit',
            'no_stats' => 'not_in_stats',
            'so' => 'so',
            'co_druh' => 'co_type'
	];

	// definuje pole, kde je potřeba převézt hodnotu sloupce dle databáze
	public $exportTransValues = ["zruseno", "typ", "sek_jednotka_cnv"];

	public function zruseno($val){
		if(isset($val)){
			return 1;
		}
		else{
			return 0;
		}
	}

	public function sek_jednotka_cnv($val){
		if($val == 0){
			return null;
		}

		return $val;
	}

	public function typ($val){
		if($val == 1){
			return 0;
		}
		else{
			return 1;
		}
	}


	/***** KONEC NASTAVENÍ EXPORTU ****/


	public function initialize(array $config)
	{
		$this->hasMany("CountryCommodities");
	}

    /**Vrátí komoditu/ty podle země
     * @param null $commodity_id
     * @param null $country_id
     * @return $this|array
     */
	public function getCountryCommodity($commodity_id = null, $country_id = null){
		$cct = TableRegistry::get("CountryCommodities");

        $res = $cct->find()->contain(array("Commodities"));
        if(isset($country_id)){
          $res->where(["country_id" => $country_id]);
        }
        if(isset($commodity_id)){
          $res->where(["commodity_id" => $commodity_id]);
        }

		return $res;
	}

    /**V country commodity je ve sloupce dph typ dph a v tabulce dph se ještě řeší kdy bylo validní
     * @param int $dphIntFromCountryCommodityTable hodnota ve sloupci typ v tabulce country_comodities
     * @param Carbon|null $date datum platnosti dph
     * @param string $select umožňuje vrátít konkrétní hodnotu nějakého pole v tabulce
     * @return QueryObject|string
     */
	public function getDph($dphIntFromCountryCommodityTable, Date $date = null, $select = ""){
	    $Dphs = TableRegistry::get("Dphs");
        $q = $Dphs->find()->where(["type" => $dphIntFromCountryCommodityTable])->limit(1);

        if(isset($date)){
            $q->andWhere(["valid_from <=" => $date, "OR" => ["valid_to IS NULL", "valid_to >=" => $date->format("Y-m-d")]]);
        }
        else{
            $q->andWhere(["valid_to IS NULL", "valid" => 1]);
        }

        if($select != ""){
            return $q->select($select)->first()->{$select};
        }

        return $q;
    }

}
