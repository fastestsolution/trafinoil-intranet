<?php
namespace App\Model\Table;

use App\Model\Entity\User;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use Cake\Core\Exception\Exception;

use Cake\Network\Session;

class RoutesTable extends Table
{

  public $exportTable = "trasy";
  public $importTable = "routes";
	public $exportPrimaryKey = "trasa_id";

  public $exportSetting = [
	  'trasa_id' => 'id',
    'nazev' => 'name',
    'aktivni' => 'status',
    'region_id' => 'region_id',
    'delka' => 'distance',
    'smazano' => 'kos',
  ];





  public function initialize(array $config)
  {
	  $this->hasMany("CarrierRoutes");
	  $this->hasMany("RouteShops");
    //$this->addBehavior('Synchronize');
  }

  public function validationDefault(Validator $validator)
  {
    $validator
      ->requirePresence('name', 'create',   __("Název trasy je povinný"))
      ->notEmpty('name');
      //->add('ic', [ 'unique' => ['rule' => 'validateUnique', 'provider' => 'table', 'message' => __("Firma s tímto IČO již v databázi je")]]);

    return $validator;
  }

	public function findCarriersWithIds($query, $opt){

		$cr = TableRegistry::get("CarrierRoutes");
		$query = $cr->find()->where(["CarrierRoutes.route_id" => $opt["id"]])->Contain("Carriers")->select("Carriers.name")->hydrate(false);

		$count = $query->count();

		if($count){
			foreach($query as $k => $c){
				echo $c["Carriers"]["name"].(($k < $count-1)? ", " : null);
			}
		}
	}

	// vrací dopravce kteří jsou připojeni k trase
	public function getCarriers($route_id){
		$cr = TableRegistry::get("CarrierRoutes");

		$data = $cr->find()->where(["CarrierRoutes.route_id" => $route_id])->map(function($row){
			return $row->carrier_id;
		});

		return $data;
	}

	// ukládá spojení dopravce s trasou a kontroluje zda se nějaké nemá odstranit.
	public function saveAssocCarriers($route_id, $carrier_ids = null){

		if(empty($carrier_ids)){
			$carrier_ids = [];
		}

		$cr = TableRegistry::get("CarrierRoutes");
		$crd = $cr->find("list", [
			'keyField' => 'id',
			'valueField' => 'carrier_id'
		])->where(["route_id" => $route_id])->toArray();




		// kontrola co se má odstranit a co nechat
		if(count($crd) > 0) {
			foreach ($crd as $cri) {
				if (!in_array($cri, $carrier_ids)) {
					$cr->query()->delete()->where(["route_id" => $route_id, "carrier_id" => $cri])->execute();
				} else {
					unset($carrier_ids[array_search($cri, $carrier_ids)]);
				}
			}
		}

		// co zbyde uložit do db
		if(!empty($carrier_ids)){
			foreach($carrier_ids as $id){
				$cr->query()->insert(['route_id', 'carrier_id'])->values(['route_id' => $route_id, 'carrier_id' => $id])->execute();
			}
		}
	}

	// vrací jeden konkrétní shop v trase nebo všechny
	public function getShopsInRoute($route_id, $shop_id = null){
		if(!isset($route_id)){
			throw new Exception(__("ID trasy není zadáno"));
		}

		$rst = TableRegistry::get("RouteShops");
		$query = $rst->find()->where(["route_id" => $route_id])->order("ord")->contain("Shops")->select(["Shops.id", "Shops.name", "Shops.color", "Shops.lat", "Shops.lng"]);

		if(isset($shop_id)){
			$query->where(["RouteShops.shop_id" => $shop_id]);
		}

		return $query;
	}

	public function countShops($route_id){
		$shop_c = array();
		$rst = TableRegistry::get("RouteShops");
		$shops = $rst->find()->where(["RouteShops.route_id" => $route_id])->contain("Shops");
		$shops = $shops->select(["c" => "count(*)", "Shops.color"])->group("Shops.color")->order("Shops.color")->hydrate(false)->toArray();
		if (count($shops)) {
			foreach ($shops as $shop) {
				$shop_c[$shop["shop"]["color"]] = $shop["c"];
			}
		}
		return $shop_c;
	}

}
