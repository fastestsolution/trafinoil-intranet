<?php
namespace App\Model\Table;

use App\Model\Entity\User;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

use Cake\Network\Session;

class CarrierRoutesTable extends Table
{

  public $exportTable = "dop_trasy";
  public $importTable = "carrier_routes";
	public $exportPrimaryKey = null;

  public $exportSetting = [
    'trasa_id' => 'route_id', // primary key musí být první
    'dopravce_id' => 'carrier_id',
    'zadano' => 'created',
  ];





  public function initialize(array $config)
  {
	  $this->BelongsTo("Carriers");
	  $this->belongsToMany("Routes");
    //$this->addBehavior('Synchronize');
  }

}
