<?php
namespace App\Model\Table;

use App\Model\Entity\User;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use Cake\Core\Exception\Exception;

use Cake\Network\Session;

class RegionsTable extends Table
{

  public function initialize(array $config)
  {
	  $this->BelongsTo("Countries");
    //$this->addBehavior('Synchronize');
  }
}
