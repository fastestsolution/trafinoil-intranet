<?php
namespace App\Model\Entity\Driver;

use Cake\ORM\Entity;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Symfony\Component\Console\Exception\LogicException;

class Collection
{

    /**Store for data
     * @var array
     */
    protected $data = array();

    private static $className = "Cartage";


    public function __construct($collectionData)
    {
        if(!isset($collectionData["id"], $collectionData["route_id"], $collectionData["date_plan"], $collectionData["carrier_id"], $collectionData["route"])){
            Throw new LogicException("chybí data pro vytvoření collection");
        }

        $this->create($collectionData);
    }

    /**Vytvoří strukturu zastávek pro drivera
     * @param $collectionId
     */
    public function create($data)
    {
        $this->data["__className"] = self::$className;
        $this->data["id"] = $data["id"];
        $this->data["deleted"] = false;
        $this->data["date"] = $data["date_plan"]->format("Y-m-d");
        $this->data["route_id"] = $data["route"]["id"];
        $this->data["route"] = $data["route"]["name"];
        $this->data["area"] = "Bruntál";
    }

    public function getData(){
        return $this->data;
    }
}
