<?php
namespace App\Model\Entity\Driver;

use Cake\ORM\Entity;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Symfony\Component\Console\Exception\LogicException;

class Company
{

    /**Store for data
     * @var array
     */
    protected $data = array();

    private static $className = "Customer";


    public function __construct($shopData)
    {
        $this->data = array("__className" => self::$className);
        $this->create($shopData);
    }

    public function create($data){

       $this->data["ico"] = $data["ic"];
       $this->data["dic"] = $data["dic"];
       $this->data["street"] = $data["street"];
       $this->data["town"] = $data["town"];
       $this->data["zip"] = ($data["psc"])? $data["psc"] : "";
       $this->data["pays_vat"] = $data["dph"];
       //@TODO
       $this->data["is_invoiced"] = 0;
       $this->data["name"] = $data["name"];
       $this->data["id"] = $data["id"];
    }

    public function getData(){
        return $this->data;
    }
}
