<?php
namespace App\Model\Entity\Driver;

use Cake\ORM\Entity;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Symfony\Component\Console\Exception\LogicException;

/**Obaly produktů? Je potřeba zjistit jak to má správně fungovat, doku nic moc neříká
 * Class Containers
 * @package App\Model\Entity\Driver
 */
class Containers
{

    /**Store for data
     * @var array
     */
    protected $data = array();

    private static $className = "__Container";


    public function __construct()
    {
        $this->data = array("__className" => self::$className, "amount" => 1, "product_id" => 2);
    }

    public function getData(){
        return $this->data;
    }
}
