<?php
namespace App\Model\Entity\Driver;

use Cake\ORM\Entity;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;

use App\Model\Entity\Driver\Deal;
use App\Model\Entity\Driver\Container;

/**Entita pro drivera zastávky svozu
 * Class Stopovers
 * @package App\Model\Entity\Driver
 */
class Stopovers
{
    /**Store for data
     * @var array
     */
    protected $data = array();

    /**Název třídy pro drivera
     * @var string
     */
    private static $className = "StopOver";

    /**Vytvoří strukturu zastávek pro drivera
     * @param int $collectionId
     */
    public function create($collectionId)
    {
        $query = TableRegistry::get("CollectionShops");
        $steps = $query->find()->where(["collection_id" => $collectionId]);

        if ($steps->count()) {
            foreach ($steps as $sk => $step) {

                $this->data[$sk] = array(
                    "__className" => self::$className,
                    "premise_id" => $step["shop_id"],
                    "supplier_id" => 1,
                    "order" => 11,
                    "barrels" => 0,
                    "comment" => "",
                    "commitment" => 0,
                    "claim" => 0,
                    "marketingOffer" => "Marketing offer"
                );

                $Deal = new Deal();
                $this->data[$sk]["deals"] = array($Deal->getData());

                $Containers = new Containers();
                $this->data[$sk]["containers"] = array($Containers->getData());
            }
        }
    }

    /**Vrátí vytvořená data
     * @return array
     */
    public function getData(){
        return $this->data;
    }
}
