<?php
namespace App\Model\Entity\Driver;

use Cake\ORM\Entity;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Symfony\Component\Console\Exception\LogicException;

class Deal
{

    /**Store for data
     * @var array
     */
    protected $data = array();

    private static $className = "Deal";


    public function __construct()
    {
        $this->data = array("__className" => self::$className);
    }

    public function getData(){
        return (array)$this->data;
    }
}
