<?php
namespace App\Model\Entity\Driver;

use Cake\ORM\Entity;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Symfony\Component\Console\Exception\LogicException;

class Shop
{

    /**Store for data
     * @var array
     */
    protected $data = array();

    private static $className = "Premise";


    public function __construct($shopData)
    {
        $this->data = array("__className" => self::$className);
        $this->create($shopData);
    }

    public function create($data){

        $this->data["id"] = $data["id"];
        $this->data["name"] = $data["name"];
        $this->data["street"] = $data["street"];
        $this->data["city"] = $data["town"];
        $this->data["zip"] = $data["psc"];
        $this->data["phone"] = $data["contact_phone"];
        $this->data["mobile"] = $data["contact_mobile"];
        $this->data["note"] = $data["note"];
        $this->data["customer_id"] = $data["company_id"];
        $this->data["color"] = "";
        $this->data["article_prices"] = "asdfasdfasdf";
        $this->data["currency"] = "CZK";
        $this->data["supplier_id"] = $data["supplier_id"];
        $this->data["contact_sales_name"] = "";
        $this->data["contact_sales_phone"] = "";
        $this->data["contact_sales_mobile"] = "";
        $this->data["contact_sales_email"] = "";
        $this->data["contact_supply_name"] = "";
        $this->data["contact_supply_phone"] = "";
        $this->data["contact_supply_mobile"] = "";
        $this->data["contact_supply_email"] = "";
        $this->data["opening_hours_comment"] = "";
        $this->data["do_amount"] = "";
        $this->data["do_comment"] = "";
        $this->data["frequency"] = "";
        $this->data["do_price"] = "";
        $this->data["co_amount"] = "";
        $this->data["co_frequency"] = "";
        $this->data["co_kind_id"] = "";
        $this->data["co_price"] = "";
        $this->data["co_comment"] = "";
        $this->data["fryer_size"] = "";
        $this->data["competition_id"] = "";
        $this->data["competition_comment"] = "";
        $this->data["other_goods"] = "";
        $this->data["contract_date"] = "";
        $this->data["franchise_id"] = "";
        $this->data["country"] = $data["country_id"];
        $this->data["city_code"] = $data["town_code"];
        $this->data["post_code"] = $data["psc"];
        $this->data["web_site"] = $data["www"];
        $this->data["payment"] = $data["payment_type"];
        $this->data["iscc"] = "";
        $this->data["locationLatitude"] = $data["lat"];
        $this->data["locationLongitude"] = $data["lng"];
        $this->data["expenseCashVersion"] = "";
        $this->data["expenseCashNumber"] = "";
        $this->data["expenseInvoiceVersion"] = "";
        $this->data["expenseInvoiceNumber"] = "";
        $this->data["expenseCashDebtVersion"] = "";
        $this->data["expenseCashDebtNumber"] = "";

        $this->createOpeningHours($data["opening_hours"]);
    }

    protected function createOpeningHours($jsonData = null){
        if($jsonData) {
            $data = unserialize($jsonData);
        }

        $this->data["opening_hours_1"] = $data[1]["c"];
        $this->data["opening_hours_2"] = $data[2]["c"];
        $this->data["opening_hours_3"] = $data[3]["c"];
        $this->data["opening_hours_4"] = $data[4]["c"];
        $this->data["opening_hours_5"] = $data[5]["c"];
        $this->data["opening_hours_6"] = $data[6]["c"];
        $this->data["opening_hours_7"] = $data[7]["c"];
    }

    public function getData(){
        return $this->data;
    }
}
