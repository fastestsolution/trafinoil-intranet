<?php
define('appName','Dispečer 1.0');
$select_config = [
  'freq' => [
    0 => __('1x měsíčně'),
    1 => __('2x měsícně'),
    2 => __('3x měsícně'),
    3 => __('4x měsícně'),
    4 => __('5x měsícně'),
    5 => __('co 2 měsíce'),
    6 => __('co 3 měsíce'),
    7 => __('co 4 měsíce'),
    8 => __('co 5 měsíce'),
    9 => __('co půl roku'),
  ],

  'numbers' => [
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16
  ],

  'shop_colors' => [
    0 =>  __('Bezbarvá'),
    1 => __('Zelená'),
    2 => __('Žlutá'),
    3 => __('Červená'),
    4 => __('Oranžová'),
    5 => __('Modrá'),
    6 => __('Růžová'),
    7 => __('Asia'),
	  8 => __('Hnědá'),
    99 => __('Šedá')
  ],

	'shop_colors_plural' => [
		0 =>  __('Bezbarvé'),
		1 => __('Zelené'),
		2 => __('Žluté'),
		3 => __('Červené'),
		4 => __('Oranžové'),
		5 => __('Modré'),
		6 => __('Růžové'),
		7 => __('Asia'),
		8 => __('Hnědé'),
		99 => __('Šedé')
	],

	'shop_colors_code' => [
		0 => 'transparent',
		1 => 'green',
		2 => 'yellow',
		3 => 'red',
		4 => 'orange',
		5 => 'blue',
		6 => 'ping',
		7 => 'asia',
		8 => 'brown',
		99 => 'grey'
	],

  'shop_source' => [
    0 => __('neurčeno'),
    1 => __('vlastní'),
    2 => __('externí')
  ],

  'payment_type' => [
    0 => 'neurčeno',
    1 => 'fakturou',
    2 => 'vlastní'
  ],

  'countries' => [
    '0' => __("Nevybráno"),
    '1' => __("Česká republika"),
    '2' => __("Slovensko"),
    '3' => __("Polsko")
  ],

  'bool' => [
    0 => __('Ne'),
    1 => __('Ano')
  ],

	'co_type' => [
		0 => __("-"),
		1 => __("Řepka"),
		2 => __("Slunečnice"),
		3 => __("Palma"),
		4 => __("Mix")
	],

	'bill_type' => [
		-1 => __("Nezařazeno"),
		1 => __("Jedlé rostlinné oleje a tuky"),
		2 => __("použitý olej a tuk"),
		3 => __("Čistící prostředky"),
		4 => __("Ostatní zboží"),
		5 => __("Obaly"),
		6 => __("Jedlé oleje - cisternové")
	],

	"days" => [
		'name' => [
			0 => __("Neděle"),
			1 => __("Pondělí"),
			2 => __("Úterý"),
			3 => __("Středa"),
			4 => __("Čtvrtek"),
			5 => __("Pátek"),
			6 => __("Sobota"),
			7 => __("Neděle"),
		],
		'shortuct' => [
			0 => __("Ne"),
			1 => __("Po"),
			2 => __("Út"),
			3 => __("St"),
			4 => __("Čt"),
			5 => __("Pá"),
			6 => __("So"),
			7 => __("Ne"),
		]
	],

	'collection_status' => [
		'class' => [
			0 => 'suggest',
			1 => 'processed',
			2 => 'ready',
			3 => 'opened',
			4 => 'closed',
			5 => 'confirmed',
			6 => 'locked'
		],
		'name' => [
			0 => __('Navrhovaný'),
			1 => __('Zpracovávaný'),
			2 => __('plánovaný'),
			3 => __('Otevřený'),
			4 => __('Uzavřený'),
			5 => __('Potvrzený'),
			6 => __('Uzamčený')
		]
	],

	'driver_collection_item' => [
		'handled' => [
			'name' => [
				0 => __("-"),
				1 => __("neoblsouženo"),
				2 => __("obslouženo"),
				3 => __("vynecháno")
			],
			'code' => [
				"-" => 0,
				"NOT_HANDLED" => 1,
				"HANDLED" => 2,
				"SKIPPED" => 3
			],
			'decode' => [
				0 => "-",
				1 => "NOT_HANDLED",
				2 => "HANDLED",
				3 => "SKIPPED"
			]
		]
	],

	'co_type' => [
		'' => __("-"),
		1 => __("Řepka"),
		2 => __("Slunečnice"),
		3 => __("Palma"),
		4 => __("Mix")
	]

];
?>