<?php

$cols_translate = [

	//////////////////////
	// globální
	'name' => __('Název'),
	'ic' => __('IČO'),
	'stock_id' => __('Sklad'),
	'psc' => __('PSČ'),
	'town' => __('Město'),
	'street' => __('Ulice'),
	'mobile' => __('Mobil'),
	'phone' => __('Telefon'),
	'email' => __('Email'),
	'bank_account' => __("Bankovní účet"),
	'c_name' => __("Kontaktní osoba"),
	'region_id' => __("Region"),


	////////////////////////
	// pro jednotlivé modely

  'Companies' => [
    'name' => __('Název firmy'),
    'contact_name' => __('Kontaktní osoba'),
    'dph' => __('DPH'),
  ], 

	'Shops' => [
    'name' => __('Název provozovny'),
    'bcontact_name' => __('Obchodní zástupce'),
    'region' => __('Okres'),
    'bcontact_mobile' => __('Mobil'),
    'bcontact_phone' => __('telefon'),
    'contact_mobile' => __('telefon výdej'),
    'bcontact_email' => __('email'),
  ],

	'Carriers' => [
		'shortcut' => __("Zkratka"),
	],

	'Routes' => [
		'CarrierRoutes' => 'Dopravci',
		'distance' => 'délka'
	],

	'Users' => [
		'username' => __('Email'),
		'name' => __("Jméno a příjmení")
	],

	'Commodities' => [
		'shortcut' => __("Zkratka"),
		'sale' => __("Prodej"),
		'unit' => __("Jednotka"),
		'unit2' => __("Sek. jednotka"),
		'unit2_to_unit' => __("Převod"),
		'druh_uhr' => __("Druh úhrady"),
		'in_pack' => __('ks. v balení'),
		'divisible' => __('Dělitelné'),
		'so' => __('Špinavý olej'),
		'pack' => __('Obal'),
		'not_in_stats' => __('Započítat do statistiky'),
		'co_type' => __('Druh čo.'),
		'bill_type' => __("Účet")
	]


];